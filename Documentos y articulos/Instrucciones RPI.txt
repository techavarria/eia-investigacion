1. configurar wifi
2. configurar rc.local para que corra el codigo cuando se prenda la rpi
3. obtener librerias:
	cd ~/Documents/
	sudo apt-get install automake libtool
	wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.50.tar.gz
	tar zxvf bcm2835-1.50.tar.gz
	cd bcm2835-1.50
	autoreconf -vfi
	./configure
	make
	sudo make check
	sudo make install
4. clonar librerias:
	sudo apt-get install git build-essential python-dev
	cd ~/Documents/
	git clone https://github.com/fabiovix/py-ads1256.git
	cd py-ads1256
5. (si se quiere medicion diferencial) cambiar ads1256_test.c
	por el de esta libreria (se cambio ADS1256_StartScan(0) por ADS1256_StartScan(1) en la linea 972 de ads1256_test.c
6. compilar ads1256_test.c:
	sudo python setup.py install
7. ya se puede probar con python read_volts_example.py

Nota: a veces la rpi no se conecta a internet y no corre el programa, puede ser porque la hora esta descuadrada, para esto toca ejecutar el codigo (sudo date --set '2016-04-26 18:26:00') con la fecha y hora indicada en la terminal 