# -*- coding: utf-8 -*-
"""
 ########################################################################################################
 #
 #  MODELO CRISTAL QCM
 #
 #  Universidad EIA Grupo de investigación GIBEC
 #
 #  Proyecto: QCM
 #  Director: Yeison Javier Montagut Ferizzola
 #  Investigador: Lucas Tobón Vanegas
 #
 #  E-mail: lucas.tobon@eia.edu.co
 #
 ########################################################################################################
 #
 #  Archivo:        modelo_2_qcm_00.py
 #
 #  Version python: 2.7    
 #
 #  Programador:    Lucas Tobon Vanegas
 #                  lucas.tobon.v@gmail.com
 #
 #  Descripción:    Este archivo contiene los prototipos de funciones, tareas, variables, 
 #                  definiciones, tipos de datos y constantes de la interfaz grafica
 #
 #  Creado:         Fri Aug 05 11:30:00 2016
 #                  
 #  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 #                  26.07.2016 - Comentarios y correccion de errores ejecución
 #                  09.08.2016 - Afinación de modelo
 #
 #  Pendiente:      Comparar datos con grafica
 #                  
 #
 ########################################################################################################
 #
"""
########################################################################################################
#                                        IMPORTACIÓN DE LIBRERIAS
########################################################################################################
from __future__ import division
from numpy import sqrt, pi, arange, arctan, array
import matplotlib.pyplot as plt
from cmath import phase 
font = {'family': 'serif','color':  'darkred','weight': 'normal','size': 16,}
########################################################################################################
#                                          CONSTANTES DEL CRISTAL
########################################################################################################
fs      = 10e6                                          #Frecuencia resonancia serie dinámica
f       = 10                                             #frencuencia para analizar cambio termico e.g -10,10,1 (10-100)
dt      = 0                                             #Deriva termica (Hz) 0, 5, 10, 100
V       = 1                                             #Amplitud
Ws_0    = 2*pi*fs                                       #Frecuencia angular de la resonancia serie
E_22    = 3.982e-11                                     #Permitividad del cuarzo
de      = 5.2e-3                                        #Diametro del electrodo
e_26    = 9.657e-2                                      #Constante de esfuerzo piezoeléctrico
C_66    = 2.947e10                                      #Modulo de cizalla efectivo
Pq      = 2.651e3                                       #Densidad del cuarzo
Cp      = 2e-12                                         #Capacidad experna del crital de cuarzo
nq      = 9.27e-3                                       #Viscosidad efectiva del cuarzo

Vq      = sqrt(C_66/Pq)                                 #Tension de salida**
K_0_2   = sqrt((e_26**2)/(E_22*C_66))                   #Factor de acoplamiento electromecánico efectivo sin perdidas
hq      = Vq/(2*fs)                                     #Espesor del crital de cuarzo 
Aelec   = (pi*de**2)/4                                  #Area del electrodo
C_0     = Aelec*E_22/hq                                 #Capacidad estatica
C_0x    = C_0 + Cp                                      #Capacidad paralela total
Cq      = (8*C_0*K_0_2**2)/(pi**2)                      #Componente capacitiva rama dinamica
Lq      = 1/(Cq*Ws_0**2)                                #Inductancia equivalente del cristal a la frecuencia de resonancia
#Kt      = (C_66*pi**2)/(4*Aelec*(e_26**2)*Pq*(Ws_0**2)) #Constante electroacustica
Kt      = (hq**2)/(4*(e_26**2)*Aelec)
Zcq     = sqrt(Pq*C_66)
Rq      = 9.63                                          #Resistencia dinamica sin carga
########################################################################################################
#                                           CONSTANTES DEL FLUIDO
########################################################################################################
Pc      = 1000          #mm to m                                          #Densidad del recubrimiento agua
hc      = 1e-7                                          #Grosor del recubrimiento agua
Pl      = 1000                                          #Densidad del liquido en contacto con el cristal
nl      = 1e-3                                          #Viscosidad del liquido en contacto con el cristal
dl      = sqrt(2*nl/(Ws_0*Pl))                          #Profundidad de penetración de la onda acustica en el liquido
########################################################################################################
#                                        CARGA CAPACITIVA E INDUCTIVA
########################################################################################################
                           #Contribución a la carga en la resistencia del sensor
mc      = Pc*hc                                         #Densidad superficial de masa del recubrimiento
ml      = Pl*dl/2                                       #densidad superficial de masa equivalente del liquido en contacto con el
                                                              #cristal, asociada al movimiento oscilatorio de la superficie del sensor.
Lmlco      = mc*Kt
Lmli    = Pl*dl*Kt/2 
Lmo      = Lq + Lmlco + Lmli
Rl      = (Kt*dl*Pl*Ws_0)/2  
Rm      = Rq + Rl
Cm      = Cq
f0s     = 1/(2*pi*sqrt(Lmo*Cq))

d_hc    = arange(-5e-8,5e-8,1e-10) 
d_mc    = Pc*d_hc*1000000
d_Lmlc  = Kt*Pc*d_hc

fs_load = 1/(2*pi*sqrt((Lmo+d_Lmlc)*Cm))

d_fs = -(2*fs_load**2)*(mc + ml)/Zcq
#d_fs    = -Lq*fs_load/(2*(Lmo+d_Lmlc))
#d_fs    = -Ll*f0s/2*d_Lmlc
#fs_load = 1/(2*pi*sqrt((Lm+d_Lmlc)*Cm))
#Rml     = (Kt*dl*Pl*Ws_0)/2  
#
#mc_0    = Pc*hc                                       #Densidad superficial de masa del recubrimiento
#Lmlc_0  = mc_0*Kt     
#Lml_1   = Pl*dl*Kt/2                                    #Inductancia dinamica del agua #Constribución a la carga en la inductancia
#Rm      = Rq + Rml                                      #Contribución de la carga en la resistencia dinámica del sensor
#Lm_0    = Lq + Lmlc_0 + Lml_1                           #Contribución total de la carga en la inductacia
#Cm      = Cq                                            #Capacidad dinámica del cristal sin carga
#f0s     = 1/(2*pi*sqrt(Lm_0*Cm))                        #Frecuencia de resonancia del filtro LC en paralelo con RC1
########################################################################################################
#                                           
########################################################################################################
#mq      = nq*pi/(2*Vq)
#ml      = Pl*hc*1000000   
#Rc      = Rm
#Rt      = 10*Rm
########################################################################################################
#                                        ITERACIÓN ECUACIONES MODELO
########################################################################################################
#d_hc    = arange(-5e-8,5e-8,1e-10)                      #Array de altura
#d_mc    = Pc*d_hc*1000000                               #Cambio en la masa deacuerdo al cambio en la altura
#d_Lmlc  = Kt*Pc*d_hc                                    #Cambio en la inductancia deacuerdo conel cambio de altura
#d_fs    = -d_Lmlc*f0s/2*Lm
#
#d_ff    = arange(-f,f,f*2./1000)
#d_L     = 1/(((2*pi*(f0s + d_ff))**2)*Cq)
#d_LT    = d_L - Lm
#
#fs_load = 1/(2*pi*sqrt((Lm+d_Lmlc)*Cm))               #Frecuencia deacuerdo al cambio en la inductancia

#d_fs    = f0s-(2*(f0s)*d_mc/(Aelec*sqrt(Pq*C_66)))#-((d_Lmlc)/(2*Lm_0))*fs_load                  #Cambio en la frecuencia

#d_Q_3   = ((d_mc/(mq + ml))*(Rt/(Rt+Rm)))*360/(2*pi)    #Fase por ecuacion
#d_Q_2   = ((d_mc/(mq + ml)))*360/(2*pi)                 #Fase por ecuacion
########################################################################################################
#                                       ECUACIONES FASORIALES CIRCUITO
########################################################################################################
#W       = 2*pi*f0s
#R       = Rm
#Lo      = Lm_0
#C       = Cq
#Co      = C_0x
#
#ZR1     = R + 0j
#ZRq     = R +0j
#
#ZLo     = W*Lo*1j
#ZdL     = W*d_Lmlc*1j
#ZdT     = W*d_LT*1j
#
#ZCq     = 1j/(W*Cq)
#ZCo     = 1j/(W*Co)
#Zm      = ZRq + ZLo + ZdL - ZCq + ZdT 
#Zx      = (Zm*ZCo)/(Zm+ZCo)
#
#Z       = Zx/(Zx+ZR1)
#Vi      = V                             #Voltaje de entrada
#Vo      = Z*Vi                          #Voltaje salida
#
#phzm    = []
#mgzm    = []
#phco    = []
#mgco    = []
#phzmt   = []
#for i in range(0,1000):
#    #Zm
#    phzm.append(phase(Zm[i])*360/(2*pi))
#    mgzm.append((sqrt((Zm[i].real)**2 + (Zm[i].imag)**2)))
#    
#    #Zcto
#    phco.append(phase(Vo[i])*360/(2*pi))
#    mgco.append((sqrt((Vo[i].real)**2 + (Vo[i].imag)**2)))
########################################################################################################
#                                        GRAFICA DELTA Phi - DELTA M
########################################################################################################
#fig, ax1 = plt.subplots()
#ax1.set_title(u'Representación grafica de la impedancia dinamica y Cto')
#l1, = ax1.plot(d_mc,phzm,'r')
#l2, = ax1.plot(d_mc,d_Q_3,'black')
#l3, = ax1.plot(d_mc,d_Q_2,'b')
#l4, = ax1.plot(d_mc,phco,'g')
#ax1.legend((l1,l2,l3,l4), (r'$\Delta \varphi Zm $',r'$\Delta \varphi a $',
#                           r'$\Delta \varphi b $',r'$\Delta \varphi cto $'), loc='upper left',shadow = True)
#plt.grid(True)
#ax1.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
#ax1.set_ylabel(r'$\Delta \varphi (Grad)$')
#########################################################################################################
##                                      GRAFICA DELTA DE |Z| - DELTA M
#########################################################################################################
#fig, ax2 = plt.subplots()
#ax2.set_title(u'Representación grafica de la amplitud en el Cto')
#ax2.plot(d_mc,mgco,'black')
#ax_c = ax2.twinx()
#ax_c.plot(d_mc,phzm)
#plt.grid(True)
#ax2.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
#ax2.set_ylabel(r'$\Delta |Zcto| (Voltios)$')
#########################################################################################################
##                                      GRAFICA DELTA DE M - DELTA Fs
#########################################################################################################
#fig, ax3 = plt.subplots()
#ax3.set_title(u'Cambio de la frecuncia de resonancia')
#ax3.plot(d_mc,d_fs,'b')
#ax3.grid(True)
#plt.xlabel(r'$\Delta M(ng)$')
#plt.ylabel(r'$\Delta F (Hz)$')
#########################################################################################################
##                                      GRAFICA DELTA DE F - DELTA L
#########################################################################################################
#fig, ax4 = plt.subplots()
#ax4.set_title(u'Representación grafica del cambio inductivo')
#ax4.plot(d_fs,d_LT,'b')
#ax4.grid(True)
#
#plt.ylabel(r'$\Delta L(H)$')
#plt.xlabel(r'$\Delta F (Hz)$')
#########################################################################################################
##                                      GRAFICA DELTA DE Phi - DELTA H
#########################################################################################################
#fig, ax4 = plt.subplots()
#ax4.set_title(u'Representación grafica del phi vs f')
#
#a = []
#for i in range(500,1000):
#    a.append(phzm[i])
#
#ax4.plot(d_hc,phzm,'b')
#ax4.grid(True)
#
#plt.ylabel(r'$\Delta L(H)$')
#plt.xlabel(r'$\Delta F (Hz)$')
#plt.show()