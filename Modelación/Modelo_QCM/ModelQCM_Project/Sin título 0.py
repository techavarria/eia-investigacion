# -*- coding: utf-8 -*-
"""
Created on Mon Apr 03 16:29:24 2017

@author: yeison.montagut
"""

from numpy import sqrt, pi, arange, arctan, array, searchsorted
import matplotlib.pyplot as plt
from cmath import phase 
font = {'family': 'serif','color':  'darkred','weight': 'normal','size': 16,}

S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}

#Rm = [8, 25.46, 25.46, 18.191, 18.191, 18.191, 18.191]
Rm = []

#Lm = [7.752e-3, 8.1533e-3, 8.16e-3, 0.00881305, 0.0088148, 0.00880953750362]

#Lm1 = [0.0088095376520175937, 0.0088095376371774125, 0.0088095376223372816, 0.0088095376074971976, 0.0088095375926571604, 0.0088095375778171753, 0.0088095375629772404, 0.0088095375481373542, 0.0088095375332975113, 0.0088095375184577204, 0.0088095375036179747, 0.0088095374887782775, 0.0088095374739386358, 0.0088095374590990358, 0.0088095374442594826, 0.0088095374294199814, 0.0088095374145805289, 0.0088095373997411266, 0.0088095373849017677, 0.0088095373700624573]
#Lm2 = [0.0088095375036179747, 0.0088095375036179747, 0.0088095887391176804, 0.008809641741829112, 0.0088096823775651503, 0.0088097177132163314, 0.0088097406815036052, 0.0088097512822818726, 0.0088097548158788796, 0.0088097565826781802, 0.0088097601162783791, 0.00880976276647992, 0.0088097663000838364, 0.0088097689502881719, 0.0088097716004936991, 0.0088097724838958075, 0.0088097733672980513, 0.0088097742507004251, 0.0088097742507004251, 0.0088097760175055717]

Lm = [ 8.8029e-3, 0.00880953750362, 0.0088095375036179747, 0.0088095375036179747, 0.0088095375036179747, 0.0088095375036179747, 0.0088095375036179747, 0.0088095375036179747, 0.0088095901248065045, 0.0088096427459950361, 0.0088096848429458606, 0.0088097199237382132, 0.0088097409722136255, 0.0088097514964513325, 0.0088097550045305676, 0.0088097550045305676, 0.0088097550045305676, 0.0088097585126098027, 0.0088097620206890378, 0.0088097655287682746, 0.0088097690368475097, 0.0088097690368475097, 0.0088097725449267448, 0.0088097725449267448, 0.0088097760530059799, 0.0088097760530059799, 0.0088097760530059799]
        
#Cm = [32.68e-15, 31.229e-15, 31.229e-15, 28.911e-15, 28.911e-15,  28.911e-15]
Cm = []
Co = []
for i in range(len(Lm)):
    Cm.append(28.911e-15)
    Co.append(6.338e-12)
    Rm.append(18.191)
#Co = [7.07e-12, 7.4e-12, 7.4e-12, 6.338e-12, 6.338e-12, 6.338e-12]

S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}
#########################################################################################################
#             VALORES DE FRECUENCIA PARA EL BARRIDO 
#########################################################################################################
n = 10
 
fo = 9999200 
fi = fo - 27500
ff = fo -25500
 
f = arange(fi,ff,n)

#########################################################################################################
#             IMPEDANCIA DEL CRISTAL PARA LAS DIFERENTES FRECUENCIAS 
#########################################################################################################

Zx = []
Zm = []
ZLm = []
ZCm = []
ZCo = []
R = []
G = []
P = []
Cl = []
Cl_mag = []
picoG = []
MSRF = []
deltaf = []
ph = []

y = len(f)


for k in range(len(Lm)):
    R.append([])
    ZLm.append([])
    ZCm.append([])
    ZCo.append([])
    Zm.append([])
    Zx.append([])
    R.append([])
    P.append([])
    G.append([])
    #picoG.append([])
    #MSRF.append([])
    #deltaf.append([])

#    
for k in range(len(Lm)):
    
    for i in range(0,y):
        ZLm[k].append(2*pi*f[i]*Lm[k]*1j)
        ZCm[k].append(1/(2*pi*f[i]*Cm[k]*1j))
        ZCo[k].append(1/(2*pi*f[i]*Co[k]*1j))
#       
    for i in range(0,y):
        Zm[k].append(Rm[k]+ZLm[k][i]+ZCm[k][i])
#        
    for i in range(0,y):
        Zx[k].append((Zm[k][i]*ZCo[k][i])/(Zm[k][i]+ZCo[k][i]))
#        
#
#
    for i in range(0,y):
        R[k].append(sqrt(((Zx[k][i].real)**2) + ((Zx[k][i].imag)**2 )))
        P[k].append(phase(Zx[k][i])*360/(2*pi))
#
#   
#   
    for i in range(0,y):
        G[k].append(1/R[k][i])
        


#        
#
#for i in range(1):
#    picoG.append(G[i].index(max(G[i])))
#
#for i in range(1):
#    MSRF.append(f[picoG[i]])
#    
#for i in range(1):
#    deltaf.append((MSRF[0])-(MSRF[i]))
#    
#for i in range(1):
#    ph.append(P[i][picoG[i]])
##    
#
#    
#        
#    
#    
#    
#   
#########################################################################################################
##                                        GRAFICA G vs f
#########################################################################################################
#
#color = ['r', 'g', 'b', 'y']
#for k in range(1):
#    plt.subplot(2, 1, 1)
#    plt.plot(f, G[k], color[k])
#    plt.axvline(x=f[G[k].index(max(G[k]))],ls='--',)
##plt.title('CONDUCTANCIA')
#plt.ylabel('G (mS)')
#plt.xlabel('f (Hz)')
#plt.grid()
##
#
#for k in range(1):
#    plt.subplot(2, 1, 2)
#    plt.plot(f, P[k], color[k])
#    plt.axvline(x=f[G[k].index(max(G[k]))],ls='--',)
##plt.title('FASE')
#plt.ylabel('fase (o)')
#plt.xlabel('f (Hz)')
#plt.grid()
##
#plt.show()

#for k in range(2):        
#plt.plot(f,G[1])
#plt.ylabel('G (ms)')
#plt.xlabel('f (Hz)')
#plt.grid()
#plt.show   

fig, ax = plt.subplots()
for i in range(1,len(Lm)):
    
    line1, = ax.plot(f,G[i],'b', label='QCM sin carga')

#line2, =ax.plot(f,G[0],'r', label = 'QCM sin monocapa')

line3, =ax.plot(f,G[1],'r', label = 'QCM con monocapa')

plt.ylabel(r'$G (ms)$')
plt.xlabel(r'$f (Hz)$')
plt.grid()
plt.legend((line1, line3, ), (u'Durante interacción', u'Sin interacción', 'Sin Monocapa'))
#plt.legend(loc='upper left')



d_f = (array([  0,   0,     0,     0,     0,   0,     0,  29,    59,  82, 102, 115, 121, 
       123, 124,   126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135, 135 ])*-1)+135
      
t   = arange(0, 10, 0.39)
fig, axA = plt.subplots()

lineA, = axA.plot(t,d_f,'black', label = u'Interacción')
plt.grid()
plt.ylabel(r'$\Delta f (f-f_(min)) (Hz)$')
plt.xlabel(r'$t(min)$')
#plt.legend((lineA, ),(u'Interacción'))
#
#
#print(len(self.d_f))
#print(len(self.t))
#fig, ax = plt.subplots()
#ax.plot(self.t, self.d_f)
#ax.grid(True)
#ax.set_title(u'Representación grafica del tiempo vs frecuencia')
#ax.set_xlabel(r'$Tiempo (minutos)$')
#ax.set_ylabel(r'$\Delta F (Hz)$')


plt.show()
#line1, = ax.plot(x, np.sin(x), '--', linewidth=2,
#                 label='Dashes set retroactively')
#line1.set_dashes(dashes)
#
#line2, = ax.plot(x, -1 * np.sin(x), dashes=[30, 5, 10, 5],
#                 label='Dashes set proactively')
#
#ax.legend(loc='lower right')
#plt.show() 