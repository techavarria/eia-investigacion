# -*- coding: utf-8 -*-
"""
 ########################################################################################################
 #
 #  MODELO CRISTAL QCM
 #
 #  Universidad EIA Grupo de investigación GIBEC
 #
 #  Proyecto: QCM
 #  Director: Yeison Javier Montagut Ferizzola
 #  Investigador: Lucas Tobón Vanegas
 #
 #  E-mail: lucas.tobon@eia.edu.co
 #
 ########################################################################################################
 #
 #  Archivo:        modelo_2_qcm_00.py
 #
 #  Version python: 2.7    
 #
 #  Programador:    Lucas Tobon Vanegas
 #                  lucas.tobon.v@gmail.com
 #
 #  Descripción:    Este archivo contiene los prototipos de funciones, tareas, variables, 
 #                  definiciones, tipos de datos y constantes de la interfaz grafica
 #
 #  Creado:         Fri Aug 05 11:30:00 2016
 #                  
 #  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 #                  26.07.2016 - Comentarios y correccion de errores ejecución
 #                  09.08.2016 - Afinación de modelo
 #                  23.11.2016 - Afinación de modelo
 #                  23.11.2016 - Control de versiones
 #          
 #  Pendiente:      Comparar datos con grafica
 #                  
 ########################################################################################################
 #
"""
########################################################################################################
#                                        IMPORTACIÓN DE LIBRERIAS
########################################################################################################
from __future__ import division
from numpy import sqrt, pi, arange, array
import matplotlib.pyplot as plt
from cmath import phase
from scipy import stats
from time import time
font = {'family': 'serif','color':  'darkred','weight': 'normal','size': 16,}


class circuito:
    def __init__(self, cto, f, fss, ft, Rq, Cq, Lq, Co):
        ########################################################################################################
        #                                       ECUACIONES FASORIALES CIRCUITO medicion (1a)
        ########################################################################################################
        ########################################################################################################
        #                                          CONSTANTES DEL CRISTAL
        ########################################################################################################
 
        self.cto = cto
        self.f   = f                                              #Frecuencia resonancia serie dinamica
        self.fss = fss
        self.ft  = ft                                            #Frecuencia para analizar cambio termico e.g -10,10,1 (10-100)#Abs de los cambios de frecuencia hacia el cambio inductivo
        
        if self.fss:
            self.fs = arange((self.f*10**6-self.fss),(self.f*10**6+self.fss),self.fss/self.f) #Frecuencia resonancia serie dinámica #len 20
        else:
            self.fs = array([self.f*10**6]*20)  #len 20
    
        V       = 1                                             #Amplitud
        Ws_0    = 2*pi*self.fs                                  #Frecuencia angular de la resonancia serie #len 20
        #E_22    = 3.982e-11                                    #Permitividad del cuarzo
        de      = 5.2e-3                                        #Diametro del electrodo
        e_26    = 9.657e-2                                      #Constante de esfuerzo piezoeléctrico
        C_66    = 2.947e10                                      #Modulo de cizalla efectivo
        Pq      = 2.651e3                                       #Densidad del cuarzo
        #Cp     = 2e-12                                         #Capacidad experna del crital de cuarzo
        nq      = 9.27e-3                                       #Viscosidad efectiva del cuarzo
        
        Vq      = sqrt(C_66/Pq)                                 #Tension de salida**
        #K_0_2  = sqrt((e_26**2)/(E_22*C_66))                   #Factor de acoplamiento electromecánico efectivo sin perdidas
        hq      = Vq/(2*self.fs)                                #Espesor del crital de cuarzo #len 20
        Aelec   = (pi*de**2)/4                                  #Area del electrodo
        #C_0     = Aelec*E_22/hq                                #Capacidad estatica #len 20
        #C_0x    = array([1]*1000)*Co#C_0 + Cp                  #Capacidad paralela total #len 20
        self.Cq      = array([1]*20)*Cq#(8*C_0*K_0_2**2)/(pi**2)#Componente capacitiva rama dinamica #len 20
        #Ws     = (1/hq)*(sqrt(C_66/Pq))*sqrt((1*pi**2)-8*K_0_2)        
        self.Lq      = Lq#1/(Cq*Ws_0**2)                        #Inductancia equivalente del cristal a la frecuencia de resonancia #len 20
        Kt      = (hq**2)/(4*(e_26**2)*Aelec)
        #Kt     = (C_66*pi**2)/(4*Aelec*(e_26**2)*Pq*(Ws_0**2)) #Constante electroacustica #len 20
        self.Rq      = Rq#9.63                                  #Resistencia dinamica sin medicion
        ########################################################################################################
        #                                           CONSTANTES DEL FLUIDO
        ########################################################################################################
        Pc      = 1000                                          #Densidad del recubrimiento agua g/m3
        hc      = 1e-7                                          #Grosor del recubrimiento agua
        Pl      = 1000                                          #Densidad del liquido en contacto con el cristal
        nl      = 1e-3                                          #Viscosidad del liquido en contacto con el cristal
        dl      = sqrt(2*nl/(Ws_0*Pl))                          #Profundidad de penetración de la onda acustica en el liquido #len 20
        ########################################################################################################
        #                                        medicion CAPACITIVA E INDUCTIVA
        ########################################################################################################
        Rml     = (Kt*dl*Pl*Ws_0)/2                             #Contribución a la medicion en la resistencia del sensor #len 20
        mc_0    = Pc*hc                                         #Densidad superficial de masa del recubrimiento
        Lmlc_0  = mc_0*Kt                                       #len 20
        print(Kt)
        Lml_1   = Pl*dl*Kt/2                                    #Inductancia dinamica del agua #Constribución a la medicion en la inductancia #len 20
        Rm      = self.Rq + Rml                                 #Contribución de la medicion en la resistencia dinámica del sensor #len 20
        Lm      = self.Lq + Lmlc_0 + Lml_1                      #Contribución total de la medicion en la inductacia #len 20
        Cm      = self.Cq                                       #Capacidad dinámica del cristal sin medicion
        self.f0s= 1/(2*pi*sqrt(Lm*Cm))                          #Frecuencia de resonancia del filtro LC en paralelo con RC1 #len 20
        ########################################################################################################
        #                                           
        ########################################################################################################
        mq      = nq*pi/(2*Vq) 
        ml      = (Pl*dl/2)*1000000
        Rt      = Rm*10
        ########################################################################################################
        #                                        ITERACIÓN ECUACIONES MODELO
        ########################################################################################################      
        
        self.d_hc    = arange(-5e-8,5e-8,1e-10)                           #Array de altura #len 1000 pasos -50ng a 50 ng 1A= 100pg 
        self.d_mc    = Pc*self.d_hc*1000000                               #Cambio en la masa deacuerdo al cambio en la altura #len 1000 #conversión de gramos a micro gramos         
        
        self.d_Lmlc  = []
        for i in range(0,20):        
            self.d_Lmlc.append(Kt[i]*Pc*self.d_hc)                        #Cambio en la inductancia de acuerdo con el cambio de altura #len 20 1000
        #print(self.d_Lmlc)
        d_L = []
        self.d_LT = []
            
        if(self.ft != 0):
            self.d_ff = arange(-self.ft,self.ft,self.ft/10)                  #len 20
            for i in range(20):
                d_L.append(1/(((2*pi*(self.f0s[i] + self.d_ff[i]))**2)*self.Cq[i])) #len 20

            for i in range(20):
                self.d_LT.append(d_L[i] - Lm[i]) #len 20
            print(self.d_LT)
        else:
            self.d_ff = [0]*20                                            #len 20
            for i in range(20):
                d_L.append(1/(((2*pi*(self.f0s[i] + self.d_ff[i]))**2)*self.Cq[i])) #len 20
            print(self.f0s)
            print(self.d_ff)
            
        
            for i in range(20):
                self.d_LT.append(0) #len 20

        self.fs_load=[]
        for i in range(0,20):
            self.fs_load.append([])
            for j in range(0,1000):
                self.fs_load[i].append(1/(2*pi*sqrt((Lm[i] + self.d_Lmlc[i][j])*Cm[i])))               #Frecuencia deacuerdo al cambio en la inductancia
    
        
        self.d_fs = []
        for i in range(0,20):
            self.d_fs.append([])                                                                       
            for j in range(0,1000):                           
                self.d_fs[i].append(-((self.d_Lmlc[i][j])/(2*Lm[i]))*self.fs_load[i][j])                  #Cambio en la frecuencia
                #self.d_fs[i].append(self.f0s[i]-2*(self.f0s[i])*self.d_mc[j]/(Aelec*sqrt(Pq*C_66)))#-((d_Lmlc)/(2*Lm_0))*fs_load                  #Cambio en la frecuencia
        
        self.d_Q_4 = []
        self.d_Q_3 = []
        self.d_Q_2 = []
        
        for i in range(20):
            self.d_Q_4.append(((self.d_mc/(mq + ml[i]))*(Rm[i]/(Rm[i]+Rm[i])))*360/(2*pi))    #Fase por ecuacion
            self.d_Q_3.append(((self.d_mc/(mq + ml[i]))*(Rt[i]/(Rt[i]+Rm[i])))*360/(2*pi))    #Fase por ecuacion
            self.d_Q_2.append(((self.d_mc/(mq + ml[i])))*360/(2*pi))                          #Fase por ecuacion
        ########################################################################################################
        ########################################################################################################
        W       = 2*pi*self.f0s #len 20
        R       = Rm
        Lo      = Lm
        C       = Cq
        self.Co = Co #C_0x
        
        ZR1     = R*10 + 0j #R1 10 veces mayro
        ZRq     = R +0j
        ########################################################################################################
        # 1 Se refiere a la vinculacion de la inductancia "termica"
       
        if self.cto == "medicion1":
            ZLo     = W*Lo*1j
            ZdL = []
            ZdT = []
            for i in range(0,20):
                ZdT.append(W[i]*self.d_LT[i]*1j)
                ZdL.append([])
                for j in range(0,1000):
                    ZdL[i].append(W[i]*self.d_Lmlc[i][j]*1j)
                    
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)

        elif self.cto == "referencia1":
            ZLo     = W*Lo*1j
            ZdL     = [[0]*1000]*20
            ZdT     = W*self.d_LT*1j
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)
        
        elif self.cto == "rc1":
            ZLo     = [0]*20
            ZdL     = [[0]*1000]*20
            ZdT     = [0]*20          

            ZCq     = [0]*20
            ZCo     = 1j/(W*Co)        
        ########################################################################################################         
        # 1 Se refiere a la desvinculacion de la inductancia "termica"
        if self.cto == "medicion2":
            ZLo     = W*Lo*1j
            ZdL = []
            ZdT = [0]*20
            for i in range(0,20):
                #ZdT.append(W[i]*self.d_LT[i]*1j) # Inductancia "Termica"
                ZdL.append([])
                for j in range(0,1000):
                    ZdL[i].append(W[i]*self.d_Lmlc[i][j]*1j)
                    
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)

        elif self.cto == "referencia2":
            ZLo     = W*Lo*1j
            ZdL     = [[0]*1000]*20
            ZdT     = [0]*20#W*self.d_LT*1j      # Inductancia "Termica"
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)
        
        elif self.cto == "rc2":
            ZLo     = [0]*20
            ZdL     = [[0]*1000]*20
            ZdT     = [0]*20

            ZCq     = [0]*20
            ZCo     = 1j/(W*Co)
        ########################################################################################################
        self.Zm2 = []
        for i in range(20):
            self.Zm2.append([])
            for j in range(1000):
                self.Zm2[i].append(ZRq[i] + ZLo[i] + ZdL[i][j] - ZCq[i] + ZdT[i])

        self.Zx2 = []
        for i in range(0,20):
            self.Zx2.append([])
            for j in range(0,1000):
                self.Zx2[i].append((self.Zm2[i][j]*ZCo[i])/(self.Zm2[i][j]+ZCo[i]))
        
        self.Z   = []
        for i in range(0,20):
            self.Z.append(self.Zx2[i]/(self.Zx2[i]+ZR1[i]))

        Vi      = V                             #Voltaje de entrada
        self.Vo = self.Z*Vi                     #Voltaje salida       
    
    def rama_dinamica(self):
        phzm2_2    = []
        mgzm2_2    = []

        for j in range(0,20):
            phzm2_2.append([])
            mgzm2_2.append([])
            for i in range(0,1000):
                phzm2_2[j].append(phase(self.Zm2[j][i])*360/(2*pi))
                mgzm2_2[j].append(sqrt((self.Zm2[j][i].real)**2 + (self.Zm2[j][i].imag**2)))
        
        return (phzm2_2, mgzm2_2)
        
    def rama_dcp(self):
        phzx_2    = []
        mgzx_2    = []
        for j in range(0,20):
            phzx_2.append([])
            mgzx_2.append([])
            for i in range(0,1000):
                phzx_2[j].append(phase(self.Zx2[j][i])*360/(2*pi))
                mgzx_2[j].append((sqrt((self.Zx2[j][i].real)**2 + (self.Zx2[j][i].imag)**2)))
        
        return (phzx_2, mgzx_2)
    
    def rama_circuito(self):
        phco_2    = []
        mgco_2    = []
        for j in range(0,20):
            phco_2.append([])
            mgco_2.append([])
            for i in range(0,1000):
                phco_2[j].append(phase(self.Vo[j][i])*360/(2*pi))
                mgco_2[j].append((sqrt((self.Vo[j][i].real)**2 + (self.Vo[j][i].imag)**2)))   
        
        return (phco_2,mgco_2)
        
    def graf_1(self):                   #GRAFICA DELTA Phi - DELTA M
        c = circuito(self.cto, self.f, self.fss, self.ft, self.Rq, self.Cq, self.Lq, self.Co)
 
        fig, ax1 = plt.subplots()
        ax1.set_title(u'Representación grafica de la impedancia dinamica y Cto Cto: ' + self.cto)
        
        phzm = c.rama_dinamica()[0]
        phzx = c.rama_dcp()[0]
        phco = c.rama_circuito()[0]

        for i in range(0,20):        
            l1, = ax1.plot(self.d_mc,phzm[i],'r')
            l4, = ax1.plot(self.d_mc,phzx[i],'g')
            l5, = ax1.plot(self.d_mc,phco[i],'y')
            l2, = ax1.plot(self.d_mc,self.d_Q_3[i],'black')
            l3, = ax1.plot(self.d_mc,self.d_Q_2[i],'b')
            l6, = ax1.plot(self.d_mc,self.d_Q_4[i],'black')

        ax1.legend((l1,l2,l3,l4,l5,l6), (r'$\Delta \varphi Zm $',r'$\Delta \varphi a $',
                                         r'$\Delta \varphi b $',r'$\Delta \varphi cto $',r'$\Delta \varphi Zx $',r'$\Delta \varphi acto $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax1.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax1.set_ylabel(r'$\Delta \varphi (Grad)$')
        fig.savefig('Graficas/'+'Validacion/'+'Efectos_termicos/'+str(c.f)+'/'+self.cto+'/Impedanciadinamica'+'.png')
        
        plt.show()
        
    def graf_2(self):                    #GRAFICA DELTA DE |Z| - DELTA M
        
        c = circuito(self.cto,self.f,self.fss,self.ft, self.Rq, self.Cq, self.Lq, self.Co)
                        
        fig, ax2 = plt.subplots()
        ax2.set_title(u'Representación grafica de la amplitud en el Cto Cto: ' + self.cto)
        
        mgco = c.rama_circuito()  
        mgzx = c.rama_dcp()
        mgzm = c.rama_dinamica()
   
        
        for i in range(20):
            l1, = ax2.plot(c.d_mc, mgco[1][i], 'r')
        
            ax_c = ax2.twinx()
        
            l2, = ax_c.plot(c.d_mc, mgzx[1][i], 'b')
        
            l3, = ax_c.plot(c.d_mc, mgzm[1][i], 'g')
    
        ax2.legend((l1, l2, l3), (r'$|Zcto| $', r'$|Zx| $',r'$|Zm| $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax2.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax2.set_ylabel(r'$\Delta |Zcto| (Voltios)$',color='r')
        fig.savefig('Graficas/'+'Validacion/'+'Efectos_termicos/'+str(c.f)+'/'+self.cto+'/Amplitud'+'.png')
        
        plt.show()
        
    def graf_3(self):                   #GRAFICA DELTA DE M - DELTA Fs
        c = circuito(self.cto,self.f,self.fss,self.ft, self.Rq, self.Cq, self.Lq, self.Co)
        fig, ax3 = plt.subplots()
        ax3.set_title(u'Cambio de la frecuncia de resonancia Cto: ' + self.cto)

        ax3.plot(c.d_mc,self.d_fs[9],color= 'black')
        
        for i in range(0,9):
            ax3.plot(c.d_mc,self.d_fs[i],color= 'gray')
        for i in range(10,20):
            ax3.plot(c.d_mc,self.d_fs[i],color= 'gray')
        
        ax3.grid(True)
        plt.xlabel(r'$\Delta M(ng)$')
        plt.ylabel(r'$\Delta F (Hz)$')
        fig.savefig('Graficas/'+'Validacion/'+'Efectos_termicos/'+str(c.f)+'/'+self.cto+'/Frecuencia'+'.png')
        
        plt.show()
        
    def graf_4(self):                   #GRAFICA DELTA DE F - DELTA L
        c = circuito(self.cto,self.f,self.fss,self.ft, self.Rq, self.Cq, self.Lq, self.Co)        
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del cambio inductivo medicion Cto: ' + self.cto)       
            
        ax4.plot(self.d_fs[9],self.d_Lmlc[9],color= 'black')
        for i in range(0,9):
            ax4.plot(self.d_fs[i],self.d_Lmlc[i],color= 'gray')
        for i in range(10,20):
            ax4.plot(self.d_fs[i],self.d_Lmlc[i],color= 'gray')
            
        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F (Hz)$')
        fig.savefig('Graficas/'+'Validacion/'+'Efectos_termicos/'+str(c.f)+'/'+self.cto+'/Cambioinductivo'+'.png')
        
        plt.show()
        
    def graf_5(self):                   #GRAFICA DELTA DE Phi - DELTA H
        
        c = circuito(self.cto,self.f,self.fss,self.ft, self.Rq, self.Cq, self.Lq, self.Co)        
        
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del phi vs f Cto: ' + self.cto)
            
        phzm = c.rama_dinamica()
        
        a = []
        for i in range(500,1000):
            a.append(phzm[0][10][i])
            
        ax4.plot(self.d_hc,phzm[0][9],color= 'black')
        for i in range(0,9):
            ax4.plot(self.d_hc,phzm[0][i],color= 'gray')
        for i in range(10,20):
            ax4.plot(self.d_hc,phzm[0][i],color= 'gray')

        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F(Hz)$')
        fig.savefig('Graficas/'+'Validacion/'+'Efectos_termicos/'+str(c.f)+'/'+self.cto+'/phiF'+'.png')
        plt.show()
        
###############################################################################
class celda:
    
    def __init__(self,celda,f,fss,ft):
        if celda == 1:
            #Sensor 1
            S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}
            #Sensor 2
            S2 = {'Rq':22.729, 'Cq':28.923e-15, 'Lq':8.79823-3, 'Co':6.9469e-12}
            
            self.qa = circuito("medicion1", f, fss, ft, S1['Rq'], S1['Cq'], S1['Lq'], S1['Co'])
            self.qb = circuito("referencia1", f, fss, ft, S2['Rq'], S2['Cq'], S2['Lq'], S2['Co'])
            self.qc = circuito("rc1", f, fss, ft, S2['Rq'], S2['Cq'], S2['Lq'], S2['Co'])
            
        elif celda == 2:
            self.qa = circuito("medicion2",f,fss,ft)
            self.qb = circuito("medicion2",f,fss,ft)
            self.qc = circuito("rc2",f,fss,ft)
    
    def phase(self):  
        qa_ph = self.qa.rama_circuito()[0]
        qb_ph = self.qb.rama_dinamica()[0]
        qc_ph = self.qc.rama_dinamica()[0]
        
        qx = []
        qy = []
        qt = []
    
        for i in range(20):
            qx.append([])
            qy.append([])
            for j in range(1000):
                qx[i].append(qa_ph[i][j] - qc_ph[i][j])
                qy[i].append(qb_ph[i][j] - qc_ph[i][j])
        
        for i in range(20):
            qt.append([])
            for j in range(1000):
                qt[i].append(qx[i][j]-qy[i][j])
                
        return ( qa_ph, qb_ph, qc_ph, qt)

###############################################################################
class ensayo:
    
    def __init__(self, conf_celda, Vi, fss, ft, dt):
        ################################################################################
        #   Modelo de interacción
        ################################################################################
        self.f = Vi
        self.fss = fss
        self.ft = ft
        self.dt = dt
        
        self.celda = conf_celda
#       self.cto = cto
        self.t   = arange(0, 10, 0.5)#0.34) #Minutos
        self.d_f = [0,     0,  29,    59,  82, 102, 115, 121, 
                    123, 124,   126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135]#, 135, 135, 135, 135, 135 ] #Hz
                    
        ################################################################################
        ################################################################################
        S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}
#        
        self.c = circuito('medicion1', self.f, self.fss, self.ft, S1['Rq'], S1['Cq'], S1['Lq'], S1['Co'])
        self.d_mc = self.c.d_mc
        self.d_fs = self.c.d_fs
        self.d_ff = self.c.d_ff
        self.d_LT = self.c.d_LT
        deriv_t   = {0:array([0]*20), 5:arange(0,5,0.17), 10:arange(0,10,0.34), 100:arange(0,100,3.4)} #Arreglos de derivas #Deriva termica (Hz) 0, 5, 10, 100
        self.d_t  = self.d_f-deriv_t[self.dt]*1000000
        #print(len(self.d_f))
        
    def Ym(self, f):
        m = []
        b = []
        #e = ensayo(self.celda, self.cto, self.f, self.fss, self.ft, self.dt)
        for i in range(20):
            m.append(stats.linregress(self.d_mc, self.d_fs[i])[0]) #Pendiente
            b.append(stats.linregress(self.d_mc, self.d_fs[i])[1]) #Intercepto
        Ym = []
        for i in range(20):
                Ym.append((f - b[i])/m[i])
        return Ym

    def Yq(self, d_m, q):
        m  = stats.linregress(self.d_mc, q)[0] #Pendiente
        b  = stats.linregress(self.d_mc, q)[1] #Intercepto
        Yq = m*d_m + b
        return Yq
        
    def YL(self, f):
        #c = circuito(self.cto,self.f, self.fss, self.ft)
        m = stats.linregress(self.d_ff, self.d_LT)[0] #Pendiente
        b = stats.linregress(self.d_ff, self.d_LT)[1] #Intercepto
        L = m*f + b
        return L
    
    def tf(self):
    #----------------------------------------------------
        #e = ensayo(self.celda, self.cto, self.f, self.fss, self.ft, self.dt)
        print(len(self.d_f))
        print(len(self.t))
        fig, ax = plt.subplots()
        ax.plot(self.t, self.d_f)
        ax.grid(True)
        ax.set_title(u'Representación grafica del tiempo vs frecuencia')
        ax.set_xlabel(r'$Tiempo (minutos)$')
        ax.set_ylabel(r'$\Delta F (Hz)$')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/interaccioninicial'+'.png')
        #print('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/interaccioninicial'+'.png')
    #----------------------------------------------------
    def mf(self):
        #e = ensayo(self.celda, self.cto, self.f, self.fss, self.ft, self.dt)
        fig, ax = plt.subplots()
        self.d_m = self.Ym(self.d_f)
        for i in range(20):
            ax.plot(-self.d_m[i], self.d_f)
        ax.grid(True)
        ax.set_title(u'Representación grafica de la masa vs frecuencia')
        ax.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax.set_ylabel(r'$\Delta F (Hz)$')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/masafrecuencia'+'.png')
    #----------------------------------------------------
    def tm(self):
        #e = ensayo(self.celda, self.cto, self.f, self.fss, self.ft, self.dt)
        fig, ax03 = plt.subplots()
        self.d_m  = self.Ym(self.d_f)
        for i in range(20):
            ax03.plot(self.t, self.d_m[i])
        ax03.grid(True)
        ax03.set_title(u'Representación grafica del tiempo vs masa')
        ax03.set_xlabel(r'$Tiempo (minutos)$')
        ax03.set_ylabel(r'$\Delta M_c (ng/mm^2)$')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/masatiempo'+'.png')
    #----------------------------------------------------
    def tphi(self):
        
        #e = ensayo(self.celda, self.cto, self.f, self.fss, self.ft, self.dt)
        
        #deriv_t = {0:array([0]*30), 5:arange(0,5,0.17), 10:arange(0,10,0.34), 100:arange(0,100,3.4)} #Arreglos de derivas #Deriva termica (Hz) 0, 5, 10, 100
        
        #d_m = self.Ym(self.d_f)*1000000
        
        d_md = self.d_t#self.Ym(self.d_f-deriv_t[10])*1000000
        #print(len(d_md))
        c = celda(self.celda, self.f, self.fss, self.ft)
        qa, qb, qc, qt = c.phase()
        #print(str(len(qa)) + " | "+ str(len(qb)) + " | " + str(len(qc)) + " | " + str(len(qt)))
        
        phi_2 = []
        for i in range(0,20):
            phi_2.append(self.Yq(d_md[i], qa[i]))       
        
        phi_3 = []
        for i in range(0,20):
            phi_3.append(self.Yq(d_md[i], qb[i]))
            
        phi_4 = []
        for i in range(0,20):
            phi_4.append(self.Yq(d_md[i], qc[i]))
        #print(phi_2)
        phi_x = []
        phi_y = []
        phi_t = []

        for i in range(20):
            phi_x.append(phi_2[i]-phi_4[i])

        for i in range(20):
            phi_y.append(phi_3[i]-phi_4[i])
            
        for i in range(20):
            phi_t.append(phi_x[i]-phi_y[i])
        
#        phi_1 = []
#        for i in range(0,20):
#            phi_1.append(e.Yq(d_m[i],qt[i]))
        
#        r = [phi_2[19][25]-0.1,phi_2[0][0]+0.1]
        #----------------------------------------------------
        fig, ax0 = plt.subplots()
        ax0.plot(self.t,phi_t[9],color='black')
        for i in range(0,9):
            ax0.plot(self.t,phi_t[i],color='gray')
        for i in range(10,20):
            ax0.plot(self.t,phi_t[i],color='gray')
        
 #       ax0.set_ylim(r)
        ax0.grid(True)
        ax0.set_title(u'Representación grafica del tiempo vs fase'+ r'$( \varphi t)$')
        ax0.set_xlabel(r'$Tiempo $'+u' (min)')
        ax0.set_ylabel(r'$\Delta \varphi $'+  u' (°)')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/Fase(compensada)'+'.png')        
        #----------------------------------------------------
        fig, ax04 = plt.subplots()
        #ax04.set_ylim(r)
        ax04.plot(self.t,phi_2[9],color='black')
        for i in range(0,9):
            ax04.plot(self.t,phi_2[i],color='gray')
        for i in range(10,20):
            ax04.plot(self.t,phi_2[i],color='gray')
            
        ax04.grid(True)
        ax04.set_title(u'Representación grafica del tiempo vs fase ' + r'$( \varphi a)$')
        ax04.set_xlabel(r'$Tiempo (min)$')
        ax04.set_ylabel(r'$\Delta \varphi $'+  u' (°)')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/Fase(medicion1)'+'.png')
        #----------------------------------------------------
        fig, ax05 = plt.subplots()
        ax05.set_ylim([-3,3])
        ax05.plot(self.t,phi_3[9],color='black')
        for i in range(0,9):
            ax05.plot(self.t,phi_3[i],color='gray')
        for i in range(10,20):
            ax05.plot(self.t,phi_3[i],color='gray')
            
        ax05.grid(True)
        ax05.set_title(u'Representación grafica del tiempo vs fase ' + r'$( \varphi b)$')
        ax05.set_xlabel(r'$Tiempo (min)$')
        ax05.set_ylabel(r'$\Delta \varphi $'+  u' (°)')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/Fase(medicion1)'+'.png')
        #----------------------------------------------------      
        fig, ax06 = plt.subplots()
        ax06.set_ylim([-3,3])
        ax06.plot(self.t, phi_4[9],color='black')
        for i in range(0,9):
            ax06.plot(self.t, phi_4[i],color='gray')
        for i in range(10,20):
            ax06.plot(self.t, phi_4[i],color='gray')
            
        ax06.grid(True)

        ax06.set_title(u'Representación grafica del tiempo vs fase ' + r'$( \varphi c)$')
        ax06.set_xlabel(r'$Tiempo (min)$')
        ax06.set_ylabel(r'$\Delta \varphi $'+  u' (°)')
        fig.savefig('Graficas/'+'Interaccion/'+'Efectos_termicos/'+'Derivas/'+str(self.dt)+'/'+str(self.ft)+'/Fase(RC)'+'.png')
        #----------------------------------------------------   
    plt.show()
    
def ensayo_inicial(conf_celda, Vi, fss, ft, dt):
    tiempo_inicial = time()
    e = ensayo(conf_celda, Vi, fss, ft, dt)
    e.tf()
    e.mf()
    e.tm()
    e.tphi()
    print("Modelo generado")
    print("Tiempo requerido: %s segundos" % str(time()-tiempo_inicial))
    
def validacion_circuitos(numcto, f, fss, ft):
    #Datos reales de dos sensores piezoelectricos
    #Sensor 1
    S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}
    #Sensor 2
    S2 = {'Rq':22.729, 'Cq':28.923e-15, 'Lq':8.79823-3, 'Co':6.9469e-12}
    #Sensor 3
    S3 = {'Rq': 220, 'Co': 7e-12}
    
    c1 = circuito("medicion" + str(numcto), f, fss, ft, S1['Rq'], S1['Cq'], S1['Lq'], S1['Co'])
    c2 = circuito("referencia" + str(numcto), f, fss, ft, S2['Rq'], S2['Cq'], S2['Lq'], S2['Co'])
    c3 = circuito("rc" + str(numcto), f, fss, ft, S3['Rq'], 0, 0, S3['Co'])
    
    c1.graf_1()
    c2.graf_1()
    c3.graf_1()
    
    c1.graf_2()
    c2.graf_2()
    c3.graf_2()
    
    c1.graf_3()
    c2.graf_3()
    c3.graf_3()
    
    c1.graf_4()
    c2.graf_4()
    c3.graf_4()
    
    c1.graf_5()
    c2.graf_5()
    c3.graf_5()

#ensayo_inicial(conf_celda, Vi, fss, ft, dt)    
#conf_celda: 1. Xtal ref + Xtal Med 2. Xtal ref + Xtal Med + RC
#Vi: Frecuencia de la fuente
#fss: Cambio en la frecuencia fuente +/-fss
#ft: Cambio en la frecuencia Temperatura +/-ft
#dt: Derivas termicas 0, 5, 10, 100
    
#1.a
#ensayo_inicial(1, 10, 0, 0, 0)

validacion_circuitos(1,10,0,0)


S1 = {'Rq':18.191, 'Cq':28.911e-15, 'Lq': 8.8029e-3, 'Co':6.338e-12}
fs = 10*10**6
V       = 1                                             #Amplitud
Ws_0    = 2*pi*fs                                  #Frecuencia angular de la resonancia serie #len 20
#E_22    = 3.982e-11                                    #Permitividad del cuarzo
de      = 5.2e-3                                        #Diametro del electrodo
e_26    = 9.657e-2                                      #Constante de esfuerzo piezoeléctrico
C_66    = 2.947e10                                      #Modulo de cizalla efectivo
Pq      = 2.651e3                                       #Densidad del cuarzo
#Cp     = 2e-12                                         #Capacidad experna del crital de cuarzo
nq      = 9.27e-3                                       #Viscosidad efectiva del cuarzo

Vq      = sqrt(C_66/Pq)                                 #Tension de salida**
#K_0_2  = sqrt((e_26**2)/(E_22*C_66))                   #Factor de acoplamiento electromecánico efectivo sin perdidas
#hq      = Vq/(2*self.fs)                                #Espesor del crital de cuarzo #len 20
Aelec   = (pi*de**2)/4                                  #Area del electrodo
#C_0     = Aelec*E_22/hq                                #Capacidad estatica #len 20
#C_0x    = array([1]*1000)*Co#C_0 + Cp                  #Capacidad paralela total #len 20
#Cq      = array([1]*20)*Cq#(8*C_0*K_0_2**2)/(pi**2)#Componente capacitiva rama dinamica #len 20
#Ws     = (1/hq)*(sqrt(C_66/Pq))*sqrt((1*pi**2)-8*K_0_2)        
#Lq      = Lq#1/(Cq*Ws_0**2)                        #Inductancia equivalente del cristal a la frecuencia de resonancia #len 20
#Kt      = (hq**2)/(4*(e_26**2)*Aelec)
Kt     = (C_66*pi**2)/(4*Aelec*(e_26**2)*Pq*(Ws_0**2)) #Constante electroacustica #len 20
 
 
Pc      = 1000                                          #Densidad del recubrimiento agua g/m3
hc      = 1e-7                                          #Grosor del recubrimiento agua
Pl      = 1000                                          #Densidad del liquido en contacto con el cristal
nl      = 1e-3                                          #Viscosidad del liquido en contacto con el cristal
dl      = sqrt(2*nl/(Ws_0*Pl))                          #Profundidad de penetración de la onda acustica en el liquido #len 20

mco = Pc*hc
Lmlo = mco*Kt
Lmcl = (Pc*dl*Kt)/2
Lmo = S1['Lq'] + Lmlo + Lmcl

fos = 1/(2*pi*sqrt(Lmo*S1['Cq']))

#100 a 150 nm 1A Total 500
dhc = arange(0,1.5e-7,1e-10)
dmc = Pc*dhc
dLmco = Kt*dmc

fsload = 1/(2*pi*sqrt((Lmo+dLmco)*S1['Cq']))


d_f = [0, 0,  29,    59,  82, 102, 115, 121, 123, 124,   126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135]

ft = []
    
for i in range(1500):
    ft.append(fos-fsload[i])
L=[]
for i in range(1500):
    L.append(1/(((2*pi*fsload[i])**2)*S1['Cq']))
    
    
a = fos-fsload
#b = list(a.round())

#c = []
#for i in d_f:
#    try:
#        c.append(b.index(i))
#    except:
#        try:
#            c.append(b.index(i+1))
#        except:
#            c.append(b.index(i+0.5))

d = [0, 0, 0, 0, 0, 0, 15, 30, 42, 52, 58, 61, 62, 62, 62, 63, 64, 65, 66,66, 67, 67, 68, 68, 68]

k = []

for i in d:
    k.append(a[i])


q = Lmo+dLmco

Lx = []

for i in d:
    Lx.append(q[i])

