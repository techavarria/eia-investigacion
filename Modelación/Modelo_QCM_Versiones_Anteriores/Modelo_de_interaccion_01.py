# -*- coding: utf-8 -*-
"""
 ########################################################################################################
 #
 #  MODELO CRISTAL QCM
 #
 #  Universidad EIA Grupo de investigación GIBEC
 #
 #  Proyecto: QCM
 #  Director: Yeison Javier Montagut Ferizzola
 #  Investigador: Lucas Tobón Vanegas
 #
 #  E-mail: lucas.tobon@eia.edu.co
 #
 ########################################################################################################
 #
 #  Archivo:        Senal_medida_interaccion_antigeno_anticuerpo.py
 #
 #  Version python: 2.7    
 #
 #  Programador:    Lucas Tobon Vanegas
 #                  lucas.tobon.v@gmail.com
 #
 #  Descripción:    Señal máxima, medida con el oscilador, que registra la interacción conjugado de hapteno
 #                  Este archivo contiene los prototipos de funciones, tareas, variables, 
 #                  definiciones, tipos de datos y constantes de la interfaz grafica
 #
 #  Creado:         Fri Aug 19 08:29:57 2016
 #                  
 #  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 #                  26.07.2016 - Comentarios y correccion de errores ejecución
 #                  09.08.2016 - Afinación de modelo
 #
 #  Pendiente:      Comparar datos con grafica
 #                  
 #
 ########################################################################################################
 #
"""
from numpy import sqrt, pi, arange, array
import matplotlib.pyplot as plt
from scipy import stats
from Modelo_QCM_comp_01 import d_mc, d_fs, d_Q_3, d_LT, d_ff, f, dt, qt, qa_ph, qb_ph, qc_ph

t   = arange(0,13,0.5)
d_f = [  0,   0,     0,     0,   0,   0,     0,  29,    59,  82, 102, 115, 121, 
       123, 124, 126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135, 135]
       
def Ym(f):
    m  = stats.linregress(d_mc,d_fs)[0] #Pendiente 
    b  = stats.linregress(d_mc,d_fs)[1] #Intercepto    
    #f   = mfs*Ym + bfs
    Ym    = (f - b)/m
    return Ym
    
def Yq(d_m, q,i):
    m = stats.linregress(d_mc,qt[i])[0] #Pendiente
    b = stats.linregress(d_mc,qt[i])[1] #Intercepto
    Yq   = m*d_m + b
    return Yq
    
def YL(f):
    m = stats.linregress(d_ff,d_LT)[0] #Pendiente
    b = stats.linregress(d_ff,d_LT)[1] #Intercepto
    L   = m*f + b
    return L

deriv_t = {0:[0]*26,5:arange(0,5,0.195),10:arange(0,10,0.39),100:arange(0,100,3.9)} #Arreglos de derivas


fig, ax01 = plt.subplots()
ax01.plot(t,d_f)

fig, ax02 = plt.subplots()
d_m2 = Ym(d_f)
ax02.plot(d_m2,d_f)

fig, ax03 = plt.subplots()
ax03.plot(t,d_m2)


phi_1 = []
for i in range(0,20):
    phi_1.append(Yq(d_m2,qt,i))

fig, ax0 = plt.subplots()#******
for i in range(0,20):
    ax0.plot(t,phi_1[i])

phi_2 = []
for i in range(0,20):
    phi_2.append(Yq(d_m2,qa,i))

fig, ax04 = plt.subplots()#******
for i in range(0,20):
    ax04.plot(t,phi_2[i])
    
phi_3 = []
for i in range(0,20):
    phi_3.append(Yq(d_m2,qb,i))

fig, ax05 = plt.subplots()#******
for i in range(0,20):
    ax05.plot(t,phi_3[i])
    
phi_4 = []
for i in range(0,20):
    phi_4.append(Yq(d_m2,qc,i))

fig, ax06 = plt.subplots()#******
for i in range(0,20):
    ax06.plot(t,phi_4[i])




#########################################################################################################
##            EVALUACIÓN POR CAMBIO DE FRECUENCIA A PARTIR DEL CAMBIO DE IMPEDANCIA
#########################################################################################################
#fig, ax1 = plt.subplots()
#ax1.set_title(u'Representación grafica del cambio de frecuencia')
#d_f_2 = []
#for i in range(0,20):
#    d_f_2.append([])
#    for k in range(0,len(d_f)):
#        a = -f+i*f/10
#        d_f_2[i].append(d_f[k]+a+deriv_t[dt][k])
#for i in range(0,20):
# #   ax1 = axs[0]
#    a = i/20.0
#
#    ax1.plot(t,list(array(d_f_2[i])),color=(a,0,0))
#ax1.grid(True)
#
#ax1.set_xlabel(r'$Tiempo (minutos)$')
#ax1.set_ylabel(r'$\Delta Frecuencia (Hz)$')
#########################################################################################################
##                                      GRAFICA DELTA DE T - DELTA M
#########################################################################################################
#fig, ax2 = plt.subplots()
#ax2.set_title(u'Representación grafica del cambio de masa')
#d_m = Ym(array(d_f_2))
#for i in range(0,20):
#  #  ax2 = axs[1]
#    ax2.plot(t,list(-array(d_m[i])),color=(0,a,0))
#ax2.grid(True)
#ax2.set_xlabel(r'$Tiempo (minutos)$')
#ax2.set_ylabel(r'$\Delta Masa (ng)$')
#########################################################################################################
##                                      GRAFICA DELTA DE T - DELTA Phi
#########################################################################################################
#fig, ax3 = plt.subplots()
#ax3.set_title(u'Representación grafica del cambio de fase')
#d_q = Yq(array(d_m))
#for i in range(0,20):
#   # ax = axs[2]
#    ax3.plot(t,list(-array(d_q[i])),color=(0,0,a))
#    
#ax3.grid(True)
#ax3.set_xlabel(r'$Tiempo (minutos)$')
#ax3.set_ylabel(r'$\Delta \varphi (Grad)$')
#########################################################################################################
##                                      GRAFICA DELTA DE T - DELTA L
#########################################################################################################
#fig, ax4 = plt.subplots()
#ax4.set_title(u'Representación grafica del cambio en la inductancia')
#d_L = YL(array(d_f_2))
#for i in range(0,20):
#  #  ax = axs[3]
#    ax4.plot(t,list(-array(d_L[i])),color=(0,a,a))
#    
#ax4.grid(True)
#ax4.set_xlabel(r'$Tiempo (minutos)$')
#ax4.set_ylabel(r'$\Delta L (H)$')

plt.show()