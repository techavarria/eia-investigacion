# -*- coding: utf-8 -*-
"""
 ########################################################################################################
 #
 #  MODELO CRISTAL QCM
 #
 #  Universidad EIA Grupo de investigación GIBEC
 #
 #  Proyecto: QCM
 #  Director: Yeison Javier Montagut Ferizzola
 #  Investigador: Lucas Tobón Vanegas
 #
 #  E-mail: lucas.tobon@eia.edu.co
 #
 ########################################################################################################
 #
 #  Archivo:        modelo_2_qcm_00.py
 #
 #  Version python: 2.7    
 #
 #  Programador:    Lucas Tobon Vanegas
 #                  lucas.tobon.v@gmail.com
 #
 #  Descripción:    Este archivo contiene los prototipos de funciones, tareas, variables, 
 #                  definiciones, tipos de datos y constantes de la interfaz grafica
 #
 #  Creado:         Fri Aug 05 11:30:00 2016
 #                  
 #  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 #                  26.07.2016 - Comentarios y correccion de errores ejecución
 #                  09.08.2016 - Afinación de modelo
 #
 #  Pendiente:      Comparar datos con grafica
 #                  
 #
 ########################################################################################################
 #
"""
########################################################################################################
#                                        IMPORTACIÓN DE LIBRERIAS
########################################################################################################
from __future__ import division
from numpy import sqrt, pi, arange, arctan, array
import matplotlib.pyplot as plt
from cmath import phase
from scipy import stats
font = {'family': 'serif','color':  'darkred','weight': 'normal','size': 16,}

class circuito():
    
    def __init__(self, cto,fss, ft):
    
        ########################################################################################################
        #                                       ECUACIONES FASORIALES CIRCUITO CARGA (1a)
        ########################################################################################################
        ########################################################################################################
        #                                          CONSTANTES DEL CRISTAL
        ########################################################################################################
        fs      = fss*10**6#10e6                                #Frecuencia resonancia serie dinámica
        self.f       = ft                                       #frencuencia para analizar cambio termico e.g -10,10,1 (10-100)
        self.dt      = 0                                        #Deriva termica (Hz) 0, 5, 10, 100
        V       = 1                                             #Amplitud
        Ws_0    = 2*pi*fs                                       #Frecuencia angular de la resonancia serie
        E_22    = 3.982e-11                                     #Permitividad del cuarzo
        de      = 5.2e-3                                        #Diametro del electrodo
        e_26    = 9.657e-2                                      #Constante de esfuerzo piezoeléctrico
        C_66    = 2.947e10                                      #Modulo de cizalla efectivo
        Pq      = 2.651e3                                       #Densidad del cuarzo
        Cp      = 2e-12                                         #Capacidad experna del crital de cuarzo
        nq      = 9.27e-3                                       #Viscosidad efectiva del cuarzo
        
        Vq      = sqrt(C_66/Pq)                                 #Tension de salida**
        K_0_2   = sqrt((e_26**2)/(E_22*C_66))                   #Factor de acoplamiento electromecánico efectivo sin perdidas
        hq      = Vq/(2*fs)                                     #Espesor del crital de cuarzo 
        Aelec   = (pi*de**2)/4                                  #Area del electrodo
        C_0     = Aelec*E_22/hq                                 #Capacidad estatica
        C_0x    = C_0 + Cp                                      #Capacidad paralela total
        Cq      = (8*C_0*K_0_2**2)/(pi**2)                      #Componente capacitiva rama dinamica
        Lq      = 1/(Cq*Ws_0**2)                                #Inductancia equivalente del cristal a la frecuencia de resonancia
        Kt      = (C_66*pi**2)/(4*Aelec*(e_26**2)*Pq*(Ws_0**2)) #Constante electroacustica
        Rq      = 9.63                                          #Resistencia dinamica sin carga
        ########################################################################################################
        #                                           CONSTANTES DEL FLUIDO
        ########################################################################################################
        Pc      = 1000                                          #Densidad del recubrimiento agua
        hc      = 1e-7                                          #Grosor del recubrimiento agua
        Pl      = 1000                                          #Densidad del liquido en contacto con el cristal
        nl      = 1e-3                                          #Viscosidad del liquido en contacto con el cristal
        dl      = sqrt(2*nl/(Ws_0*Pl))                          #Profundidad de penetración de la onda acustica en el liquido
        ########################################################################################################
        #                                        CARGA CAPACITIVA E INDUCTIVA
        ########################################################################################################
        Rml     = (Kt*dl*Pl*Ws_0)/2                             #Contribución a la carga en la resistencia del sensor
        mc_0    = Pc*hc                                         #Densidad superficial de masa del recubrimiento
        Lmlc_0  = mc_0*Kt     
        Lml_1   = Pl*dl*Kt/2                                    #Inductancia dinamica del agua #Constribución a la carga en la inductancia
        Rm      = Rq + Rml                                      #Contribución de la carga en la resistencia dinámica del sensor
        Lm_0    = Lq + Lmlc_0 + Lml_1                           #Contribución total de la carga en la inductacia
        Cm      = Cq                                            #Capacidad dinámica del cristal sin carga
        f0s     = 1/(2*pi*sqrt(Lm_0*Cm))                        #Frecuencia de resonancia del filtro LC en paralelo con RC1
        ########################################################################################################
        #                                           
        ########################################################################################################
        mq      = nq*pi/(2*Vq)
        ml      = Pl*hc*1000000   
        #Rc      = Rm
        Rt      = Rm*10
        ########################################################################################################
        #                                        ITERACIÓN ECUACIONES MODELO
        ########################################################################################################
        self.d_hc    = arange(-5e-8,5e-8,1e-10)                      #Array de altura
        self.d_mc    = Pc*self.d_hc*1000000                               #Cambio en la masa deacuerdo al cambio en la altura
        self.d_Lmlc  = Kt*Pc*self.d_hc                                    #Cambio en la inductancia deacuerdo conel cambio de altura
        
        self.d_ff    = arange(-self.f,self.f,self.f/10)
        d_L     = 1/(((2*pi*(f0s + self.d_ff))**2)*Cq)
        self.d_LT    = d_L - Lm_0
        
        self.fs_load = 1/(2*pi*sqrt((Lm_0+self.d_Lmlc)*Cm))               #Frecuencia deacuerdo al cambio en la inductancia
                                                                               
        self.d_fs    = f0s-2*(f0s)*self.d_mc/(Aelec*sqrt(Pq*C_66))#-((d_Lmlc)/(2*Lm_0))*fs_load                  #Cambio en la frecuencia
        
        self.d_Q_4   = ((self.d_mc/(mq + ml))*(Rm/(Rm+Rm)))*360/(2*pi)    #Fase por ecuacion
        self.d_Q_3   = ((self.d_mc/(mq + ml))*(Rt/(Rt+Rm)))*360/(2*pi)    #Fase por ecuacion
        self.d_Q_2   = ((self.d_mc/(mq + ml)))*360/(2*pi)                 #Fase por ecuacion        
        ########################################################################################################
        ########################################################################################################        
        
        
        self.ct = cto        
        self.fss = fss
        self.ft = ft
        
        W       = 2*pi*f0s
        R       = Rm
        Lo      = Lm_0
        C       = Cq
        Co      = C_0x
        
        ZR1     = R*10 + 0j #R1 10 veces mayro
        ZRq     = R +0j

        if self.ct == "carga":
            ZLo     = W*Lo*1j
            ZdL     = W*self.d_Lmlc*1j
            ZdT     = W*self.d_LT*1j
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)
            
        elif self.ct == "referencia":
            ZLo     = W*Lo*1j
            ZdL     = [0]*1000#W*d_Lmlc*1j
            ZdT     = W*self.d_LT*1j
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)
        
        elif self.ct == "rc":
            ZLo     = 0#W*Lo*1j
            ZdL     = [0]*1000#W*d_Lmlc*1j
            ZdT     = [0]*20#W*d_LT*1j            

            ZCq     = 0#1j/(W*C)
            ZCo     = 1j/(W*Co)        
    
        self.Zm2 = []
        for i in arange(len(ZdT)):
            self.Zm2.append(ZRq + ZLo + ZdL - ZCq + ZdT[i])
    
        self.Zx2 = []
        for i in range(len(self.Zm2)):
            self.Zx2.append((self.Zm2[i]*ZCo)/(self.Zm2[i]+ZCo))
        
        self.Z       = []
        for i in range(0,20):
            self.Z.append([])
            for j in range(0,1000):
                self.Z[i].append(self.Zx2[i][j]/(self.Zx2[i][j]+ZR1))
            
        Vi      = V                             #Voltaje de entrada
        self.Vo = self.Z*Vi                     #Voltaje salida       
    
    def rama_dinamica(self):
        phzm2_2    = []
        mgzm2_2    = []
        for j in range(0,20):
            phzm2_2.append([])
            mgzm2_2.append([])
            for i in range(0,1000):
                phzm2_2[j].append(phase(self.Zm2[j][i])*360/(2*pi))
                mgzm2_2[j].append(sqrt((self.Zm2[j][i].real)**2 + (self.Zm2[j][i].imag**2)))
        
        return (phzm2_2, mgzm2_2)
        
    def rama_dcp(self):
        phzx_2    = []
        mgzx_2    = []
        for j in range(0,20):
            phzx_2.append([])
            mgzx_2.append([])
            for i in range(0,1000):
                phzx_2[j].append(phase(self.Zx2[j][i])*360/(2*pi))
                mgzx_2[j].append((sqrt((self.Zx2[j][i].real)**2 + (self.Zx2[j][i].imag)**2)))
        
        return (phzx_2, mgzx_2)
    
    def rama_circuito(self):
        phco_2    = []
        mgco_2    = []
        for j in range(0,20):
            phco_2.append([])
            mgco_2.append([])
            for i in range(0,1000):
                phco_2[j].append(phase(self.Vo[j][i])*360/(2*pi))
                mgco_2[j].append((sqrt((self.Vo[j][i].real)**2 + (self.Vo[j][i].imag)**2)))   
        
        return (phco_2,mgco_2)
            
    def graf_1(self):                   #GRAFICA DELTA Phi - DELTA M
        c1a = circuito(self.ct,self.fss,self.ft)
        
        fig, ax1 = plt.subplots()
        ax1.set_title(u'Representación grafica de la impedancia dinamica y Cto Cto: ' + self.ct)
        
        phzm = c1a.rama_dinamica()        
        l1, = ax1.plot(self.d_mc,phzm[0][10],'r')
        
        l2, = ax1.plot(self.d_mc,self.d_Q_3,'black')
        l3, = ax1.plot(self.d_mc,self.d_Q_2,'b')
        
        phzx = c1a.rama_dcp()
        l4, = ax1.plot(self.d_mc,phzx[0][10],'g')
        
        phco = c1a.rama_circuito()
        l5, = ax1.plot(self.d_mc,phco[0][10],'y')
        l6, = ax1.plot(self.d_mc,self.d_Q_4,'black')
        ax1.legend((l1,l2,l3,l4,l5,l6), (r'$\Delta \varphi Zm $',r'$\Delta \varphi a $',
                                         r'$\Delta \varphi b $',r'$\Delta \varphi cto $',r'$\Delta \varphi Zx $',r'$\Delta \varphi acto $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax1.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax1.set_ylabel(r'$\Delta \varphi (Grad)$')
        
        plt.show()
        
    def graf_2(self):                   #GRAFICA DELTA DE |Z| - DELTA M
        
        c1a = circuito(self.ct,self.fss,self.ft)
                        
        fig, ax2 = plt.subplots()
        ax2.set_title(u'Representación grafica de la amplitud en el Cto Cto: ' + self.ct)
        
        mgco = c1a.rama_circuito()        
        l1, = ax2.plot(self.d_mc, mgco[1][10], 'r')
        ax_c = ax2.twinx()
        
        mgzx = c1a.rama_dcp()
        l2, = ax_c.plot(self.d_mc, mgzx[1][10], 'b')
        
        mgzm = c1a.rama_dinamica()
        l3, = ax_c.plot(self.d_mc, mgzm[1][10], 'g')
    
        ax2.legend((l1, l2, l3), (r'$|Zcto| $', r'$|Zx| $',r'$|Zm| $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax2.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax2.set_ylabel(r'$\Delta |Zcto| (Voltios)$',color='r')
        
        plt.show()
        
    def graf_3(self):                   #GRAFICA DELTA DE M - DELTA Fs
        
        fig, ax3 = plt.subplots()
        ax3.set_title(u'Cambio de la frecuncia de resonancia Cto: ' + self.ct)
        ax3.plot(self.d_mc,self.d_fs,'b')
        ax3.grid(True)
        plt.xlabel(r'$\Delta M(ng)$')
        plt.ylabel(r'$\Delta F (Hz)$')
        
        plt.show()
        
    def graf_4(self):                   #GRAFICA DELTA DE F - DELTA L
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del cambio inductivo carga Cto: ' + self.ct)
        ax4.plot(self.d_fs,self.d_Lmlc,'b')
        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F (Hz)$')
        
        plt.show()
        
    def graf_5(self):                   #GRAFICA DELTA DE Phi - DELTA H
        
        c1a = circuito(self.ct,self.fss,self.ft)        
        
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del phi vs f Cto: ' + self.ct)
            
        phzm = c1a.rama_dinamica()
        
        a = []
        for i in range(500,1000):
            a.append(phzm[0][10][i])
        
        ax4.plot(self.d_hc,phzm[0][10],'b')
        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F (Hz)$')
        plt.show()
        
###############################################################################
class celda():

    def __init__(self,frec = 10, noice = 0.05):
        self.qa = circuito("carga", frec, noice)
        self.qb = circuito("referencia", frec, noice)
        self.qc = circuito("rc", frec, noice)
    
    def phase(self):  
        qa_ph = self.qa.rama_circuito()[0]
        qb_ph = self.qb.rama_dinamica()[0]
        qc_ph = self.qc.rama_dinamica()[0]
        
        qx = []
        qy = []
        qt = []
    
        for i in range(20):
            qx.append([])
            qy.append([])
            for j in range(1000):
                qx[i].append(qa_ph[i][j] - qc_ph[i][j])
                qy[i].append(qb_ph[i][j] - qc_ph[i][j])
        
        for i in range(20):
            qt.append([])
            for j in range(1000):
                qt[i].append(qx[i][j]-qy[i][j])
                
        return (qa_ph, qb_ph, qc_ph, qt)
###############################################################################
           
class ensayo():
    
    def __init__(self):
        ################################################################################
        #   Modelo de interacción
        ################################################################################
        self.t   = arange(0,13,0.5)
        self.d_f = [  0,   0,     0,     0,     0,   0,     0,  29,    59,  82, 102, 115, 121, 
                    123, 124,   126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135, 135]
        ################################################################################
        ################################################################################
    
    def Ym(self, f):
        c = circuito("carga", 10, 0.05)
        d_mc = c.d_mc
        m  = stats.linregress(d_mc, c.d_fs)[0] #Pendiente 
        b  = stats.linregress(d_mc,c.d_fs)[1] #Intercepto    
        Ym = (f - b)/m
        return Ym
        
    def Yq(self, d_m, q,i):
        c = circuito("carga", 10, 0.05)
        d_mc = c.d_mc
        m  = stats.linregress(d_mc, q[i])[0] #Pendiente
        b  = stats.linregress(d_mc, q[i])[1] #Intercepto
        Yq = m*d_m + b
        return Yq
        
    def YL(self, f):
        m = stats.linregress(self.d_ff, self.d_LT)[0] #Pendiente
        b = stats.linregress(self.d_ff, self.d_LT)[1] #Intercepto
        L = m*f + b
        return L
    
    #deriv_t = {0:[0]*26,5:arange(0,5,0.195),10:arange(0,10,0.39),100:arange(0,100,3.9)} #Arreglos de derivas
    def tf(self):    
    #----------------------------------------------------
        fig, ax01 = plt.subplots()
        ax01.plot(self.t, self.d_f)
        ax01.grid(True)
        ax01.set_title(u'Representación grafica del tiempo vs frecuencia')
    #----------------------------------------------------
    def mf(self):
        fig, ax02 = plt.subplots()
        self.d_m = ensayo.Ym(self.d_f)
        ax02.plot(self.d_m, self.d_f)
        ax02.grid(True)
        ax02.set_title(u'Representación grafica del masa vs frecuencia')
    #----------------------------------------------------
    def tm(self):
        fig, ax03 = plt.subplots()

        d_m = ensayo.Ym(self.d_f)
        ax03.plot(self.t, d_m)
        ax03.grid(True)
        ax03.set_title(u'Representación grafica del tiempo vs masa')
    #----------------------------------------------------
    def tphi(self):
        e = ensayo()
        d_m = e.Ym(self.d_f)
        c = celda()
        qa, qb, qc, qt = c.phase()
        
        phi_1 = []
        for i in range(0,20):
            phi_1.append(e.Yq(d_m,qt,i))
        
        fig, ax0 = plt.subplots()#******
        for i in range(0,20):
            ax0.plot(self.t,phi_1[i])
        
        ax0.grid(True)
        ax0.set_title(u'Representación grafica del tiempo vs fase')
        
        
        phi_2 = []
        for i in range(0,20):
            phi_2.append(e.Yq(d_m,qa,i))
        
        fig, ax04 = plt.subplots()#******
        ax04.set_ylim([0,6])
        for i in range(0,20):
            ax04.plot(self.t,phi_2[i])
        ax04.grid(True)
        ax04.set_title(u'Representación grafica del tiempo vs fase')
        
    
        phi_3 = []
        for i in range(0,20):
            phi_3.append(e.Yq(d_m,qb,i))
        
        fig, ax05 = plt.subplots()#******
        for i in range(0,20):
            ax05.plot(self.t,phi_3[i])
        ax05.grid(True)
        ax05.set_title(u'Representación grafica del tiempo vs fase')
        
        phi_4 = []
        for i in range(0,20):
            phi_4.append(e.Yq(d_m,qc,i))
        
        fig, ax06 = plt.subplots()#******
        for i in range(0,20):
            ax06.plot(self.t,phi_4[i])
        ax06.grid(True)
        ax06.set_title(u'Representación grafica del tiempo vs fase')
    #----------------------------------------------------
    plt.show()

