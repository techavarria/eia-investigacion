# -*- coding: utf-8 -*-
"""
 ########################################################################################################
 #
 #  MODELO CRISTAL QCM
 #
 #  Universidad EIA Grupo de investigación GIBEC
 #
 #  Proyecto: QCM
 #  Director: Yeison Javier Montagut Ferizzola
 #  Investigador: Lucas Tobón Vanegas
 #
 #  E-mail: lucas.tobon@eia.edu.co
 #
 ########################################################################################################
 #
 #  Archivo:        modelo_2_qcm_00.py
 #
 #  Version python: 2.7    
 #
 #  Programador:    Lucas Tobon Vanegas
 #                  lucas.tobon.v@gmail.com
 #
 #  Descripción:    Este archivo contiene los prototipos de funciones, tareas, variables, 
 #                  definiciones, tipos de datos y constantes de la interfaz grafica
 #
 #  Creado:         Fri Aug 05 11:30:00 2016
 #                  
 #  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 #                  26.07.2016 - Comentarios y correccion de errores ejecución
 #                  09.08.2016 - Afinación de modelo
 #                  23.11.2016 - Afinación de modelo
 #                  23.11.2016 - Control de versiones
 #          
 #  Pendiente:      Comparar datos con grafica
 #                  
 ########################################################################################################
 #
"""
########################################################################################################
#                                        IMPORTACIÓN DE LIBRERIAS
########################################################################################################
from __future__ import division
from numpy import sqrt, pi, arange, arctan, array
import matplotlib.pyplot as plt
from cmath import phase
from scipy import stats
from time import  time
font = {'family': 'serif','color':  'darkred','weight': 'normal','size': 16,}

class circuito:

    def __init__(self, cto):
        ########################################################################################################
        #                                       ECUACIONES FASORIALES CIRCUITO CARGA (1a)
        ########################################################################################################
        ########################################################################################################
        #                                          CONSTANTES DEL CRISTAL
        ########################################################################################################
        f = 1
        fss = 10
        ft = 10
        
        if f == 0:
            fs = array([fss*10**6]*20)  #len 20  
        elif f == 1:
            fs = arange((fss*10**6-fss),(fss*10**6+fss),fss/10) #10e6 #Frecuencia resonancia serie dinámica #len 20

        self.f  = ft                                            #frencuencia para analizar cambio termico e.g -10,10,1 (10-100)
        self.dt = 0                                             #Deriva termica (Hz) 0, 5, 10, 100
        
        V       = 1                                             #Amplitud
        Ws_0    = 2*pi*fs                                       #Frecuencia angular de la resonancia serie #len 20
        E_22    = 3.982e-11                                     #Permitividad del cuarzo
        de      = 5.2e-3                                        #Diametro del electrodo
        e_26    = 9.657e-2                                      #Constante de esfuerzo piezoeléctrico
        C_66    = 2.947e10                                      #Modulo de cizalla efectivo
        Pq      = 2.651e3                                       #Densidad del cuarzo
        Cp      = 2e-12                                         #Capacidad experna del crital de cuarzo
        nq      = 9.27e-3                                       #Viscosidad efectiva del cuarzo
        
        Vq      = sqrt(C_66/Pq)                                 #Tension de salida**
        K_0_2   = sqrt((e_26**2)/(E_22*C_66))                   #Factor de acoplamiento electromecánico efectivo sin perdidas
        hq      = Vq/(2*fs)                                     #Espesor del crital de cuarzo #len 20
        Aelec   = (pi*de**2)/4                                  #Area del electrodo
        C_0     = Aelec*E_22/hq                                 #Capacidad estatica #len 20
        C_0x    = C_0 + Cp                                      #Capacidad paralela total #len 20
        Cq      = (8*C_0*K_0_2**2)/(pi**2)                      #Componente capacitiva rama dinamica #len 20
        Lq      = 1/(Cq*Ws_0**2)                                #Inductancia equivalente del cristal a la frecuencia de resonancia #len 20
        Kt      = (C_66*pi**2)/(4*Aelec*(e_26**2)*Pq*(Ws_0**2)) #Constante electroacustica #len 20
        Rq      = 9.63                                          #Resistencia dinamica sin carga
        ########################################################################################################
        #                                           CONSTANTES DEL FLUIDO
        ########################################################################################################
        Pc      = 1000                                          #Densidad del recubrimiento agua
        hc      = 1e-7                                          #Grosor del recubrimiento agua
        Pl      = 1000                                          #Densidad del liquido en contacto con el cristal
        nl      = 1e-3                                          #Viscosidad del liquido en contacto con el cristal
        dl      = sqrt(2*nl/(Ws_0*Pl))                          #Profundidad de penetración de la onda acustica en el liquido #len 20
        ########################################################################################################
        #                                        CARGA CAPACITIVA E INDUCTIVA
        ########################################################################################################
        Rml     = (Kt*dl*Pl*Ws_0)/2                             #Contribución a la carga en la resistencia del sensor #len 20
        mc_0    = Pc*hc                                         #Densidad superficial de masa del recubrimiento
        Lmlc_0  = mc_0*Kt                                       #len 20
        Lml_1   = Pl*dl*Kt/2                                    #Inductancia dinamica del agua #Constribución a la carga en la inductancia #len 20
        Rm      = Rq + Rml                                      #Contribución de la carga en la resistencia dinámica del sensor #len 20
        Lm_0    = Lq + Lmlc_0 + Lml_1                           #Contribución total de la carga en la inductacia #len 20
        Cm      = Cq                                            #Capacidad dinámica del cristal sin carga
        self.f0s     = 1/(2*pi*sqrt(Lm_0*Cm))                   #Frecuencia de resonancia del filtro LC en paralelo con RC1 #len 20
        ########################################################################################################
        #                                           
        ########################################################################################################
        mq      = nq*pi/(2*Vq) 
        ml      = Pl*hc*1000000
        #Rc      = Rm
        Rt      = Rm*10
        ########################################################################################################
        #                                        ITERACIÓN ECUACIONES MODELO
        ########################################################################################################      
        
        self.d_hc    = arange(-5e-8,5e-8,1e-10)                           #Array de altura #len 1000
        self.d_mc    = Pc*self.d_hc*1000000                               #Cambio en la masa deacuerdo al cambio en la altura #len 1000
        
        self.d_Lmlc  = []
        for i in range(0,20):        
            self.d_Lmlc.append(Kt[i]*Pc*self.d_hc)                        #Cambio en la inductancia deacuerdo conel cambio de altura #len 20 1000
            
        if(self.f != 0):
            self.d_ff = arange(-self.f,self.f,self.f/10)                  #len 20
        else:
            self.d_ff = [0]*20                                            #len 20
        d_L = []
        self.d_LT = []
        
        for i in range(20):
            d_L.append(1/(((2*pi*(self.f0s[i] + self.d_ff[i]))**2)*Cq[i])) #len 20
        
        for i in range(20):
            self.d_LT.append(d_L[i] - Lm_0[i]) #len 20
        
        self.fs_load=[]
        for i in range(0,20):
            self.fs_load.append([])
            for j in range(0,1000):
                self.fs_load[i].append(1/(2*pi*sqrt((Lm_0[i] + self.d_Lmlc[i][j])*Cm[i])))               #Frecuencia deacuerdo al cambio en la inductancia
        
        self.d_fs = []
        for i in range(0,20):
            self.d_fs.append([])                                                                       
            for j in range(0,1000):            
                self.d_fs[i].append(self.f0s[i]-2*(self.f0s[i])*self.d_mc[j]/(Aelec*sqrt(Pq*C_66)))#-((d_Lmlc)/(2*Lm_0))*fs_load                  #Cambio en la frecuencia
        
        self.d_Q_4 = []
        self.d_Q_3 = []
        self.d_Q_2 = []
        
        for i in range(20):
            self.d_Q_4.append(((self.d_mc/(mq + ml))*(Rm[i]/(Rm[i]+Rm[i])))*360/(2*pi))    #Fase por ecuacion
            self.d_Q_3.append(((self.d_mc/(mq + ml))*(Rt[i]/(Rt[i]+Rm[i])))*360/(2*pi))    #Fase por ecuacion
            self.d_Q_2.append(((self.d_mc/(mq + ml)))*360/(2*pi))                          #Fase por ecuacion        
        
        ########################################################################################################
        ########################################################################################################        
        
        self.ct = cto        
        self.fss = fss
        self.ft = ft
        
        W       = 2*pi*self.f0s #len 20
        R       = Rm
        Lo      = Lm_0
        C       = Cq
        Co      = C_0x
        
        ZR1     = R*10 + 0j #R1 10 veces mayro
        ZRq     = R +0j
        if self.ct == "carga":
            ZLo     = W*Lo*1j
            ZdL = []
            ZdT = []
            for i in range(0,20):
                ZdT.append(W[i]*self.d_LT[i]*1j)
                ZdL.append([])
                for j in range(0,1000):
                    ZdL[i].append(W[i]*self.d_Lmlc[i][j]*1j)
                    
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)

        elif self.ct == "referencia":
            ZLo     = W*Lo*1j
            ZdL     = [[0]*1000]*20
            ZdT     = W*self.d_LT*1j
            
            ZCq     = 1j/(W*C)
            ZCo     = 1j/(W*Co)
        
        elif self.ct == "rc":
            ZLo     = [0]*20
            ZdL     = [[0]*1000]*20
            ZdT     = [0]*20          

            ZCq     = [0]*20
            ZCo     = 1j/(W*Co)        

        self.Zm2 = []
        for i in range(20):
            self.Zm2.append([])
            for j in range(1000):
                self.Zm2[i].append(ZRq[i] + ZLo[i] + ZdL[i][j] - ZCq[i] + ZdT[i])

        self.Zx2 = []
        for i in range(0,20):
            self.Zx2.append([])
            for j in range(0,1000):
                self.Zx2[i].append((self.Zm2[i][j]*ZCo[i])/(self.Zm2[i][j]+ZCo[i]))
        
        self.Z       = []
        for i in range(0,20):
            self.Z.append(self.Zx2[i]/(self.Zx2[i]+ZR1[i]))
        
        Vi      = V                             #Voltaje de entrada
        self.Vo = self.Z*Vi                     #Voltaje salida       
    
    def rama_dinamica(self):
        phzm2_2    = []
        mgzm2_2    = []

        for j in range(0,20):
            phzm2_2.append([])
            mgzm2_2.append([])
            for i in range(0,1000):
                phzm2_2[j].append(phase(self.Zm2[j][i])*360/(2*pi))
                mgzm2_2[j].append(sqrt((self.Zm2[j][i].real)**2 + (self.Zm2[j][i].imag**2)))
        
        return (phzm2_2, mgzm2_2)
        
    def rama_dcp(self):
        phzx_2    = []
        mgzx_2    = []
        for j in range(0,20):
            phzx_2.append([])
            mgzx_2.append([])
            for i in range(0,1000):
                phzx_2[j].append(phase(self.Zx2[j][i])*360/(2*pi))
                mgzx_2[j].append((sqrt((self.Zx2[j][i].real)**2 + (self.Zx2[j][i].imag)**2)))
        
        return (phzx_2, mgzx_2)
    
    def rama_circuito(self):
        phco_2    = []
        mgco_2    = []
        for j in range(0,20):
            phco_2.append([])
            mgco_2.append([])
            for i in range(0,1000):
                phco_2[j].append(phase(self.Vo[j][i])*360/(2*pi))
                mgco_2[j].append((sqrt((self.Vo[j][i].real)**2 + (self.Vo[j][i].imag)**2)))   
        
        return (phco_2,mgco_2)
        
    def graf_1(self):                   #GRAFICA DELTA Phi - DELTA M
        c1a = circuito(self.ct)
        
        fig, ax1 = plt.subplots()
        ax1.set_title(u'Representación grafica de la impedancia dinamica y Cto Cto: ' + self.ct)
        
        phzm = c1a.rama_dinamica()
        phzx = c1a.rama_dcp()
        phco = c1a.rama_circuito()

        for i in range(0,20):        
            l1, = ax1.plot(self.d_mc,phzm[0][i],'r')
            l4, = ax1.plot(self.d_mc,phzx[0][i],'g')
            l5, = ax1.plot(self.d_mc,phco[0][i],'y')
            l2, = ax1.plot(self.d_mc,self.d_Q_3[i],'black')
            l3, = ax1.plot(self.d_mc,self.d_Q_2[i],'b')
            l6, = ax1.plot(self.d_mc,self.d_Q_4[i],'black')

        ax1.legend((l1,l2,l3,l4,l5,l6), (r'$\Delta \varphi Zm $',r'$\Delta \varphi a $',
                                         r'$\Delta \varphi b $',r'$\Delta \varphi cto $',r'$\Delta \varphi Zx $',r'$\Delta \varphi acto $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax1.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax1.set_ylabel(r'$\Delta \varphi (Grad)$')
        
        plt.show()
        
    def graf_2(self):                    #GRAFICA DELTA DE |Z| - DELTA M
        
        c1a = circuito(self.ct)
                        
        fig, ax2 = plt.subplots()
        ax2.set_title(u'Representación grafica de la amplitud en el Cto Cto: ' + self.ct)
        
        mgco = c1a.rama_circuito()  
        mgzx = c1a.rama_dcp()
        mgzm = c1a.rama_dinamica()
        
        for i in range(20):
            l1, = ax2.plot(self.d_mc, mgco[1][i], 'r')
        
            ax_c = ax2.twinx()
        
            l2, = ax_c.plot(self.d_mc, mgzx[1][i], 'b')
        
            l3, = ax_c.plot(self.d_mc, mgzm[1][i], 'g')
    
        ax2.legend((l1, l2, l3), (r'$|Zcto| $', r'$|Zx| $',r'$|Zm| $'), loc='upper left',shadow = True)
        plt.grid(True)
        ax2.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax2.set_ylabel(r'$\Delta |Zcto| (Voltios)$',color='r')
        
        plt.show()
        
    def graf_3(self):                   #GRAFICA DELTA DE M - DELTA Fs
        
        fig, ax3 = plt.subplots()
        ax3.set_title(u'Cambio de la frecuncia de resonancia Cto: ' + self.ct)

        ax3.plot(self.d_mc,self.d_fs[9],color= 'black')
        for i in range(0,9):
            ax3.plot(self.d_mc,self.d_fs[i],color= 'gray')
        for i in range(10,20):
            ax3.plot(self.d_mc,self.d_fs[i],color= 'gray')
            
        ax3.grid(True)
        plt.xlabel(r'$\Delta M(ng)$')
        plt.ylabel(r'$\Delta F (Hz)$')
        
        plt.show()
        
    def graf_4(self):                   #GRAFICA DELTA DE F - DELTA L
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del cambio inductivo carga Cto: ' + self.ct)       
            
        ax4.plot(self.d_fs[9],self.d_Lmlc[9],color= 'black')
        for i in range(0,9):
            ax4.plot(self.d_fs[i],self.d_Lmlc[i],color= 'gray')
        for i in range(10,20):
            ax4.plot(self.d_fs[i],self.d_Lmlc[i],color= 'gray')
            
        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F (Hz)$')
        
        plt.show()
        
    def graf_5(self):                   #GRAFICA DELTA DE Phi - DELTA H
        
        c1a = circuito(self.ct)        
        
        fig, ax4 = plt.subplots()
        ax4.set_title(u'Representación grafica del phi vs f Cto: ' + self.ct)
            
        phzm = c1a.rama_dinamica()
        
        a = []
        for i in range(500,1000):
            a.append(phzm[0][10][i])
            
            
        ax4.plot(self.d_hc,phzm[0][9],color= 'black')
        for i in range(0,9):
            ax4.plot(self.d_hc,phzm[0][i],color= 'gray')
        for i in range(10,20):
            ax4.plot(self.d_hc,phzm[0][i],color= 'gray')
    
    
        ax4.grid(True)
        
        plt.ylabel(r'$\Delta L(H)$')
        plt.xlabel(r'$\Delta F (Hz)$')
        plt.show()
        
###############################################################################
class celda:
    
    def __init__(self):
        self.qa = circuito("carga")
        self.qb = circuito("referencia")
        self.qc = circuito("rc")
    
    def phase(self):  
        qa_ph = self.qa.rama_circuito()[0]
        qb_ph = self.qb.rama_dinamica()[0]
        qc_ph = self.qc.rama_dinamica()[0]
        
        qx = []
        qy = []
        qt = []
    
        for i in range(20):
            qx.append([])
            qy.append([])
            for j in range(1000):
                qx[i].append(qa_ph[i][j] - qc_ph[i][j])
                qy[i].append(qb_ph[i][j] - qc_ph[i][j])
        
        for i in range(20):
            qt.append([])
            for j in range(1000):
                qt[i].append(qx[i][j]-qy[i][j])
                
        return (qa_ph, qb_ph, qc_ph, qt)
###############################################################################
class ensayo:
    
    def __init__(self,frec=10, ruido=10):
        ################################################################################
        #   Modelo de interacción
        ################################################################################
        self.t   = arange(0,13,0.5)
        self.d_f = [  0,   0,     0,     0,     0,   0,     0,  29,    59,  82, 102, 115, 121, 
                    123, 124,   126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135, 135 ]
        ################################################################################
        ################################################################################
        self.c = circuito("carga")
        self.d_mc = self.c.d_mc    
    def Ym(self, f):
        m = []
        b = []
        for i in range(20):
            m.append(stats.linregress(self.d_mc, self.c.d_fs[i])[0]) #Pendiente 
            b.append(stats.linregress(self.d_mc, self.c.d_fs[i])[1]) #Intercepto    
        Ym = []    
        for i in range(20):
                Ym.append((f - b[i])/m[i])
        return Ym

    def Yq(self, d_m, q):
        m  = stats.linregress(self.d_mc, q)[0] #Pendiente
        b  = stats.linregress(self.d_mc, q)[1] #Intercepto
        Yq = m*d_m + b
        return Yq
        
    def YL(self, f):
        m = stats.linregress(self.c.d_ff, self.c.d_LT)[0] #Pendiente
        b = stats.linregress(self.c.d_ff, self.c.d_LT)[1] #Intercepto
        L = m*f + b
        return L
    #deriv_t = {0:[0]*26,5:arange(0,5,0.195),10:arange(0,10,0.39),100:arange(0,100,3.9)} #Arreglos de derivas
    def tf(self):    
    #----------------------------------------------------
        fig, ax = plt.subplots()
        ax.plot(self.t, self.d_f)
        ax.grid(True)
        ax.set_title(u'Representación grafica del tiempo vs frecuencia')
        ax.set_xlabel(r'$Tiempo (minutos)$')
        ax.set_ylabel(r'$\Delta F (Hz)$')
    #----------------------------------------------------
    def mf(self):
        fig, ax = plt.subplots()
        e = ensayo()
        self.d_m = e.Ym(self.d_f)
        ax.plot(self.d_m, self.d_f)
        ax.grid(True)
        ax.set_title(u'Representación grafica del masa vs frecuencia')
        ax.set_xlabel(r'$\Delta M_c (ng/mm^2)$')
        ax.set_ylabel(r'$\Delta F (Hz)$')
    #----------------------------------------------------
    def tm(self):
        fig, ax03 = plt.subplots()

        d_m = ensayo.Ym(self.d_f)
        ax03.plot(self.t, d_m)
        ax03.grid(True)
        ax03.set_title(u'Representación grafica del tiempo vs masa')
        ax03.set_xlabel(r'$Tiempo (minutos)$')
        ax03.set_ylabel(r'$\Delta M_c (ng/mm^2)$')
    #----------------------------------------------------
    def tphi(self):
        global phi_1
        e = ensayo()
        d_m = e.Ym(self.d_f)
        c = celda()
        qa, qb, qc, qt = c.phase()

        phi_2 = []
        for i in range(0,20):
            phi_2.append(e.Yq(d_m[i],qa[i]))

        r = [phi_2[19][25],phi_2[0][0]]        
        
        phi_1 = []
        for i in range(0,20):
            phi_1.append(e.Yq(d_m[i],qt[i]))
        
        fig, ax0 = plt.subplots()#******
        ax0.plot(self.t,phi_1[9],color= 'black')
        for i in range(0,9):
            ax0.plot(self.t,phi_1[i],color= 'gray')
        for i in range(10,20):
            ax0.plot(self.t,phi_1[i],color= 'gray')
            
        ax0.set_ylim(r)
        ax0.grid(True)
        ax0.set_title(u'Representación grafica del tiempo vs fase (qt)')
        ax0.set_xlabel(r'$Tiempo (minutos)$')
        ax0.set_ylabel(r'$\Delta \varphi (Grad)$')
        
        fig, ax04 = plt.subplots()#******
        ax04.set_ylim(r)
        
        ax04.plot(self.t,phi_2[9],color= 'black')
        for i in range(0,9):
            ax04.plot(self.t,phi_2[i],color= 'gray')
        for i in range(10,20):
            ax04.plot(self.t,phi_2[i],color= 'gray')
            
        ax04.grid(True)
        ax04.set_title(u'Representación grafica del tiempo vs fase (qa)')
        ax04.set_xlabel(r'$Tiempo (minutos)$')
        ax04.set_ylabel(r'$\Delta \varphi (Grad)$')
        
        phi_3 = []
        for i in range(0,20):
            phi_3.append(e.Yq(d_m[i],qb[i]))
        
        fig, ax05 = plt.subplots()#******
        
        ax05.plot(self.t,phi_3[9],color= 'black')
        for i in range(0,9):
            ax05.plot(self.t,phi_3[i],color= 'gray')
        for i in range(10,20):
            ax05.plot(self.t,phi_3[i],color= 'gray')
            
        ax05.grid(True)
        ax05.set_title(u'Representación grafica del tiempo vs fase (qb)')
        ax05.set_xlabel(r'$Tiempo (minutos)$')
        ax05.set_ylabel(r'$\Delta \varphi (Grad)$')
        
        phi_4 = []
        for i in range(0,20):
            phi_4.append(e.Yq(d_m[i],qc[i]))
        
        fig, ax06 = plt.subplots()#******
        
        ax06.plot(self.t,phi_4[9],color= 'black')
        for i in range(0,9):
            ax06.plot(self.t,phi_4[i],color= 'gray')
        for i in range(10,20):
            ax06.plot(self.t,phi_4[i],color= 'gray')
            
        ax06.grid(True)

        ax06.set_title(u'Representación grafica del tiempo vs fase (qc)')
        ax06.set_xlabel(r'$Tiempo (minutos)$')
        ax06.set_ylabel(r'$\Delta \varphi (Grad)$')
    #----------------------------------------------------
    plt.show()
    

def ensayo_inicial():
    tiempo_inicial = time()
    e = ensayo()
    e.tphi()
    print("Modelo generado")
    print("Tiempo requerido: %s segundos" % str(time()-tiempo_inicial))