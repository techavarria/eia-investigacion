# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 10:23:57 2016

@author: lucas.tobon
"""
import xlrd
import csv
import matplotlib.pyplot as plt
from numpy import sqrt, pi, arange, arctan, array

book = xlrd.open_workbook("Transient Analysis3.xls")
sh = book.sheet_by_index(0)

vi = []
vo = []
n      = sh.nrows

time  = []
t     = arange(0,n-2,1)
phase  = []

for j in range(0,21):
    vi.append([])
    for i in range(2,n):
        vi[j].append(sh.cell_value(rowx=i, colx=4+j))

for j in range(0,21):
    vo.append([])
    for i in range(2,n):
        vo[j].append(sh.cell_value(rowx=i, colx=26+j))
    
#for i in range(1,n):
#    time.append(sh.cell_value(rowx=i, colx=1))
    
#for i in range(0,23):
#    phase.append([])
#for i in range(0,n-1):
#    phase.append((vi[i]-vo[i]))    
    
#fig, ax2 = plt.subplots()
#for i in range(0,23):
#    ax2.plot(time,phase[i],'b')

#plt.grid(True)
    
fig, ax1 = plt.subplots()
#ax1.plot(t,vi,'b')
#ax1.plot(t,vo,'b')
#ax1.plot(t,phase,'b')

for i in range(0,21):
    for i in range(0,21):
        ax1.plot(t,vi[i],color = (i/23,0,i/23))
    
plt.grid(True)