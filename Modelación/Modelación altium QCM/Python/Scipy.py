# -*- coding: utf-8 -*-
"""
Created on Mon Sep 19 09:57:07 2016

@author: lucas.tobon
"""

import numpy as np
from scipy.interpolate import interp1d
import pylab as pl
import scipy as sc

measured_time = np.linspace(0, 1, 26)

measures = [  0,   0,     0,     0,   0,   0,     0,  29,    59,  82, 102, 115, 121, 
            123, 124, 126, 127.5, 129.5, 131, 132.5, 133, 133.5, 134, 134, 135, 135]

cubic_interp = interp1d(measured_time, measures, kind='cubic')
computed_time = np.linspace(0, 1, 50)
cubic_results = cubic_interp(computed_time)


pl.plot(computed_time, cubic_results, label='cubic interp')
pl.plot(measured_time, measures, label='cubic interp')
pl.legend()

