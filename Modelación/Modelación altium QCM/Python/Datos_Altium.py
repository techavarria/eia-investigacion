# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 10:23:57 2016

@author: lucas.tobon
"""
import xlrd
import matplotlib.pyplot as plt
from numpy import sqrt, pi, arange, arctan, array


from __future__ import division
from sympy import *


book = xlrd.open_workbook("Transient Analysis2.xls")
sh = book.sheet_by_index(0)

vi = []
vo = []
n      = sh.nrows

time  = []
t     = arange(0,n-1,1)
phase  = []

#for j in range(0,23):
#    vi.append([])
for i in range(1,n):
    vi.append(sh.cell_value(rowx=i, colx=sh.row_values(0).index('vi')))

#for j in range(0,23):
#    vo.append([])
for i in range(1,n):
    vo.append(sh.cell_value(rowx=i, colx=sh.row_values(0).index('vo')))
    
#for i in range(1,n):
#    time.append(sh.cell_value(rowx=i, colx=1))
    
#for i in range(0,23):
#    phase.append([])
for i in range(0,n-1):
    phase.append((vi[i]-vo[i]))    
    
#fig, ax2 = plt.subplots()
#for i in range(0,23):
#    ax2.plot(time,phase[i],'b')

#plt.grid(True)
dbode(system[, w, n])
sys = signal.TransferFunction([1], [1, 2, 3], dt=0.05)
fig, ax1 = plt.subplots()
ax1.plot(t,vi,'b')
ax1.plot(t,vo,'b')
ax1.plot(t,phase,'b')

#for i in range(0,23):
#for i in range(0,23):
#    ax1.plot(time,vo[i],color = (i/23,0,i/23))
    
plt.grid(True)