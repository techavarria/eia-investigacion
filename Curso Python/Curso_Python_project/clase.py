# Introduction
Garanje = "Ferrari", "Honda", "Mazda", "Nissan"

for cada_carro in Garanje:
    print(cada_carro)

# Print
print("can do this",5)
print('can do this',5)
#print('no se puede' esto')
print('se puede\' esto')

# Math
exponentes = 4**4
print(exponentes)

# Variables
var = 1
var_s = '1'

# While loop
condicion = 1

while condicion < 5:
    print(condicion)
    condicion +=1
#while True:
#    print('ciclo infinito')

# For loop
ejemplo = 2,3,4                 # igual a ejemplo = [2,3,4]

for x in ejemplo:
    print(x)

for y in range(1,6):
    print(y)

# if

# if else

# elif

# Funciones
def fun_ejemp():
    print('funcion ejemplo')
    z = 3+9
    print(z)
fun_ejemp()

# Parametros de funciones
def fun_para(n1,n2):
    resp = n1+n2
    print('n1 es ',n1)
    print(resp)
fun_para(7,2)

# Parametros definidos en funciones
def fun_def(par1, par2=5):
    print(par1,par2)
fun_def(2)

# Variables locales y globales
varia = 3
def funci():
    print(varia)
    print(varia+2)
    # varia+=6                                  # No funciona porque no se puede modificar la variable
def funci_g():
    global varia                                # Ya se puede modificar la variable porque quedó global
    varia+=6
    print(varia)
def funci_1(varia):                             # Otra opcion de poder usar una variable en una fun sin volverla global
    varia+=4
    print(varia)
    return varia
x = funci_1(varia)
print(x)

# Escribiendo en un archivo
texto = 'Texto de ejemplo para guardar \nNueva linea'

guardaArchivo = open('archivoEjem.txt','w')         # Abre archivo
guardaArchivo.write(texto)                          # Escribe el archivo
guardaArchivo.close()                               # Cierra el archivo

# Agregar a un archivo (append)
agregame = '\n mas info'

arch_append = open('archivoEjem.txt','a')
arch_append.write(agregame)
arch_append.close()

# Leer archivos
leerme = open('archivoEjem.txt','r').read()         # Lee archivos
print(leerme)
leerme = open('archivoEjem.txt','r').readlines()    # Lee a una lista
print(leerme)

# Clases
class Calculadora:
    def suma(self,x,y):
        self.z = x+y

    def resta(self,x,y):
        self.w = x-y
        print(w)

# resul = Calculadora.suma(3,4)                         # ERROR porque la clase no devuelve nada
# print(resul)

calculadora = Calculadora()                             # Forma ideal
calculadora.suma(5,7)
print(calculadora.z)


# Preguntas frecuentes
#!/usr/bin/python                                       # Para Linux permite ejecutar codigo como ejecutable

'''bloque de 
comentarios
se puede hacer 
asi'''
def funejemplo():
    print('hola')

if __name__ == '__main__':                              # Se ejecuta cuando se corre este programa(clase.py) no cuando se importa en otro archivo .py
    print('hello')

# Adquiriendo entradas por el usuario
# nombre = input('Escriba su nombre: ')
# print('Hola', nombre)
# Seleccioné tdo y con clic derecho comprimí tdo


# Modulo Estadistica
import statistics
lista_ejem = 6,5,3,6,8,9,1,7,4,7,9
x = statistics.mean(lista_ejem)
print(x)

# Sintaxis
import statistics as s              # abreviacion
print(s.mean(lista_ejem))
from statistics import mean
print(mean(lista_ejem))
from statistics import mean as m
print(m(lista_ejem))
from statistics import median as me, variance as v
from statistics import *

# Creando modulos
# Guardarlos en direccion lib y en la misma carpeta

# Listas vs Tuples # Tuples no se pueden modificar y listas si
x = 5,6,3,4     #tuple
y = (4,5,2,8)   #tuple
z = [4,2,1,1]   #lista
#x[1] = 1       #no se puede
z[1] = 1        #se puede

# Manipulcion de listas
g = [4,2,1,3,4,9]
g.append(100)       #agrega al final
g.insert(2,99)      #inserta 99 en la tercera casilla
g.remove(3)         #borra la casilla 4
print(g.count(4))   #cuenta cuantos 4s hay
g = ['Ene', 'Feb', 'Mar', 'Abr']
g.sort()
print(g)     #organiza la lista
g.reverse()
print(g)  #voltea la lista

# Listas multidimensionales
x = [[2,6],[6,2],[8,2],[5,12]]
print(x[2])
print(x[2][1])

# Diccionarios
exDict = {'Jack':[15,'blonde'],'Bob':[22, 'brown'],'Alice':[12,'black'],'Kevin':[17,'red']}
print(exDict['Jack'][1])
exDict['Tim'] = 14
print(exDict)

# Funciones ya creadas
#max min abs round int string float
'''
# Modulo OS
import os
direc = os.getcwd()
print(direc)
# os.mkdir('newDir')
import time
time.sleep(2)
os.rename('newDir','newDir2')
time.sleep(2)
os.rmdir('newDir')

# Modulo SYS (no me funciono)
import sys

sys.stderr.write('This is stderr text\n')
sys.stderr.flush()
sys.stdout.write('This is stdout text\n')

def main(arg):
    print(arg)

main(sys.argv[1])
'''
# urllib
import urllib.request
import urllib.parse

# x = urllib.request.urlopen('http://www.google.com/')
# print(x.read())
'''
url = 'http://pythonprogramming.net'
values = {'s':'basic','submit':'search'}

data = urllib.parse.urlencode(values)
data = data.encode('utf-8')
req = urllib.request.Request(url,data)
resp = urllib.request.urlopen(req)
respData = resp.read()
print(respData)

'''
try:
    x = urllib.request.urlopen('https://www.google.com/search?q=test')
    print(x.read())
except Exception as e:
    print(str(e))
# sale error y toca hacer lo siguiente

try:
    url = 'https://www.google.com/search?q=test'
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
    req = urllib.request.Request(url, headers=headers)
    resp = urllib.request.urlopen(req)
    respData = resp.read()

    saveFile = open('witgHeaders.txt','w')
    saveFile.write(str(respData))
    saveFile.close()
except Exception as e:
    print(str(e))

# Regular expressions re
'''
Identifiers:

\d = any number
\D = anything but a number
\s = space
\S = anything but a space
\w = any letter
\W = anything but a letter
. = any character, except for a new line
\b = space around whole words
\. = period. must use backslash, because . normally means any character.
Modifiers:

{1,3} = for digits, u expect 1-3 counts of digits, or "places"
+ = match 1 or more
? = match 0 or 1 repetitions.
* = match 0 or MORE repetitions
$ = matches at the end of string
^ = matches start of a string
| = matches either/or. Example x|y = will match either x or y
[] = range, or "variance"
{x} = expect to see this amount of the preceding code.
{x,y} = expect to see this x-y amounts of the precedng code
White Space Charts:

\n = new line
\s = space
\t = tab
\e = escape
\f = form feed
\r = carriage return
Characters to REMEMBER TO ESCAPE IF USED!

. + * ? [ ] $ ^ ( ) { } | \
Brackets:

[] = quant[ia]tative = will find either quantitative, or quantatative.
[a-z] = return any lowercase letter a-z
[1-5a-qA-Z] = return all numbers 1-5, lowercase letters a-q and uppercase A-Z

'''
import re
exampleString = '''
Jessica is 15 years old, and Daniel is 27 years old.
Edward is 97 years old, and his grandfather, Oscar, is 102. 
'''
ages = re.findall(r'\d{1,3}',exampleString)
names = re.findall(r'[A-Z][a-z]*',exampleString)
print(ages)
print(names)

ageDict = {}

x = 0
for eachName in names:
    ageDict[eachName] = ages[x]
    x+=1
print(ageDict)

#