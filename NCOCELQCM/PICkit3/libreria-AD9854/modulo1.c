//CODIGO PARA MODULO DE EVALUACIÓN
//!
//---------------------------Cabecera de dispositivo----------------------------
#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)
#include <usb_cdc.h>
#pragma config PBADEN = 0;


void main(){

    set_tris_B(0xFF); //Puerto b Como entrada 
    
    
    
    // Comunicación serial USB-------------------------------------------------
      usb_init_cs(); 
      while(1){
      usb_task(); 
      if (usb_enumerated()) 
      {
         //#pragma config PBADEN = 0;
         //int8 Reg_1D = input_B();     //REVISAR, CAMBIAR TIPO DE VARIABLE
         char B0 = input(PIN_B0);
         char B1 = input(PIN_B1);
         char B2 = input(PIN_B2);
         char B3 = input(PIN_B3);
         char B4 = input(PIN_B4);
         char B5 = input(PIN_B5);
         char B6 = input(PIN_B6);
         char B7 = input(PIN_B7);
         char puertoB = input_B();
         printf(usb_cdc_putc, "B0 = %x B1 = %x B2 = %x B3 = %x B4 = %x B5 = %x B6 = %x B7 = %x Puerto_B = %x\n\r",B0,B1,B2,B3,B4,B5,B6,B7,puertoB);
         output_high(PIN_D1);
         delay_ms(1000);
         output_low(PIN_D1);
         delay_ms(1000);
      }
      }

}
