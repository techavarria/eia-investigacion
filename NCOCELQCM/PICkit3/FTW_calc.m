function [ FTW, FTW_hex ] = FTW_calc( Frequency )
    sysclk = 100*10^6;
%     sysclk = 30*10^6;
    
    N = 48;

    FTW = round((Frequency*(2^N))/sysclk)
    FTW_hex = dec2hex(FTW,12)
end

