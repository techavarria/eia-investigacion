//CODIGO PARA MODULO DE EVALUACIÓN
//!
//---------------------------Cabecera de dispositivo----------------------------
#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)
#include <usb_cdc.h>
#pragma config PBADEN = 0;

// Funciones
//int freq_config(f1,f2,f3,f4,f5,f6)
//{

//}


void main()
{
   
   //Configuracion de puertos
   set_tris_a(0x00); //Puerto a Como salida
   set_tris_b(0x00); //Puerto b Como salida
   set_tris_e(0x00); //Puerto e Como salida
   
   output_e(0b00000001); // Por defecto WR = 1
   
   //Master Reset
   output_e(0b00000011);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(10);
   
   //Configuración de registros generales
   //-------------------------------------------------------------------------
   output_a(0x1D);
   output_b(0b00010111); //00010000
   
   //-------------------------------------------------------------------------
   output_a(0x1E);
   output_b(0b00100100);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x1F);
   output_b(0b00000001); //0 ext clk
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x20);
   output_b(0b01000000);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   
   //Configuración frecuencia
   
   //-------------------------------------------------------------------------
   output_a(0x04);
   output_b(0x02);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x05);
   output_b(0xDE);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x06);
   output_b(0x00);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x07);
   output_b(0xD1);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x08);
   output_b(0xB7);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //-------------------------------------------------------------------------
   output_a(0x09);
   output_b(0x17);
   
   output_e(0b00000100);
   delay_ms(1);
   output_e(0b00000101);
   delay_ms(1);
   //--------------------------------------------------------------------------
   // Final de escritura de registros
   output_high(PIN_D1);
   delay_ms(3000);
   output_low(PIN_D1);
   delay_ms(3000);
   
   

   output_e(0b00000101);
   delay_ms(1);
   
   // Lectura de registros -----------------------------------------------
   set_tris_b(0xFF); //Puerto b Como entrada
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x04);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Reg_1D = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x05);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Reg_1E = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x06);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Reg_1F = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x07);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Reg_20 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x04);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_04 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x05);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_05 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x06);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_06 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x07);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_07 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x08);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_08 = input_b();
   delay_ms(1);
   // ---
   output_e(0b00000101);
   delay_ms(1);
   output_a(0x09);
   delay_ms(10);
   output_e(0b00000001);
   delay_ms(1);
   char Freq_09 = input_b();
   delay_ms(1);
   // ---
   
   // Comunicación serial USB---------------------------------------------------
   usb_init_cs();
   while(1)
   {
      usb_task();
      if(usb_enumerated())
      {
         //printf(usb_cdc_putc, "1D = %x 1E = % x 1F = % x 20 = % x\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20);
         
         if(usb_cdc_kbhit())
         {
            int comando = usb_cdc_getc();
            if(comando == 1)
            {
               // enviar lectura de registros
            }
            if(comando == 2)
            {
               // leer frecuencia y configurarla
               int recib1 = usb_cdc_getc();
               int recib2 = usb_cdc_getc();
               int recib3 = usb_cdc_getc();
               int recib4 = usb_cdc_getc();
               
               while(1) // para probar que si esté llegando bien
               {
                  printf(usb_cdc_putc, "frec1 = %lx frec2 = %lx frec3 = %lx frec4 = %lx \n\r", recib1, recib2, recib3, recib4);
               }
            }
            
            /* Separar palabra
            int frec1, frec2, frec3, frec4;
            frec1 = freq&0xFF;
            frec2 = (freq>>8)&0xFF;
            frec3 = (freq>>16)&0xFF;
            frec4 = (freq>>24);
            while(1)
            {
               printf(usb_cdc_putc, "frec1 = %lx frec2 = % lx frec3 = % lx frec4 = % lx frecuencia = % lx\n\r", frec1, frec2, frec3, frec4, freq);
            }
            */
            
            
            output_high(PIN_D1);
            delay_ms(500);
            output_low(PIN_D1);
            delay_ms(500);
         }
      }

      //---------------------------------------------------------------------------
   }
}


