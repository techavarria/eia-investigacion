function [ Freq ] = Freq_calc( FTW )
%     sysclk = 30*10^6;
    sysclk = 100*10^6;
    
    N = 48;
    
    Freq = round((FTW*sysclk)/(2^N))
end