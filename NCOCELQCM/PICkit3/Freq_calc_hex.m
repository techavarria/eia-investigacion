function [ Freq ] = Freq_calc_hex( FTW_hex )
%     sysclk = 30*10^6;
    sysclk = 100*10^6;
    
    N = 48;
    
    FTW = hex2dec(FTW_hex);
    Freq = round((FTW*sysclk)/(2^N))
end