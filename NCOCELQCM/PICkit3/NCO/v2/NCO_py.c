//CODIGO PARA NCO
#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)
#include <usb_cdc.h>
#pragma config PBADEN = 0;


void freq_config(int f1,int f2,int f3,int f4,int f5,int f6)
{
   //Configuración frecuencia
   //-------------------------------------------------------------------------
   output_a(0x04);
   output_b(f1);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x05);
   output_b(f2);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x06);
   output_b(f3);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x07);
   output_b(f4);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x08);
   output_b(f5);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x09);
   output_b(f6);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //--------------------------------------------------------------------------
   // Final de escritura de registros
   
   // Quitar para aumentar velocidad de barrido
   //output_high(PIN_E0); // Cambiar a otro led en NCO
   //delay_ms(3000);
   //output_low(PIN_E0);
   //delay_ms(3000);
   
   return;
}
void read_reg()
{
   output_d(0b00000101);
   delay_us(1);
   
   // Lectura de registros -----------------------------------------------
   set_tris_b(0xFF); //Puerto b Como entrada
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1D);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1D = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1E);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1E = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1F);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1F = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x20);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_20 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x04);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_04 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x05);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_05 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x06);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_06 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x07);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_07 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x08);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_08 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x09);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_09 = input_b();
   delay_us(1);
   // ---
   
   printf(usb_cdc_putc, "%X,%X,%X,%X,%X,%X,%X,%X,%X,%X\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20, Freq_04, Freq_05, Freq_06, Freq_07, Freq_08, Freq_09);
   
   // ---
//!   output_d(0b00000101);
//!   delay_us(1);
//!   output_a(0x19);
//!   delay_us(10);
//!   output_d(0b00000001);
//!   delay_us(1);
//!   char Freq_19 = input_b();
//!   delay_us(1);
//!   printf(usb_cdc_putc, "%X,%X,%X,%X,%X,%X,%X,%X,%X,%X,%X\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20, Freq_04, Freq_05, Freq_06, Freq_07, Freq_08, Freq_09,Freq_19);
   
   output_high(PIN_E2);
   delay_ms(2000);
   output_low(PIN_E2);
   delay_ms(2000);
   
   return;
}

void main()
{
   //Configuracion de puertos
   set_tris_a(0x00); //Puerto a Como salida
   set_tris_b(0x00); //Puerto b Como salida
   set_tris_d(0x00); //Puerto d Como salida
   
   output_d(0b00000001); // Por defecto WR = 1
   
   //Master Reset
   output_d(0b00000011);
   delay_us(10);
   output_d(0b00000001);
   delay_us(10);
   
   
   //Configuración de registros generales
   //-------------------------------------------------------------------------
   output_a(0x1D);
   output_b(0b00010000); //00010000
   
   //-------------------------------------------------------------------------
   output_a(0x1E);
   output_b(0b00100100);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x1F);
   output_b(0b00000001); //0 ext clk
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x20);
   output_b(0b01000000);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   // Comunicación serial USB---------------------------------------------------
   usb_init_cs();
   while(1)
   {
      usb_task();
      if(usb_enumerated())
      {
         if(usb_cdc_kbhit())
         {
            int comando = usb_cdc_getc();//int
            if(comando == 1)
            {
               read_reg();
            }
            if(comando == 2)
            {
               int f1 = usb_cdc_getc();
               int f2 = usb_cdc_getc();
               int f3 = usb_cdc_getc();
               int f4 = usb_cdc_getc();
               int f5 = usb_cdc_getc();
               int f6 = usb_cdc_getc();
               freq_config(f1,f2,f3,f4,f5,f6);
            }
            /* Separar palabra
            int frec1, frec2, frec3, frec4;
            frec1 = freq&0xFF;
            frec2 = (freq>>8)&0xFF;
            frec3 = (freq>>16)&0xFF;
            frec4 = (freq>>24);
            while(1)
            {
               printf(usb_cdc_putc, "frec1 = %lx frec2 = % lx frec3 = % lx frec4 = % lx frecuencia = % lx\n\r", frec1, frec2, frec3, frec4, freq);
            }
            */
         }
      }
      output_high(PIN_E1);
      delay_ms(500);
      output_low(PIN_E1);
      delay_ms(500);
      //---------------------------------------------------------------------------
   }
}

