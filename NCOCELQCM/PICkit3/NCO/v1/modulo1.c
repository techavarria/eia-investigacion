//CODIGO PARA MODULO DE EVALUACIÓN
//!
//---------------------------Cabecera de dispositivo----------------------------
#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)
#include <usb_cdc.h>
#pragma config PBADEN = 0;
//#device adc = 10
//#include <usb_cdc.h>
void main()
{
   //setup_adc_ports (AN0) ;
   //setup_adc (ADC_CLOCK_INTERNAL) ;
   //set_adc_channel (0) ;
   /*
   D[7:0] ->> TODO PURTO B
   A[5:3] ->> PUERTO A[5:3]
   
   S / P SELECT ->> VDD
   MASTER RESET - >> PUERTO E1
   IORESET ->> PUERTO A2
   SD0  - >> PUERTO A1
   SD1  - >> PUERTO A0
   IO_UD_CLK - >> NC
   WR_NEGADO - >> PURTO E0
   RD_NEGADO - >> VDD
   NC  ->> NC
   FBH  - >> NC
   S_KEY  - >> NC
   
   */
   //Configuracion de puertos
   set_tris_a (0x00); //Puerto a Como salida
   set_tris_b (0x00); //Puerto b Como salida
   set_tris_d (0x00); //Puerto e Como salida
   
   output_d (0b00000001); // Por defecto WR = 1
   
   //Master Reset
   output_d (0b00000011) ;
   delay_ms (10) ;
   output_d (0b00000001) ;
   delay_ms (10) ;
   
   //Configuración de registros generales
   //-------------------------------------------------------------------------
   output_a (0x1D) ;
   output_b (0b00010000); //00010000
   
   //-------------------------------------------------------------------------
   output_a (0x1E) ;
   output_b (0b00100100) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x1F) ;
   output_b (0b00000001); //0 ext clk
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x20) ;
   output_b (0b01000000) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   
   //Configuración frecuencia
   //-------------------------------------------------------------------------
   output_a (0x04) ;
   output_b (0x00); //estaba 19
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x05) ;
   output_b (0xDC) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x06) ;
   output_b (0x33) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x07) ;
   output_b (0x72) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x08) ;
   output_b (0x1D) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   //-------------------------------------------------------------------------
   output_a (0x09) ;
   output_b (0x54) ;
   
   output_d (0b00000100) ;
   delay_ms (1) ;
   output_d (0b00000101) ;
   delay_ms (1) ;
   // Final de escritura de registros
   //--------------------------------------------------------------------------
   output_high (PIN_E1) ;
   delay_ms (10000) ;
   output_low (PIN_E1) ;
   delay_ms (10000) ;
   
   
   // Revisar la lectura
   output_d (0b00000101) ;    //cambio
   delay_ms (1) ;

   // Comunicación serial USB---------------------------------------------------
   usb_init_cs ();
   WHILE (1)
   {
      usb_task ();
      IF (usb_enumerated ())
      {
         // Lectura de registros -----------------------------------------------
         set_tris_b (0xFF); //Puerto b Como entrada
         output_d (0b00000101) ;
         delay_ms (1) ;
         output_a (0x1D) ;
         delay_ms (10);
         output_d (0b00000001) ;
         delay_ms (1) ;
         CHAR Reg_1D = input_b ();
         delay_ms (1);
         // ---
         output_d (0b00000101) ;
         delay_ms (1) ;
         output_a (0x1E) ;
         delay_ms (10);
         output_d (0b00000001) ;
         delay_ms (1) ;
         CHAR Reg_1E = input_b ();
         delay_ms (1);
         // ---
         output_d (0b00000101) ;
         delay_ms (1) ;
         output_a (0x1F) ;
         delay_ms (10);
         output_d (0b00000001) ;
         delay_ms (1) ;
         CHAR Reg_1F = input_b ();
         delay_ms (1);
         // ---
         output_d (0b00000101) ;
         delay_ms (1) ;
         output_a (0x20) ;
         delay_ms (10);
         output_d (0b00000001) ;
         delay_ms (1) ;
         CHAR Reg_20 = input_b ();
         delay_ms (1);
         // ---
         
         //---------------------------------------------------------------------
         
         printf (usb_cdc_putc, "1D = %x 1E = %x 1F = %x 20 = %x\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20);
         
         output_high (PIN_E1) ;
         delay_ms (1000) ;
         output_low (PIN_E1) ;
         delay_ms (1000) ;
      }
   }

   //---------------------------------------------------------------------------
}


