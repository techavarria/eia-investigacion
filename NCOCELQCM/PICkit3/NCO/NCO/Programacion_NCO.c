// NCO
//!
//---------------------------Cabecera de dispositivo----------------------------
#include <18F4550.h>

#fuses HSPLL,NOWDT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)

/* ADC
//#device adc = 10
//#include <usb_cdc.h>
*/
void main(){
    /* ADC
    //setup_adc_ports(AN0);
    //setup_adc(ADC_CLOCK_INTERNAL);
    //set_adc_channel(0);
    */
    
    /*Conexiones
    D[7:0]       ->> TODO PURTO B
    A[5:3]       ->> PUERTO A[5:3]
    
    S/P SELECT   ->> VDD
    MASTER RESET ->> PUERTO E1
    IORESET      ->> PUERTO A2
    SD0          ->> PUERTO A1
    SD1          ->> PUERTO A0
    IO_UD_CLK    ->> NC
    WR_NEGADO    ->> PURTO E0
    RD_NEGADO    ->> VDD
    NC           ->> NC
    FBH          ->> NC
    S_KEY        ->> NC
    
    */
    
    //Configuracion de puertos
    set_tris_a(0x00); //Puerto a Como salida
    set_tris_b(0x00); //Puerto b Como salida
    set_tris_d(0x00); //Puerto d Como salida
    set_tris_c(0b00111111);
      
    
    output_d(0b00000001); // Por defecto WR = 1
    
    //Master Reset
    output_d(0b00000011);
    delay_ms(10);
    output_d(0b00000001);
    delay_ms(10);
    
    //Configuración de registros generales
    //-------------------------------------------------------------------------
    output_a(0x1D);
    output_b(0b00010000);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x1E);
    output_b(0b00100100);//01000100
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------z
    output_a(0x1F);
    output_b(0b00000001); //0 ext clk
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x20);
    output_b(0b01000000);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    
    //Configuración frecuencia
    //-------------------------------------------------------------------------
    output_a(0x04);
    output_b(0x00);//estaba 19
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x05);
    output_b(0x00);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x06);
    output_b(0x38);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x07);
    output_b(0x5F);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x08);
    output_b(0x17);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //-------------------------------------------------------------------------
    output_a(0x09);
    output_b(0x12);
    
    output_d(0b00000000);
    delay_ms(1);
    output_d(0b00000001);
    delay_ms(1);
    //--------------------------------------------------------------------------
    output_e(0b00000010);
    delay_ms(1000);
    output_e(0b00000000);
    delay_ms(1000);
    output_e(0b00000010);
    
    while(1){
      output_e(0b00000100);
      delay_ms(500);
      output_e(0b00000000);
      delay_ms(500);
   }
   
}
