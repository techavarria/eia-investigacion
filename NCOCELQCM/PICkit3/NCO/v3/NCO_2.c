//CODIGO PARA NCO
#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)
#include <usb_cdc.h>
#pragma config PBADEN = 0;


void freq_config(int f1,int f2,int f3,int f4,int f5,int f6)
{
   //Configuración frecuencia
   //-------------------------------------------------------------------------
   output_a(0x04);
   output_b(0x00);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x05);
   output_b(0x20);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x06);
   output_b(0xC4);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x07);
   output_b(0x9B);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x08);
   output_b(0xA5);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x09);
   output_b(0xE3);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //--------------------------------------------------------------------------
   // Final de escritura de registros
   
   // Quitar para aumentar velocidad de barrido
   //output_high(PIN_E0); // Cambiar a otro led en NCO
   //delay_ms(3000);
   //output_low(PIN_E0);
   //delay_ms(3000);
   
   return;
}
void read_reg()
{
   output_d(0b00000101);
   delay_us(1);
   
   // Lectura de registros -----------------------------------------------
   set_tris_b(0xFF); //Puerto b Como entrada
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1D);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1D = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1E);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1E = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x1F);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_1F = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x20);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Reg_20 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x04);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_04 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x05);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_05 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x06);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_06 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x07);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_07 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x08);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_08 = input_b();
   delay_us(1);
   // ---
   output_d(0b00000101);
   delay_us(1);
   output_a(0x09);
   delay_us(10);
   output_d(0b00000001);
   delay_us(1);
   char Freq_09 = input_b();
   delay_us(1);
   // ---
   
   printf(usb_cdc_putc, "%X,%X,%X,%X,%X,%X,%X,%X,%X,%X\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20, Freq_04, Freq_05, Freq_06, Freq_07, Freq_08, Freq_09);
   
   // ---
//!   output_d(0b00000101);
//!   delay_us(1);
//!   output_a(0x19);
//!   delay_us(10);
//!   output_d(0b00000001);
//!   delay_us(1);
//!   char Freq_19 = input_b();
//!   delay_us(1);
//!   printf(usb_cdc_putc, "%X,%X,%X,%X,%X,%X,%X,%X,%X,%X,%X\n\r", Reg_1D, Reg_1E, Reg_1F, Reg_20, Freq_04, Freq_05, Freq_06, Freq_07, Freq_08, Freq_09,Freq_19);
   
   output_high(PIN_E2);
   delay_ms(2000);
   output_low(PIN_E2);
   delay_ms(2000);
   
   return;
}
void delta_config(int f1,int f2,int f3,int f4,int f5,int f6)
{
   //-------------------------------------------------------------------------
   output_a(0x10);
   output_b(0x00);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x11);
   output_b(0x00);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x12);
   output_b(0x01);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x13);
   output_b(0x0C);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x14);
   output_b(0x6f);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x15);
   output_b(0x7A);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   
}
void rate_config(int r1, int r2, int r3)
{
   //-------------------------------------------------------------------------
   output_a(0x1A);
   output_b(0x0F);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x1B);
   output_b(0x42);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x1C);
   output_b(0x3F);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   
}
void updateclk(int r1, int r2, int r3, int r4)
{
// config de io update clock -----------------------------------
   output_a(0x16);
   output_b(0x95);
               
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   
   output_a(0x17);
   output_b(0x02);
               
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   
   output_a(0x18);
   output_b(0xF8);
               
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   
   output_a(0x19);
   output_b(0xFF);
               
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   return;
}

void main()
{
   //Configuracion de puertos
   set_tris_a(0x00); //Puerto a Como salida
   set_tris_b(0x00); //Puerto b Como salida
   set_tris_d(0x00); //Puerto d Como salida
   
   output_d(0b00000001); // Por defecto WR = 1
   
   //Master Reset
   output_d(0b00000011);
   delay_us(10);
   output_d(0b00000001);
   delay_us(10);
   
   
   //Configuración de registros generales
   //-------------------------------------------------------------------------
   output_a(0x1D);
   output_b(0b00010000); //00010000
   
   //-------------------------------------------------------------------------
   output_a(0x1E);
   output_b(0b00100100);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x1F);
   output_b(0b00000001); //0 ext clk
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   output_a(0x20);
   output_b(0b01000000);
   
   output_d(0b00000100);
   delay_us(1);
   output_d(0b00000101);
   delay_us(1);
   //-------------------------------------------------------------------------
   
   
   // Comunicación serial USB---------------------------------------------------
   usb_init_cs();
   while(1)
   {
      usb_task();
      if(usb_enumerated())
      {
         if(usb_cdc_kbhit())
         {
            int comando = usb_cdc_getc();//int
            if(comando == 1)
            {             
               read_reg();
            }
            if(comando == 2)
            {
               delay_us(1);
               output_a(0x1F);
               output_b(0b00000001); //0 ext clk
               
               output_d(0b00000100);
               delay_us(1);
               output_d(0b00000101);
               delay_us(1);
               
               int f1 = usb_cdc_getc();
               int f2 = usb_cdc_getc();
               int f3 = usb_cdc_getc();
               int f4 = usb_cdc_getc();
               int f5 = usb_cdc_getc();
               int f6 = usb_cdc_getc();
               freq_config(f1,f2,f3,f4,f5,f6);
            }
            if(comando == 3)
            {
               //-------------------------------------------------------------------------
               /*
               delay_ms(1);
               output_a(0x1F);
               output_b(0b10000111); //0 ext clk
               
               output_d(0b00000100);
               delay_us(1);
               output_d(0b00000101);
               delay_us(1);
               */
               
               int f1 = usb_cdc_getc();
               int f2 = usb_cdc_getc();
               int f3 = usb_cdc_getc();
               int f4 = usb_cdc_getc();
               int f5 = usb_cdc_getc();
               int f6 = usb_cdc_getc();
               freq_config(f1,f2,f3,f4,f5,f6);
               delay_ms(1);
               int ff1 = usb_cdc_getc();
               int ff2 = usb_cdc_getc();
               int ff3 = usb_cdc_getc();
               int ff4 = usb_cdc_getc();
               int ff5 = usb_cdc_getc();
               int ff6 = usb_cdc_getc();
               delta_config(ff1,ff2,ff3,ff4,ff5,ff6);
               //poner dlay? probar antes si funciona
               //recibir dos registros de la programacion de updateclk y rate_config
               delay_ms(1);
               int rr1 = usb_cdc_getc();
               int rr2 = usb_cdc_getc();
               int rr3 = usb_cdc_getc();
               //rate_config();
               rate_config(rr1, rr2, rr3);
               
               delay_ms(1);
               int ucr1 = usb_cdc_getc();
               int ucr2 = usb_cdc_getc();
               int ucr3 = usb_cdc_getc();
               int ucr4 = usb_cdc_getc();
               
               //updateclk();
               updateclk(ucr1, ucr2, ucr3, ucr4);
               
               //-------------------------------------------------------------------------
               
               delay_ms(1);
               output_a(0x1F);
               output_b(0b10000111); //0 ext clk
               
               output_d(0b00000100);
               delay_us(1);
               output_d(0b00000101);
               delay_us(1);
               
            }
         }
      }
      output_high(PIN_E1);
      delay_ms(500);
      output_low(PIN_E1);
      delay_ms(500);
      //---------------------------------------------------------------------------
   }
}

