// NCO
//!
//---------------------------Cabecera de dispositivo----------------------------
#include <18F4550.h>
#device adc = 10
#fuses HSPLL,NOWDT,NOLVP,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)

//////#include <usb_cdc.h>

void main(){

   //////setup_adc_ports(AN0);
   //////setup_adc(ADC_CLOCK_INTERNAL);
   //////set_adc_channel(0);

    /*
    D[7:0]       ->> TODO PURTO B
    A[5:3]       ->> PUERTO A[5:3]
    
    S/P SELECT   ->> VDD
    MASTER RESET ->> PUERTO E1
    IORESET      ->> PUERTO A2
    SD0          ->> PUERTO A1
    SD1          ->> PUERTO A0
    IO_UD_CLK    ->> NC
    WR_NEGADO    ->> PURTO E0
    RD_NEGADO    ->> VDD
    NC           ->> NC
    FBH          ->> NC
    S_KEY        ->> NC
    
    */
    //Configuracion de puertos
    set_tris_a(0x00); //Puerto a Como salida
    set_tris_b(0x00); //Puerto b Como salida
    set_tris_d(0x00); //Puerto e Como salida
    set_tris_c(0b00111111);
    
    output_d(0b00000001); // Por defecto WR = 1
    
    //Master Reset
    output_d(0b00000011);
    delay_ms(10);
    output_d(0b00000001);
    delay_ms(10);
    
    //-------------------------------------------------------------------------
    while(1){
      output_e(0b11111111);
      //output_high(PIN_E2);
      output_a(0b11111111);
      output_b(0b11111111);
      output_d(0b11111111);    
      delay_ms(500);
      //output_low(PIN_E2);
      output_e(0b00000000);
      output_a(0b00000000);
      output_b(0b00000000);
      output_d(0b00000000);
      delay_ms(500);
   
   }
   
}
