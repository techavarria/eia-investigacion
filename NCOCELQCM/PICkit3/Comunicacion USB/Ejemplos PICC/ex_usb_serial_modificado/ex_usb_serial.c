/////////////////////////////////////////////////////////////////////////
////                                                                 ////
////                        ex_usb_serial.c                          ////
////                                                                 ////
//// A demonstration of the USB CDC API that is provided by CCS.     ////
//// The USB CDC API that CCS provides will create a virtual UART    ////
//// port.  USB CDC drivers are included with most versions of       ////
//// Microsoft Windows, and when properly loaded will create a COMx  ////
//// port from which you can write and read to your PIC device       ////
//// like any serial device that has a COMx port.                    ////
////                                                                 ////
//// This example creates a USB<->UART converter.  Open              ////
//// Hyperterminal to COM1 (or whatever COM port is your usual RS232 ////
//// serial COM port).  Plug the PIC to USB.  Open Hypertimernal to  ////
//// the new COM port that is the USB<->UART COM port (for this      ////
//// example say it is COM2).  Typing a character in COM1 will cause ////
//// it to be sent out COM2, and vice-versa.                         ////
////                                                                 ////
//// See usb_cdc.h for API documentation.                            ////
////                                                                 ////
//// This file is part of CCS's PIC USB driver code.  See USB.H      ////
//// for more documentation and a list of examples.                  ////
////                                                                 ////
/////////////////////////////////////////////////////////////////////////
////                                                                 ////
//// VERSION HISTORY                                                 ////
////                                                                 ////
//// March 5th, 2009:                                                ////
////   Cleanup for Wizard.                                           ////
////   PIC24 Initial release.                                        ////
////                                                                 ////
//// July 1st, 2005: Initial Release.                                ////
////                                                                 ////
/////////////////////////////////////////////////////////////////////////
////        (C) Copyright 1996,2005 Custom Computer Services         ////
//// This source code may only be used by licensed users of the CCS  ////
//// C compiler.  This source code may only be distributed to other  ////
//// licensed users of the CCS C compiler.  No other use,            ////
//// reproduction or distribution is permitted without written       ////
//// permission.  Derivative programs created using this software    ////
//// in object code form are not restricted in any way.              ////
/////////////////////////////////////////////////////////////////////////



#include <18F4550.h>
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay (clock=20000000)

#use rs232(UART1, baud=9600, errors)

#include <usb_cdc.h>


void main(void) 
{
   char c;

   usb_init_cs();


   while (TRUE) 
   {
      printf(usb_cdc_putc,"HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-HELLO-WORLD-DONE");

   }
}
