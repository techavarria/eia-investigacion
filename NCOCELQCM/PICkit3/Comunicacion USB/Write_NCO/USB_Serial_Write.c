#include "18F4550.h" 
#fuses HSPLL, NOWDT, NOPROTECT, NOLVP, NODEBUG, USBDIV, PLL5, CPUDIV1, VREGEN 
#use delay(clock=20000000)
#include <usb_cdc.h> 

void main(void) 
{ 
   BYTE i; 
   usb_init_cs(); 
    while (TRUE){ 
      usb_task(); 
      if (usb_enumerated()) 
      { 
         printf(usb_cdc_putc, "\n\rPPG");
         /*
         if (usb_cdc_kbhit()) 
         { 
            i = toupper(usb_cdc_getc()); 
            
            if (i == 'R') 
            { 
               printf(usb_cdc_putc, "Sending a R Back"); 
            } 
            
            if (i == 'W') 
            { 
               printf(usb_cdc_putc, "Sending a W Back"); 
            } 
         } */
         
         output_high(PIN_E2);
         delay_ms(500);
         output_low(PIN_E2);
         delay_ms(500);
      }
   }
} 

