#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 ************************************************************************************************
 *
 *  SISTEMA DE IDENTIFICACIÓN Y CONTROL PLANTA DE TEMPERATURA
 *
 *  Uniersidad EIA Grupo de investigación GIBEC
 *
 *  E-mail: lucas.tobon@eia.edu.co
 *
 *----------------------------------------------------------------------------------------------
 *
 *  Archivo:        HMI_00.py
 *
 *  Version python: 2.7    
 *
 *  Programador:    Lucas Tobon Vanegas
 *                  lucas.tobon.v@gmail.com
 *
 *  Descripción:    Este archivo contiene los prototipos de funciones, tareas, variables, 
 *                  definiciones, tipos de datos y constantes de la interfaz grafica
 *
 *  Creado:         Thu Jul 21 09:58:23 2016
 *                  
 *  Historia:       21.07.2016 - Estructura general de codigo clases y funciones
 *                  26.07.2016 - Comentarios y correccion de errores ejecución
 *
 *  Pendiente:      Perfiles de temperatura
 *                  Leer datos
 *
 ************************************************************************************************
 *
"""

import wx
import wx.lib.intctrl
import wx.lib.mixins.listctrl  as  listmix
import os
import matplotlib
import time as tim


matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas
import pylab
import csv
from array import array
import numpy as np

import serial.tools.list_ports
from sistema_lectura import SerialData as DataGen
#import Com_TE as com
import Registros_TE as R
import ads1256

###
TIEMPO_REFRESCAR_MS = 50
TASA_MUESTREO_MS = 50
RANGO_AUTOMATICO_X = 20000/TASA_MUESTREO_MS
RANGO_AUTOMATICO_Y = 100

tabla = [0,0]

###
#------------------------------------------------------------------------------
#----------------------------CLASE CONTROLES DE GRAFICA------------------------
#------------------------------------------------------------------------------
class BoundControlBox(wx.Panel):

    def __init__(self, parent, ID, label, initval):
        wx.Panel.__init__(self, parent, ID)
        self.value = initval
        box = wx.StaticBox(self, -1, label)
        sizer = wx.StaticBoxSizer(box, wx.HORIZONTAL)
        self.radio_auto = wx.RadioButton(self, -1, label="Auto", style=wx.RB_GROUP)
        self.radio_manual = wx.RadioButton(self, -1,label="Manual")
        self.manual_text = wx.TextCtrl(self, -1, size=(35,-1),value=str(initval),style=wx.TE_PROCESS_ENTER)

        self.Bind(wx.EVT_UPDATE_UI, self.on_update_manual_text, self.manual_text)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_text_enter, self.manual_text)

        manual_box = wx.BoxSizer(wx.HORIZONTAL)
        manual_box.Add(self.radio_manual, flag=wx.ALIGN_CENTER_VERTICAL)
        manual_box.Add(self.manual_text, flag=wx.ALIGN_CENTER_VERTICAL)

        sizer.Add(self.radio_auto, 0, wx.ALL, 10)
        sizer.Add(manual_box, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def on_update_manual_text(self, event):
        self.manual_text.Enable(self.radio_manual.GetValue())
    def on_text_enter(self, event):
        self.value = self.manual_text.GetValue()
    def is_auto(self):
        return self.radio_auto.GetValue()
    def manual_value(self):
        return self.value
class BoundControlBox2(wx.Panel):
    def __init__(self, parent, ID, label, initval_H, initval_L):
        wx.Panel.__init__(self, parent, ID)
        self.initval_H = initval_H
        self.initval_L = initval_L
        self.value_H = initval_H
        self.value_L = initval_L
        self.checked_H = False
        self.checked_L = False
        box = wx.StaticBox(self, -1, label)
        sizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        self.check_H = wx.CheckBox(self, -1, label="Alto")
        self.check_L = wx.CheckBox(self, -1, label="Bajo")

        self.manual_H = wx.TextCtrl(self, -1, size=(35, -1), value=str(initval_H), style=wx.TE_PROCESS_ENTER)
        self.manual_L = wx.TextCtrl(self, -1, size=(35, -1), value=str(initval_L), style=wx.TE_PROCESS_ENTER)



        self.Bind(wx.EVT_CHECKBOX, self.on_check_H, self.check_H)
        self.Bind(wx.EVT_CHECKBOX, self.on_check_L, self.check_L)

        High_box = wx.BoxSizer(wx.HORIZONTAL)
        High_box.Add(self.check_H, flag=wx.ALIGN_CENTER_VERTICAL)
        High_box.Add(self.manual_H, flag=wx.ALIGN_CENTER_VERTICAL)

        Low_box = wx.BoxSizer(wx.HORIZONTAL)
        Low_box.Add(self.check_L, flag=wx.ALIGN_CENTER_VERTICAL)
        Low_box.Add(self.manual_L, flag=wx.ALIGN_CENTER_VERTICAL)


        sizer.Add(High_box, 0, wx.ALL, 10)
        sizer.Add(Low_box, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def on_check_H(self, event):
        if not self.checked_H:
            self.value_H = self.manual_H.GetValue()
            self.manual_H.Enable(self.checked_H)
        else:
            self.value_H = self.initval_H
            self.manual_H.Enable(self.checked_H)
        self.checked_H = not self.checked_H

    def on_check_L(self, event):
        if not self.checked_L:
            self.value_L = self.manual_L.GetValue()
            self.manual_L.Enable(self.checked_H)
        else:
            self.value_L = self.initval_L
            self.manual_L.Enable(self.checked_L)
        self.checked_L = not self.checked_L


        #self.value = self.manual_H.GetValue()
    #     self.manual_text.Enable(self.radio_manual.GetValue())
    #
    # def on_text_enter(self, event):
    #     self.value = self.manual_text.GetValue()
    #
    # def is_auto(self):
    #     return self.radio_auto.GetValue()
    #
    # def manual_value(self):
    #     return self.value
#------------------------------------------------------------------------------
#----------------------------CLASE SISTEMA DE ENTRADA--------------------------
#------------------------------------------------------------------------------
class SystemInputBox(wx.Panel):

    def __init__(self, parent, ID, label, initval, minval, maxval):
        wx.Panel.__init__(self, parent, ID)

        self.value = initval

        box = wx.StaticBox(self, 0, label)
        sizer = wx.StaticBoxSizer(box, wx.HORIZONTAL)

        self.system_input = wx.lib.intctrl.IntCtrl(self,id=0,size=(50,20),value=initval,	min=minval,	max=maxval,	style=wx.TE_PROCESS_ENTER|wx.TE_CENTRE)

        self.Bind(wx.EVT_TEXT_ENTER, self.on_text_enter, self.system_input)

        sizer.Add(self.system_input, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)
    def on_text_enter(self, event):
        if self.system_input.GetValue() >= self.system_input.GetMin() and self.system_input.GetValue() <= self.system_input.GetMax():
            self.value = self.system_input.GetValue()
            self.system_input.SelectAll()
    def step_value(self):
        return float(self.value)
#-----------------------------------------------------------------------------
#----------------------------CLASE SISTEMA DE SALIDA---------------------------
#-----------------------------------------------------------------------------
class SystemOutputBox(wx.Panel):
    def __init__(self,parent,label,val):
        wx.Panel.__init__(self,parent)
        box_1 = wx.StaticBox(self,0,label)
        sizer_1 = wx.StaticBoxSizer(box_1, wx.HORIZONTAL)
        self.system_output = wx.StaticText(self,size=(50,30),label= str(val),style=wx.TE_PROCESS_ENTER|wx.TE_CENTRE)
        sizer_1.Add(self.system_output, 5, wx.ALL, 10)

        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
#-----------------------------------------------------------------------------
#----------------------------CLASE GRAFICA PRINCIPAL--------------------------
#-----------------------------------------------------------------------------
class PPLForm(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, title = 'SISTEMA PARA EL CONTROL TERMICO Y MONITOREO DE PLANTA')
        self.panel = wx.Panel(self, wx.ID_ANY)
        self.SetBackgroundColour('#E6E6E6')
        self.Maximize()
        #----------------ATRIBUTOS ESPECIALES DE CLASE-------------------------
        self.datagen = 0
        #self.State = self.datagen.next(0)

        self.tiempo_step = [0.0]
        self.step     = [0.0] #SetPoint (entrada)
        self.sysresp  = [0.0] #Respuesta del sistema  Temp (entrada)
        self.actuador = [0.0] #Corriente del actuador (entrada)
        self.sysref   = [0.0] #Sensor de temp exterior
        #self.errors   = [0.0]
        #self.perturb = [0.0]

        self.diffase = [0.0]
        self.difamp = [0.0]

        self.paused = False
        self.run    = False

        self.setpp = 0      #SetPoint (salida)
        self.errors = [0.0] #Error (salida)

        #self.per = 0

        self.tiempo_refresco = wx.Timer(self)
        self.tiempo_muestreo = wx.Timer(self)

        self.contador_tiempo = 0
        self.num_fila = 0
        self.var_cambio = 1
        self.t_estab = [0.0]
        self.unavar = 1
        self.otravar = 0
        self.bandera = 0
        self.var_i = 1

        self.temp_inicial = 30                          ### Ir Cambiando

        # --- Escalones de a 1 grado
        self.aumento_grados = self.temp_inicial + 1
        # self.aumento_grados = self.temp_inicial - 1  # Escalones de a 1 grado bajando

        # --- Escalon de 10 grados
        # self.aumento_grados = self.temp_inicial + 10
        # self.aumento_grados = self.temp_inicial - 10

        self.tiempo_estabilizacion = [0.0]              # Variable que se va a guardar para ver tiempo de estabilización


        self.var_fin = 0
        #-------------------------METODOS--------------------------------------
        self.crear_menu_superior()
        self.grafo_1()
        self.crear_panel_principal()
        self.crear_barra_estado()
        self.tiempo_refresco.Start(TIEMPO_REFRESCAR_MS)
        self.tiempo_muestreo.Start(TASA_MUESTREO_MS)
        #-------------------------EVENTOS--------------------------------------
        self.Bind(wx.EVT_TIMER, self.on_tiempo_refresco, self.tiempo_refresco)
        self.Bind(wx.EVT_TIMER, self.on_tiempo_muestreo, self.tiempo_muestreo)


    def crear_menu_superior(self):
        self.menubar = wx.MenuBar()
        menu_file    = wx.Menu()
        menu_control = wx.Menu()
        self.menu_comunica = wx.Menu()
        menu_soporte = wx.Menu()

        #----------------INSTANCIAMIENTO y LAYOUTDE OBJETOS MENU FILE----------
        f_ggrafico = menu_file.Append(wx.ID_PRINT, u"&Guardar Gráfico\tCtrl-G", "Guarda gráfico en un archivo")
        f_gdatos   = menu_file.Append(wx.ID_SAVE, u"&Guardar Datos\tCtrl-D","Guarda datos en archivo")
        f_limpiar  = menu_file.Append(wx.ID_CLEAR, u"&Limpia Gráfico\tCtrl-L", "Limpia la grafica de señales")
        menu_file.AppendSeparator()
        f_exit = menu_file.Append(wx.ID_EXIT, "&Salir\tCtrl-Q","Salir")
        #---------------------EVENTOS DE COMPONENTES MENU FILE-----------------
        self.Bind(wx.EVT_MENU, self.on_save_plot, f_ggrafico)
        self.Bind(wx.EVT_MENU, self.on_save_data, f_gdatos)
        self.Bind(wx.EVT_MENU, self.on_limpiar_plot, f_limpiar)
        self.Bind(wx.EVT_MENU, self.OnClose, f_exit)

        #----------------------------------------------------------------------
        #----------------INSTANCIAMIENTO y LAYOUTDE OBJETOS MENU CONTROL------
        c_tecnicas = menu_control.Append(wx.ID_INFO, u"&Tecnicas de control", "Seleccionar una tecnica de control")
        c_alarmas = menu_control.Append(wx.ID_INFO, u"&Alarmas", "Defina estado de alarmas")
        c_perfil = menu_control.Append(wx.ID_INFO, u"&Perfil de temperatura","Defina un perfil de temperatura")

        #---------------------EVENTOS DE COMPONENTES MENU CONTROL--------------
        self.Bind(wx.EVT_MENU, self.frametecnicas, c_tecnicas)
        self.Bind(wx.EVT_MENU, self.framealarmas, c_alarmas)
        #----------------------------------------------------------------------
        #----------------INSTANCIAMIENTO y LAYOUTDE OBJETOS MENU COMUNICACIÓN------
        com_puerto = self.menu_comunica.Append(wx.ID_INFO, u"&Puerto Serial", "Seleccionar un puerto para la comunicación")
        com_conectar = self.menu_comunica.Append(wx.ID_INFO, u"&Conectar", "Seleccionar un puerto para la comunicación")
        #---------------------EVENTOS DE COMPONENTES MENU COMUNICACIÓN--------------
        #self.Bind(wx.EVT_MENU, self.on_conectar, com_conectar)
        #----------------------------------------------------------------------
        self.menubar.Append(menu_file,"&ARCHIVO")
        self.menubar.Append(menu_control,"&CONTROL")
        self.menubar.Append(self.menu_comunica,u"&COMUNICACIÓN")
        self.menubar.Append(menu_soporte,"&SOPORTE")
        self.SetMenuBar(self.menubar)

    def crear_panel_principal(self):
        #-------------------INSTANCIAMIENTO DE OBJETOS-------------------------
        titulo_2 = wx.StaticText(self.panel, wx.ID_ANY, u'SISTEMA PARA EL CONTROL TERMICO Y MONITOREO DE PLANTA')
        font = wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL)
        titulo_2.SetFont(font)

        self.canvas_1        = FigCanvas(self.panel,wx.ID_ANY,self.fig_1)

        titulo_3             = wx.StaticText(self.panel, wx.ID_ANY, u'PERFILES DE TEMPERATURA')
        titulo_3.SetFont(font)

        #-----------





        self.t_label         = wx.ComboBox(self.panel, wx.ID_ANY, choices=['T1','T2','T3'],value="",size=(100,50))
        self.t_temp          = SystemInputBox(self.panel, wx.ID_ANY,u"Temp °C",0,0,100)
        self.t_value         = SystemInputBox(self.panel, wx.ID_ANY, "Tiempo seg",0,0,100)
        self.t_grad          = SystemInputBox(self.panel, wx.ID_ANY, "Grad %",0,0,100)
        self.t_anadir        = wx.Button(self.panel, wx.ID_ANY,u"AÑADIR", size=(100,70))
        self.boton_iniciar   = wx.Button(self.panel, wx.ID_ANY,u"INICIAR", size=(100,70))
        self.ln              = wx.StaticLine(self.panel,wx.ID_ANY,wx.Point(1,0), wx.Size(1,470),wx.VERTICAL)
        self.boton_conectar  = wx.Button(self.panel, wx.ID_ANY, size=(100,70))
        self.boton_pausar    = wx.Button(self.panel, wx.ID_ANY, size=(100,70))
        self.xmax_control    = BoundControlBox(self.panel, wx.ID_ANY, u"X máx", 50)
        self.ymax_control    = BoundControlBox(self.panel, wx.ID_ANY, u"Y máx", 90)

        #---------------------------------------------------    # Agregué esto
        self.boton_parar = wx.Button(self.panel, wx.ID_ANY,u"PARAR", size=(100,70))

        self.t_label.Hide()
        self.t_temp.Hide()
        self.t_value.Hide()
        self.t_grad.Hide()

        self.t_anadir.Hide()


        self.index = 0

        self.list_ctrl = EditableListCtrl(self.panel,wx.ID_ANY, size=(-1, 100),
                                          style=wx.LC_REPORT
                                                | wx.BORDER_SUNKEN | wx.ALIGN_LEFT
                                          )
        # self.list_ctrl.InsertColumn(0, 'Indice', width=50)
        self.list_ctrl.InsertColumn(0, '# pasos', width=50)
        self.list_ctrl.InsertColumn(1, 'Temp inicial', width=65)
        self.list_ctrl.InsertColumn(2, 'Duracion', width=65)


        btn = wx.Button(self.panel, label="Agregar fila",style=wx.ALIGN_LEFT,size = (100, 40))
        btn2 = wx.Button(self.panel, label="Guardar",style=wx.ALIGN_LEFT,size = (100, 40))
        btn3 = wx.Button(self.panel, label='Eliminar fila',style=wx.ALIGN_LEFT,size = (100, 40))

        btn2.Hide()

        btn.Bind(wx.EVT_BUTTON, self.add_line)
        btn2.Bind(wx.EVT_BUTTON, self.getColumn)
        btn3.Bind(wx.EVT_BUTTON, self.deleteRow)


        #--------------------------------------------





        self.o_grid          = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar Cuadrícula",style=wx.ALIGN_LEFT)
        self.o_setp          = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar Set Point",style=wx.ALIGN_LEFT)
        self.o_rsis          = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar R.Sistema",style=wx.ALIGN_LEFT)
        self.o_sctr          = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar S.Control",style=wx.ALIGN_LEFT)
        self.o_err           = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar Error",style=wx.ALIGN_LEFT)
        self.o_ref           = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar Refer", style=wx.ALIGN_LEFT)
        #self.o_per           = wx.CheckBox(self.panel, wx.ID_ANY, u"Ocultar Perturb",style=wx.ALIGN_LEFT)

        #self.setpoint     = SystemInputBox(self.panel, wx.ID_ANY,"SetPoint",0,0,100)
        self.l_step       = SystemOutputBox(self.panel, "Set Point ", 0)
        self.l_sysresp    = SystemOutputBox(self.panel, "R.Sistema", 0)
        self.l_actuador   = SystemOutputBox(self.panel, "S.Control", 0)
        self.l_errors     = SystemOutputBox(self.panel, "Error ", 0)
        self.l_sysref     = SystemOutputBox(self.panel, "Referencia ", 0)
        #self.l_perturb    = SystemOutputBox(self.panel,"Pertub",0)
        self.l_diffase = SystemOutputBox(self.panel, "Dif Fase ", 0)
        self.l_difamp = SystemOutputBox(self.panel, "Dif Amp ", 0)

        #---------------------EVENTOS DE COMPONENTES---------------------------
        self.Bind(wx.EVT_BUTTON,self.conectar,self.boton_conectar)
        self.Bind(wx.EVT_UPDATE_UI, self.act_estado_conectar, self.boton_conectar)
        #self.boton_conectar.SetBackgroundColour('#14d900')

        self.Bind(wx.EVT_BUTTON, self.iniciar, self.boton_iniciar)
        self.Bind(wx.EVT_BUTTON, self.parar, self.boton_parar)

        self.Bind(wx.EVT_BUTTON, self.anadir, self.t_anadir)                    # Se agregó
        self.Bind(wx.EVT_BUTTON, self.pausar, self.boton_pausar)
        self.Bind(wx.EVT_UPDATE_UI, self.act_estado_pausar, self.boton_pausar)

        self.Bind(wx.EVT_CHECKBOX, self.on_o_grid, self.o_grid)
        self.Bind(wx.EVT_CHECKBOX, self.on_o_setp, self.o_setp)
        self.Bind(wx.EVT_CHECKBOX, self.on_o_rsis, self.o_rsis)
        self.Bind(wx.EVT_CHECKBOX, self.on_o_sctr, self.o_sctr)
        self.Bind(wx.EVT_CHECKBOX, self.on_o_err, self.o_err)
        self.Bind(wx.EVT_CHECKBOX, self.on_o_ref, self.o_ref)
        #self.Bind(wx.EVT_CHECKBOX, self.on_o_per, self.o_per)
        #-----------------------LAYOUT INTERFAZ--------------------------------
        PPSizer      = wx.BoxSizer(wx.VERTICAL)
        #-------------------------------------------
        TituloSizer  = wx.BoxSizer(wx.HORIZONTAL)
        #-------------------------------------------
        GraficaSizer = wx.BoxSizer(wx.HORIZONTAL)
        #-------------------------------------------
        Input_Sizer  = wx.BoxSizer(wx.VERTICAL)
        #-------------------------------------------
        ControlSizer = wx.BoxSizer(wx.HORIZONTAL)
        #-------------------------------------------
        CheckSizer   = wx.BoxSizer(wx.VERTICAL)
        CheckSizer_2 = wx.BoxSizer(wx.VERTICAL)
        #-------------------------------------------
        PPSizer.AddSpacer(10)
        TituloSizer.Add(titulo_2, 0, wx.ALL, 5)
        #-------------------------------------------

        # sizer = wx.BoxSizer(wx.VERTICAL)

        # self.panel.SetSizer(sizer)


        Input_Sizer.Add(titulo_3,  0, wx.CENTER,5)
        Input_Sizer.Add(wx.StaticLine(self.panel,), 0, wx.ALL|wx.EXPAND, 10)
        Input_Sizer.Add(self.list_ctrl, 0, wx.ALL | wx.EXPAND, 5)
        Input_Sizer.AddSpacer(10)
        Input_Sizer.Add(btn, 0, wx.CENTER, 5)
        Input_Sizer.AddSpacer(10)
        Input_Sizer.Add(btn2, 0, wx.CENTER, 5)
        Input_Sizer.AddSpacer(10)
        Input_Sizer.Add(btn3, 0, wx.CENTER, 5)
        Input_Sizer.Add(self.t_label,  0, wx.CENTER, 5)
        Input_Sizer.Add(self.t_temp,   0, wx.CENTER, 5)
        Input_Sizer.Add(self.t_value,  0, wx.CENTER, 5)
        Input_Sizer.Add(self.t_grad,   0, wx.CENTER, 5)
        Input_Sizer.AddSpacer(10)
        Input_Sizer.Add(self.t_anadir, 0, wx.CENTER, 5)
        Input_Sizer.Add(wx.StaticLine(self.panel,), 0, wx.ALL|wx.EXPAND, 10)
        Input_Sizer.Add(self.boton_iniciar, 0, wx.CENTER, 5)
        Input_Sizer.AddSpacer(10)
        Input_Sizer.Add(self.boton_parar, 0, wx.CENTER, 5)

        #-------------------------------------------
        GraficaSizer.AddSpacer(10)
        GraficaSizer.Add(Input_Sizer)
        GraficaSizer.Add(self.ln, 0, wx.ALL, 5)
        #GraficaSizer.Add(wx.StaticLine(self.panel,), 0, wx.ALL|wx.EXPAND, 10)
        GraficaSizer.Add(self.canvas_1, 0, wx.CENTER, 5)
        #-------------------------------------------
        ControlSizer.Add(self.boton_conectar, 0, wx.ALL, 5)
        ControlSizer.Add(self.boton_pausar,   0, wx.ALL, 5)
        ControlSizer.Add(self.xmax_control,   0, wx.ALL, 5)
        ControlSizer.Add(self.ymax_control,   0, wx.ALL, 5)
        ControlSizer.Add(CheckSizer)
        ControlSizer.Add(CheckSizer_2)
#        ControlSizer.Add(self.setpoint, 0, wx.ALL, 5)
        ControlSizer.Add(self.l_step,     0, wx.ALL, 5)
        ControlSizer.Add(self.l_sysresp,  0, wx.ALL, 5)
        ControlSizer.Add(self.l_actuador, 0, wx.ALL, 5)
        ControlSizer.Add(self.l_errors,   0, wx.ALL, 5)
        ControlSizer.Add(self.l_sysref,   0, wx.ALL, 5)
        #ControlSizer.Add(self.l_perturb,  0, wx.ALL, 5)
        ControlSizer.Add(self.l_diffase, 0, wx.ALL, 5)
        ControlSizer.Add(self.l_difamp, 0, wx.ALL, 5)

        #-------------------------------------------
        CheckSizer.Add(self.o_grid,  0, wx.ALL, 5)
        CheckSizer.Add(self.o_setp, 0, wx.ALL, 5)
        CheckSizer.Add(self.o_rsis, 0, wx.ALL, 5)
        #-------------------------------------------
        CheckSizer_2.Add(self.o_sctr, 0, wx.ALL, 5)
        CheckSizer_2.Add(self.o_err, 0, wx.ALL, 5)
        CheckSizer_2.Add(self.o_ref, 0, wx.ALL, 5)
        #CheckSizer_2.Add(self.o_per, 0, wx.ALL, 5)
        #-------------------------------------------
        PPSizer.Add(TituloSizer, 0, wx.CENTER)
        PPSizer.Add(wx.StaticLine(self.panel, ), 0, wx.ALL|wx.EXPAND, 10)
        PPSizer.Add(GraficaSizer,0, wx.ALL|wx.CENTER, 5)
        PPSizer.Add(wx.StaticLine(self.panel, ), 0, wx.ALL|wx.EXPAND, 10)
        PPSizer.Add(ControlSizer, 0, wx.ALL|wx.CENTER, 5)
        #-------------------------------------------
        self.panel.SetSizer(PPSizer)
        PPSizer.Fit(self.panel)

        #----------------------------------------------------------------------

    #-------------------------------LO AGREGUÉ
    def add_line(self, event):

        self.list_ctrl.InsertStringItem(self.index, str(self.index + 1))
        self.list_ctrl.SetStringItem(self.index, 1, "Grad")
        self.list_ctrl.SetStringItem(self.index, 2, "Seg")
        self.index += 1

    def getColumn(self, event):
        """"""

        global tabla


        count = self.list_ctrl.GetItemCount()
        cols = self.list_ctrl.GetColumnCount()

        tabla = [[None]*cols]*count
        tabla = np.array(tabla)
        for row in range(count):
            for col in range(cols):
                ite = self.list_ctrl.GetItem(itemId=row, col=col)
                # print(int(ite.GetText()))
                tabla[row,col] = (int(ite.GetText()))
                # print(row,col)
                # print(tabla)
    def deleteRow(self, e):
        count = self.list_ctrl.GetItemCount()
        self.list_ctrl.DeleteItem(count-1)
        self.index -= 1
    # -----------------------------------------
    def grafo_1(self):
        self.dpi   = 100
        self.fig_1 = Figure(figsize=(12, 4.5),dpi=self.dpi)
        self.fig_1.set_facecolor('#E6E6E6')

        self.axes  = self.fig_1.add_subplot(111)
        #self.axes.set_axis_bgcolor('white') #comentarlo para raspberry
        #self.axes.set_title('Grafica de Control\n'.decode('utf-8'), size=10)

        x_label    = 'Tiempo (m)'
        pylab.setp(self.axes.set_xlabel(x_label), fontsize=10)
        pylab.setp(self.axes.set_ylabel('Temperatura %'.decode('utf-8')), fontsize=10)
        pylab.setp(self.axes.get_xticklabels(), fontsize=8)
        pylab.setp(self.axes.get_yticklabels(), fontsize=8)
        # print(336)
        self.plot_step     = self.axes.plot(self.step,linewidth=1,color=(0, 0, 0),label='Set Point')[0]#Set Point
        self.plot_sysresp  = self.axes.plot(self.step,linewidth=1,color=(1, 0, 0),label='R.Sistema')[0]#Respuesta del Sistema
        self.plot_actuador = self.axes.plot(self.step,linewidth=1,color=(1, 1, 0),label='S.Control')[0]#Señal de Control
        self.plot_errors   = self.axes.plot(self.step,linewidth=1,color=(1, 0, 1),label='Error')[0]#Error sistema con cero en 50%
        self.plot_sysref = self.axes.plot(self.step, linewidth=1, color=(0, 1, 0), label='Referencia')[0]
        #self.plot_perturb  = self.axes.plot(self.step,linewidth=1,color=(0, 1, 0),label='Perturb')[0]  #Perturbacion
        self.plot_diffase = self.axes.plot(self.step, linewidth=1, color=(1, 0, 0), label='Dif Fase (V)')[0]
        self.plot_difamp = self.axes.plot(self.step, linewidth=1, color=(1, 0, 0), label='Dif Amp (V)')[0]

        self.axes.legend(bbox_to_anchor=(1.0, 0.4), loc=3, frameon=False, labelspacing=1.0, prop={'size':7})
        self.fig_1.subplots_adjust(left=0.09, right=0.85, top=0.96, bottom=0.1)

    #---------------------------------------------------
    def crear_barra_estado(self):
        self.statusbar = self.CreateStatusBar()
        self.PushStatusText(u"Programa en Ejecución")
    #--------------------Pinta la grafica---------------
    def pint_graf(self):
        # print(336336)
        if self.xmax_control.is_auto():
            # xmax = len(self.sysresp) if len(self.sysresp) > 10 else 10 #RANGO_AUTOMATICO_X else RANGO_AUTOMATICO_X #Redimensiona la ventana cuando la señal supera el intervalo xmax inicial
            xmax = self.tiempo_step[-1] if self.tiempo_step[-1] > 0.5 else 0.5
        else:
            xmax = int(self.xmax_control.manual_value())

        if self.ymax_control.is_auto():
            ymax = self.sysresp[len(self.sysresp)-1] + 10 if self.sysresp[len(self.sysresp)-1] + 10 > RANGO_AUTOMATICO_Y else RANGO_AUTOMATICO_Y #Redimensiona la ventana cuando la señal supera el intervalo ymax inicial
        else:
            ymax = int(self.ymax_control.manual_value())

        self.axes.set_xbound(lower=0, upper=xmax)  # intervalo para x       ######
        self.axes.set_ybound(lower=0, upper=ymax)#intervalo para y

        self.axes.grid(False) if self.o_grid.IsChecked() else self.axes.grid(True, color='gray') #Muestra cuadricula si el check esta activo

        pylab.setp(self.axes.get_xticklabels(), visible=True)#self.cb_xlab.IsChecked())

        #Señal de setpoint
        if self.o_setp.IsChecked():
           self.plot_step.set_xdata([0])
           self.plot_step.set_ydata([0])
        else:
            # self.plot_step.set_xdata(np.arange(len(self.step)))  # Cambiar esto
            self.plot_step.set_xdata(np.array(self.tiempo_step))  # Cambiar esto
            self.plot_step.set_ydata(np.array(self.step))

        #Señal de respuesta del sistema
        if self.o_rsis.IsChecked():
           self.plot_sysresp.set_xdata([0])
           self.plot_sysresp.set_ydata([0])
        else:
            # self.plot_sysresp.set_xdata(np.arange(len(self.sysresp)))
            self.plot_sysresp.set_xdata(self.tiempo_step)
            self.plot_sysresp.set_ydata(np.array(self.sysresp))

        #Señal de control
        if self.o_sctr.IsChecked():
           self.plot_actuador.set_xdata([0])
           self.plot_actuador.set_ydata([0])
        else:
           # self.plot_actuador.set_xdata(np.arange(len(self.actuador)))
           self.plot_actuador.set_xdata(self.tiempo_step)
           self.plot_actuador.set_ydata(np.array(self.actuador))

        #Señal de error
        if self.o_err.IsChecked():
           self.plot_errors.set_xdata([0])
           self.plot_errors.set_ydata([0])
        else:
            # self.plot_errors.set_xdata(np.arange(len(self.errors)))
            self.plot_errors.set_xdata(self.tiempo_step)
            self.plot_errors.set_ydata(np.array(self.errors))

        #Señal de referencia
        # if self.o_err.IsChecked():  #### CREO QUE HAY UN ERROR DEBERIA SER o_ref
        if self.o_ref.IsChecked():  #### CREO QUE HAY UN ERROR DEBERIA SER o_ref

            self.plot_sysref.set_xdata([0])
            self.plot_sysref.set_ydata([0])
        else:
            # self.plot_sysref.set_xdata(np.arange(len(self.sysref)))
            self.plot_sysref.set_xdata(self.tiempo_step)
            self.plot_sysref.set_ydata(np.array(self.sysref))
        # #Señal de perturbación
        # if self.o_per.IsChecked():
        #    self.plot_perturb.set_xdata([0])
        #    self.plot_perturb.set_ydata([0])
        # else:
        #     self.plot_perturb.set_xdata(np.arange(len(self.perturb)))
        #     self.plot_perturb.set_ydata(np.array(self.perturb))

        # Dif de Fase
        if self.o_err.IsChecked():      # CAMBIAR ESTO!!!!!!!!!!!!!!!!!!!!!!
            self.plot_diffase.set_xdata([0])
            self.plot_diffase.set_ydata([0])
        else:
            # self.plot_errors.set_xdata(np.arange(len(self.errors)))
            self.plot_diffase.set_xdata(self.tiempo_step)
            self.plot_diffase.set_ydata(np.array(self.diffase))

        # Dif de Amp
        if self.o_err.IsChecked():
            self.plot_difamp.set_xdata([0])
            self.plot_difamp.set_ydata([0])
        else:
            # self.plot_errors.set_xdata(np.arange(len(self.errors)))
            self.plot_difamp.set_xdata(self.tiempo_step)
            self.plot_difamp.set_ydata(np.array(self.difamp))

        try:
            self.canvas_1.draw()
        except:
            print('tiempo_step')
            print(len(self.tiempo_step))
            print('step')
            print(len(self.step))
            print('sysresp')
            print(len(self.sysresp))
            print('actuador')
            print(len(self.actuador))
            print('errors')
            print(len(self.errors))
            print('sysref')
            print(len(self.sysref))
            print('dif fase')
            print(len(self.diffase))
            print('dif amp')
            print(len(self.difamp))
            print('---')

        #----------------------------------------------------------------------
        #self.l_step.system_output.SetLabel(str(10))#self.State[3]))
        #com.com(6,0,self.t_temp.step_value())
        self.l_step.system_output.SetLabel(str(abs(self.step[-1])))#self.t_temp.step_value()))#self.State[3]))
        self.l_sysresp.system_output.SetLabel(str(self.sysresp[-1]))#self.State[0]))
        self.l_actuador.system_output.SetLabel(str(self.actuador[-1]))#self.State[0]))
        self.l_errors.system_output.SetLabel(str(self.errors[-1]))#self.errors[len(self.errors)-1]))#self.perturb.step_value()))
        self.l_sysref.system_output.SetLabel(str(self.sysref[-1]))
        #self.l_perturb.system_output.SetLabel(str(50))#self.perturb[len(self.perturb)-1]))
        self.l_diffase.system_output.SetLabel(str(self.diffase[-1]))
        self.l_difamp.system_output.SetLabel(str(self.difamp[-1]))

 #-----------------------------------------------------------------------------
 #-----------------------------------------------------------------------------
 #-----------------------------------------------------------------------------
    def conectar(self,event):
        # cambiar nt por posix para raspberry
        if os.name == 'posix' and list(serial.tools.list_ports.comports()):
            p_disponibles = list(serial.tools.list_ports.comports())
            puerto = p_disponibles[0][0]
            print(puerto)
            self.datagen = DataGen(puerto)
            #self.State = self.datagen.next(0)
            self.run = not self.run
        else:
            wx.MessageBox(u"NO HAY DISPOSITIVOS CONECTADOS", "Info", style=wx.CENTER|wx.OK|wx.ICON_ERROR)
        self.eje_tiempo = tim.time()
    def act_estado_conectar(self, event):
        #label = "DESCONECTAR" if self.run==True else "CONECTAR"
        if self.run == True:
            label = "DESCONECTAR"
            #self.boton_conectar.SetBackgroundColour('#fe693b')
        else:
            label = "CONECTAR"
            #self.boton_conectar.SetBackgroundColour('#14d900')

        self.boton_conectar.SetLabel(label)
    def iniciar(self, event):
        self.ind_pasos = 0
        global tabla
        count = self.list_ctrl.GetItemCount()
        cols = self.list_ctrl.GetColumnCount()
        tabla = [[None] * cols] * count
        tabla = np.array(tabla)
        for row in range(count):
            for col in range(cols):
                ite = self.list_ctrl.GetItem(itemId=row, col=col)
                # print(int(ite.GetText()))
                tabla[row, col] = (int(ite.GetText()))
                # print(row,col)
                # print(tabla)

        # self.datagen.next('$R3=20\r')

        # self.datagen.next('$R16=1\r')
        self.datagen.next('$R16=1\r')

        self.datagen.next('$R23=1\r')
        # self.datagen.next('$R13=3\r')  # Probar con 4 o 6 Pi o Pid
        self.datagen.next('$R13=5\r')  # Probar con 4 o 6 Pi o Pid

        # self.datagen.next('$R0=' + str(self.t_temp.system_input.GetValue()) + '\r')
        # print(tabla)

        # self.datagen.next('$R0=' + str(tabla[0, 1]) + '\r')           # Funciona con los datos de la tabla ------------------------------------
        # print(tabla)
        self.datagen.next('$R0=' + str(self.temp_inicial) + '\r')

        self.datagen.next('$W\r')
        self.contador_tiempo = tim.time()
        self.bandera = 1

    def parar(self, event):
        self.datagen.next('$Q\r')

    def anadir(self, event):                    # Se agregó
        app = wx.App(False)
        frame = MyForm()
        frame.Show()
        app.MainLoop()

    def pausar(self, event):
        self.paused = not self.paused

    def act_estado_pausar(self, event):
        label = "REANUDAR" if self.paused==True else "PAUSAR"
        self.boton_pausar.SetLabel(label)

    def on_o_grid(self, event):
        self.pint_graf()
    def on_o_setp(self, event):
        self.pint_graf()
    def on_o_rsis(self, event):
        self.pint_graf()
    def on_o_sctr(self, event):
        self.pint_graf()
    def on_o_err(self, event):
        self.pint_graf()
    def on_o_ref(self, event):
        self.pint_graf()
    #def on_o_per(self, event):
    #    self.pint_graf()

    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png|SVG (*.svg)|*.svg"

        dlg = wx.FileDialog(self,message="Guardar gáfico como...",	defaultDir=os.getcwd(),defaultFile="Grafico_"+tim.strftime('Resp_Sistema_'+"%H.%M.%S--%d_%m_%Y"),wildcard=file_choices,style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if dlg.ShowModal() == wx.ID_OK:
            if (dlg.GetFilterIndex() == 0):
                dlg.SetFilename(dlg.GetFilename() + ".png")
            else:
                dlg.SetFilename(dlg.GetFilename() + ".svg")
            path = dlg.GetPath()
            self.canvas_1.print_figure(path, dpi=self.dpi*6)#3Resolucion de imagen
            self.flash_status_message("Guardado en %s" % path)

    def on_save_data(self, event):

        # --------------- Lo agregué
        fecha_hora = tim.strftime("%H.%M.%S--%d_%m_%Y")
        # pat = 'D:\Documentos\EIA Investigación\HMI_modificado\Datos'
        # pat = pat.decode("utf8")
        # f = open(pat + '\T_Estab_' + str(fecha_hora) + '.txt', 'w')
        # del self.t_estab[0]
        # for elements in self.t_estab:
        #     f.write(str(elements) + '\n')
        #
        # f.close()


        file_choices = "CSV (*.csv)|*.csv"

        dlg = wx.FileDialog(self,message="Guardar datos como...",defaultDir=os.getcwd(),defaultFile=tim.strftime('Resp_Sistema_'+"%H.%M.%S--%d_%m_%Y"),wildcard=file_choices,style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            with open(path, 'w') as fp:
                a = csv.writer(fp, delimiter=';')
                # times = array('i',[])
                # k = 0
                # for time in self.sysresp:
                #     times.append(TASA_MUESTREO_MS*k)
                #     k=k+1
                v_tiemp = array('f',np.array(self.tiempo_step))
                setps = array('f',np.array(self.step))
                rsiss = array('f',np.array(self.sysresp))
                sctrs = array('f',np.array(self.actuador))
                errs  = array('f',np.array(self.errors))
                refs  = array('f',np.array(self.sysref))
                #pers  = array('f',np.array(self.perturb))
                # t_esta = array('f',np.array(self.tiempo_estabilizacion))
                diffase = array('f', np.array(self.diffase))
                difamp = array('f', np.array(self.difamp))

                k = 0
                a.writerows([['Tiempo (ms)','Escalon','Respuesta','Actuador','Error','Referencia','Dif Fase','Dif Amp']])#,'Perturbacion']])
                for step in setps:
                    a.writerows([[v_tiemp[k],setps[k],rsiss[k],sctrs[k],errs[k],refs[k],diffase[k],difamp[k]]])#,pers[k]]])
                    k=k+1
            self.flash_status_message("Guardado en %s" % path)

    def on_limpiar_plot(self, event):
        self.step     = [0.0]
        self.sysresp  = [0.0]
        self.actuador = [0.0]
        self.errors   = [0.0]
        self.sysref   = [0.0]
        #self.perturb  = [0.0]
        self.diffase = [0.0]
        self.difamp = [0.0]

    def on_tiempo_refresco(self, event):                            # Cuenta el tiempo para los perfiles
        global tabla
        if not self.paused and self.run:
            self.pint_graf()
            '''
        # Funciona con la tabla poniendo los valores--------------------------------------------------------------------
        # if (self.contador_tiempo != 0) and (self.var_cambio == 1):
        #     print(tim.time()-self.contador_tiempo)
        #     if ((tim.time()-self.contador_tiempo) > tabla[self.num_fila,2]):
        #         print(len(tabla))
        #         if self.num_fila < (len(tabla)-1):
        #             self.datagen.next('$R16=1\r')
        #             self.datagen.next('$R23=1\r')
        #             self.datagen.next('$R13=6\r')
        #             self.datagen.next('$R0=' + str(tabla[self.num_fila+1, 1]) + '\r')
        #             self.datagen.next('$W\r')
        #             self.contador_tiempo = tim.time()
        #             self.num_fila += 1
        #         else:
        #             self.datagen.next('$Q\r')
        #             self.var_cambio = 0
        #             # self.paused = not self.paused
        # --------------------------------------------------------------------------------------------------------------

        # Prueba subiendo de a un minuto

        #Para ir subiendo de a uno cuando el error sea menor a un valor-------------------------------------------------
        # if (self.contador_tiempo != 0) and (self.var_cambio == 1):
        #     print(tim.time()-self.contador_tiempo)
        #     self.tiempo_estabilizacion.append(float(tim.time() - self.contador_tiempo))
        #     if self.errors[len(self.errors)-1] < -1:#0.1:   #con -1 nunca sube el set point
        #         self.datagen.next('$R16=1\r')
        #         self.datagen.next('$R23=1\r')
        #         self.datagen.next('$R13=5\r')#5 es pd
        #         self.datagen.next('$R0=' + str(round(self.aumento_grados)) + '\r')
        #         self.datagen.next('$W\r')
        #         self.t_estab.append(float(tim.time() - self.contador_tiempo))
        #         self.contador_tiempo = tim.time()
        #         #--- Escalones de a 1 grado
        #         self.aumento_grados += 1
        #         # self.aumento_grados -= 1
        #
        #         #--- 1 Escalon de 10 grados
        #         # self.aumento_grados += 10
        #         # self.aumento_grados -= 10
        #         print(tim.time() - self.contador_tiempo)
        #
        # if self.aumento_grados > self.temp_inicial + 11:  # Escalones de a 1 Grado
        # # if self.aumento_grados < self.temp_inicial - 11:  # Escalones de a 1 Grado
        #     self.datagen.next('$Q\r')
        #---------------------------------------------------------------------------------------------------------------

        # Para ir subiendo de a uno cuando pase un tiempo----------------------------------------------------
        if (self.bandera == 1):
            print(tim.time()-self.contador_tiempo) #quitar para raspberry pi
            self.num_pasos = tabla[0, 0]
            self.duracion = tabla[0, 2]
            self.datagen.next('$R0=' + str(tabla[0, 1]) + '\r') #primera temperatura tabla
            # if (tim.time() - self.contador_tiempo > self.duracion * 60 * self.var_i):
            if (tim.time() - self.contador_tiempo > 600 * self.var_i):

                self.datagen.next('$R16=1\r')
                self.datagen.next('$R23=1\r')
                self.datagen.next('$R13=5\r')
                self.datagen.next('$R0=' + str(round(self.aumento_grados)) + '\r')
                self.datagen.next('$W\r')
                if self.unavar == 1:
                    self.aumento_grados += 1
                if self.unavar == 0:
                    self.aumento_grados -= 1
                    self.otravar = 1
                self.var_i += 1
                # if len(self.step)>len(self.tiempo_step):
                #     self.tiempo_step.append((tim.time() - self.eje_tiempo) / 60)
        # if self.aumento_grados > self.temp_inicial + self.num_pasos - 1:
        if self.aumento_grados > self.temp_inicial + 9:
            self.unavar = 0
        if (self.otravar == 1) and (self.aumento_grados < self.temp_inicial-1) and (self.bandera == 1):
                self.datagen.next('$Q\r')

                self.bandera = 0
                self.paused = True
                self.on_save_data(self)
                self.on_save_plot(self)
            '''
        if (self.bandera == 1):
            print(tim.time()-self.contador_tiempo) # para ver el tiempo transcurrido (quitar en raspberry pi)
            self.num_pasos = int(tabla.shape[0])

            if (tim.time() - self.contador_tiempo > self.duracion[self.ind_pasos]):
                self.ind_pasos += 1
                if self.ind_pasos == self.num_pasos:
                    self.otravar = 1
                else:
                    self.datagen.next('$R16=1\r')
                    self.datagen.next('$R23=1\r')
                    self.datagen.next('$R13=5\r')
                    self.datagen.next('$R0=' + str(tabla[self.ind_pasos, 1]) + '\r')
                    self.datagen.next('$W\r')

        if (self.otravar == 1) and (self.var_fin == 0):
            self.datagen.next('$Q\r')
            self.var_fin = 1
            self.bandera = 0
            self.paused = True
            # print('PARÓ')
            self.on_save_data(self)
            self.on_save_plot(self)
            self.Close()


        # ---------------------------------------------------------------------------------------------------------------

    def on_tiempo_muestreo(self, event):
        if not self.paused and self.run:

            try:

                ##########
                gain = 1  # ADC's Gain parameter
                sps = 25  # ADC's SPS parameter

                AllChannelValuesVolts = [0, 0, 0, 0, 0, 0, 0, 0]
                AllChannelValues = [0, 0, 0, 0, 0, 0, 0, 0]
                ads1256.start(str(gain), str(sps))
                AllChannelValues = ads1256.read_all_channels()
                for i in range(0, 8):
                    AllChannelValuesVolts[i] = (((AllChannelValues[i] * 100) / 167.0) / int(gain)) / 1000000.0

                # for i in range(0, 8):
                #     print AllChannelValues[i]
                # print ("\n");
                # for i in range(0, 8):
                #     print AllChannelValuesVolts[i]

                    # Stop the use of the ADC
                ads1256.stop()
                ##########





                v_step = float(self.datagen.next('$R0?\r'))
                v_sysresp = float(self.datagen.next('$R100?\r'))
                v_actuador = float(self.datagen.next('$R106?\r'))
                v_sysref = float(self.datagen.next('$R102?\r'))

                self.step.append(v_step)
                self.sysresp.append(v_sysresp)
                self.actuador.append(v_actuador)  # self.State[2])
                self.errors.append(abs(v_sysresp - v_step) * 100 / v_step)
                # self.sysref.append(float(self.datagen.next('$R103?\r')))
                self.sysref.append(v_sysref)  # Sensor de temperatura 3 (externo)
                # self.sysref.append(float(self.datagen.next('$R101?\r')))#Sensor de temperatura 2 (interno al fondo)

                # self.perturb.append(50)#self.State[4])
                self.diffase.append(AllChannelValuesVolts[0])
                self.difamp.append(AllChannelValuesVolts[1])

                vec_long = [len(self.step), len(self.sysresp), len(self.actuador), len(self.errors), len(self.sysref), len(self.diffase), len(self.difamp)]
                val_min = min(vec_long)
                self.step = self.step[0:val_min]
                self.sysresp = self.sysresp[0:val_min]
                self.actuador = self.actuador[0:val_min]
                self.errors = self.errors[0:val_min]
                self.sysref = self.sysref[0:val_min]
                self.diffase = self.diffase[0:val_min]
                self.difamp = self.difamp[0:val_min]


                if (val_min == len(self.tiempo_step)+1):
                    self.tiempo_step.append((tim.time() - self.eje_tiempo) / 60)


            except:
                print('hubo error de comunicación, se ignoró el dato')


            # self.step.append(float(self.datagen.next('$R0?\r')))
            # self.sysresp.append(float(self.datagen.next('$R100?\r')))
            # self.actuador.append(float(self.datagen.next('$R106?\r')))#self.State[2])
            # self.errors.append(abs(float(self.datagen.next('$R100?\r'))-float(self.datagen.next('$R0?\r')))*100/float(self.datagen.next('$R0?\r')))#self.State[3])
            # # se cambio el error por porcentual
            # # self.sysref.append(float(self.datagen.next('$R103?\r')))
            # self.sysref.append(float(self.datagen.next('$R102?\r')))#Sensor de temperatura 3 (externo)
            # # self.sysref.append(float(self.datagen.next('$R101?\r')))#Sensor de temperatura 2 (interno al fondo)
            #
            #
            # #self.perturb.append(50)#self.State[4])
            # while len(self.step) > len(self.tiempo_step):
            #     self.tiempo_step.append((tim.time() - self.eje_tiempo) / 60)

    def flash_status_message(self, msg, flash_len_ms=1000):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_flash_status_off, self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)

    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    def OnClose(self, event):
        result = wx.MessageBox(u"¿Seguro quieres salir?","Info", style=wx.CENTER|wx.YES_NO|wx.ICON_QUESTION)

        if result == wx.NO:
            print("No Salio")
            #event.Veto()
        else:
            print("Salio")
            print(self.datagen.next('$Q\r'))
            #event.Skip()
            #self.datagen.ser.close()
            self.datagen.__del__()
            #self.Destroy()
            #self.panel.Close()
            #event.Skip()
            exit()

    def frametecnicas(self, event):
        frame_2 = Control()
        frame_2.Show()

    def framealarmas(self, event):
        frame_3 = Alarmas(self.datagen)
        frame_3.Show()
class Control(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, title="RUTINA DE CONTROL / MOTOERES PASO A PASO")
        self.panel_2 = wx.Panel(self, wx.ID_ANY)
        title = wx.StaticText(self.panel_2, wx.ID_ANY, u"CONTROL MANUAL DESPLAZAMIENTO Y SELECCIÓN")
        # --------------------------------------
        # Variables
        # --------------------------------------
        # Seleccionar motores / Check Buttons
        # Seleccionar Velocidad / Text
        # Seleccionar Pasos / Text
        # Seleccionar Direccion ComboBox
        # Boton de START-STOP-PAUSE / Button
        # Mostrar la posición de los motores / Gauge

        self.CH_Motor1 = wx.CheckBox(self.panel_2, wx.ID_ANY, "M1(SELECCIONAR)")
        self.CH_Motor2 = wx.CheckBox(self.panel_2, wx.ID_ANY, "M2(DESPLAZAR)")

        vel = wx.StaticText(self.panel_2, wx.ID_ANY, "VEL: ")
        way = wx.StaticText(self.panel_2, wx.ID_ANY, "DIR: ")
        step = wx.StaticText(self.panel_2, wx.ID_ANY, "STP: ")

        self.txt_vel = wx.TextCtrl(self.panel_2)
        self.txt_dir = wx.TextCtrl(self.panel_2)
        self.txt_step = wx.TextCtrl(self.panel_2)

        B_Start = wx.Button(self.panel_2, wx.ID_ANY, "START", size=(200, 50))
        B_Stop = wx.Button(self.panel_2, wx.ID_ANY, "STOP", size=(200, 50))

        # EVENTOS
        self.Bind(wx.EVT_BUTTON, self.start, B_Start)
        self.Bind(wx.EVT_BUTTON, self.stop, B_Stop)

        # SIZERS
        PPSizer_2 = wx.BoxSizer(wx.VERTICAL)

        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        TxtSizer = wx.BoxSizer(wx.HORIZONTAL)
        CHSizer = wx.BoxSizer(wx.HORIZONTAL)
        # --------------------------------------
        ButtonSizer.Add(B_Start, 0, wx.CENTRE, 5)
        ButtonSizer.Add(B_Stop, 0, wx.CENTRE, 5)
        # --------------------------------------
        TxtSizer.Add(vel, 0, wx.CENTRE, 5)
        TxtSizer.Add(self.txt_vel, 0, wx.CENTRE, 5)
        TxtSizer.Add(way, 0, wx.CENTRE, 5)
        TxtSizer.Add(self.txt_dir, 0, wx.CENTRE, 5)
        TxtSizer.Add(step, 0, wx.CENTRE, 5)
        TxtSizer.Add(self.txt_step, 0, wx.CENTRE, 5)
        # --------------------------------------
        CHSizer.Add(self.CH_Motor1, 0, wx.CENTRE, 5)
        CHSizer.AddSpacer(20)
        CHSizer.Add(self.CH_Motor2, 0, wx.CENTRE, 5)
        # --------------------------------------
        PPSizer_2.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer_2.Add(title, 0, wx.CENTRE, 5)
        PPSizer_2.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer_2.Add(CHSizer, 0, wx.CENTRE, 5)
        PPSizer_2.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer_2.Add(TxtSizer, 0, wx.CENTRE, 5)
        PPSizer_2.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer_2.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer_2.Add(ButtonSizer, 0, wx.CENTRE, 5)
        # --------------------------------------
        self.panel_2.SetSizer(PPSizer_2)
        PPSizer_2.Fit(self)

    def start(self, event):
        # dato = "<I,0,1,0,0>"
        # self.PushStatusText(u"Sistema Incia/Continua:"+ dato)
        #Cliente.write("<I,0,1,0,0>")

        pass

    def stop(self, event):
        pass
class Alarmas(PPLForm):
    def __init__(self,datagen):
        wx.Frame.__init__(self, None, wx.ID_ANY, title="RUTINA DE CONTROL / MOTOERES PASO A PASO")
        self.panel_2 = wx.Panel(self, wx.ID_ANY)
        title = wx.StaticText(self.panel_2, wx.ID_ANY, u"CONTROL MANUAL DESPLAZAMIENTO Y SELECCIÓN")
        self.datagen = datagen
        self.voltagein = BoundControlBox2(self.panel_2, wx.ID_ANY, u"Voltage In", 0,50)
        self.voltageout = BoundControlBox2(self.panel_2, wx.ID_ANY, u"Corriente Out", 0,50)

        self.fan1current = BoundControlBox2(self.panel_2, wx.ID_ANY, u"I Ventilador 1", 0,50)
        self.fan2current = BoundControlBox2(self.panel_2, wx.ID_ANY, u"I Ventilador 2", 0,50)

        self.t1 = BoundControlBox2(self.panel_2, wx.ID_ANY, u"T1", 0,50)
        self.t2 = BoundControlBox2(self.panel_2, wx.ID_ANY, u"T2", 0,50)
        self.t3 = BoundControlBox2(self.panel_2, wx.ID_ANY, u"T3", 0,50)
        self.t4 = BoundControlBox2(self.panel_2, wx.ID_ANY, u"T4", 0,50)

        self.send_button = wx.Button(self.panel_2, wx.ID_ANY, u"ENVIAR", size=(100, 40))
        self.read_button = wx.Button(self.panel_2, wx.ID_ANY, u"LEER", size=(100, 40))

        # EVENTOS
        self.Bind(wx.EVT_BUTTON, self.enviar, self.send_button)
        self.Bind(wx.EVT_BUTTON, self.leer, self.read_button)

        # SIZERS
        PPSizer = wx.BoxSizer(wx.VERTICAL)

        voltage = wx.BoxSizer(wx.HORIZONTAL)
        fan = wx.BoxSizer(wx.HORIZONTAL)
        temp = wx.BoxSizer(wx.HORIZONTAL)
        button = wx.BoxSizer(wx.HORIZONTAL)

        voltage.Add(self.voltagein, 0, wx.CENTRE, 5)
        voltage.Add(self.voltageout, 0, wx.CENTRE, 5)

        fan.Add(self.fan1current, 0, wx.CENTRE, 5)
        fan.Add(self.fan2current, 0, wx.CENTRE, 5)

        temp.Add(self.t1, 0, wx.CENTRE, 5)
        temp.Add(self.t2, 0, wx.CENTRE, 5)
        temp.Add(self.t3, 0, wx.CENTRE, 5)
        temp.Add(self.t4, 0, wx.CENTRE, 5)

        button.Add(self.send_button, 0, wx.CENTRE, 5)
        button.Add(self.read_button, 0, wx.CENTRE, 5)

        PPSizer.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer.Add(title, 0, wx.CENTRE, 5)
        PPSizer.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer.Add(voltage, 0, wx.CENTRE, 5)
        PPSizer.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer.Add(fan, 0, wx.CENTRE, 5)
        PPSizer.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer.Add(temp, 0, wx.CENTRE, 5)
        PPSizer.Add(wx.StaticLine(self.panel_2, ), 0, wx.ALL | wx.EXPAND, 5)
        PPSizer.Add(button, 0, wx.CENTRE, 5)

        self.panel_2.SetSizer(PPSizer)
        PPSizer.Fit(self)

    def enviar(self, event):
        if self.voltagein.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'ALVH', self.voltagein.manual_H.GetValue()))
        if self.voltagein.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'ALVL', self.voltagein.manual_L.GetValue()))
        # ------------------------------------
        if self.voltageout.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'ALMCH', self.voltageout.manual_H.GetValue()))
        if self.voltageout.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'ALMCL', self.voltageout.manual_L.GetValue()))
        # ------------------------------------
        if self.fan1current.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'ALF1CH', self.fan1current.manual_H.GetValue()))
        if self.fan1current.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'ALF1CL', self.fan1current.manual_L.GetValue()))
        # ------------------------------------
        if self.fan2current.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'ALF2CH', self.fan2current.manual_H.GetValue()))
        if self.fan2current.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'ALF2CL', self.fan2current.manual_L.GetValue()))
        # ------------------------------------
        if self.t1.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'AT1H', self.t1.manual_H.GetValue()))
        if self.t1.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'AT1L', self.t1.manual_L.GetValue()))
        # ------------------------------------
        if self.t2.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'AT2H', self.t2.manual_H.GetValue()))
        if self.t2.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'AT2L', self.t2.manual_L.GetValue()))
        # ------------------------------------
        if self.t3.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'AT3H', self.t3.manual_H.GetValue()))
        if self.t3.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'AT3L', self.t3.manual_L.GetValue()))
        # ------------------------------------
        if self.t4.check_H.GetValue():
            self.datagen.next(R.com('Escribir', 'AT4H', self.t4.manual_H.GetValue()))
        if self.t4.check_L.GetValue():
            self.datagen.next(R.com('Escribir', 'AT4L', self.t4.manual_L.GetValue()))
        # ------------------------------------
        #self.datagen.next('$W\r')
    def leer(self, events):
        self.voltagein.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALVH')))))   #str(float(self.datagen.next('$R45?\r'))))
        self.voltagein.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALVL')))))

        self.voltageout.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALMCH')))))
        self.voltageout.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALMCL')))))

        self.fan1current.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALF1CH')))))
        self.fan1current.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALF1CL')))))

        self.fan2current.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALF2CH')))))
        self.fan2current.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'ALF1CL')))))

        self.t1.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT1H')))))
        self.t1.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT1L')))))

        self.t2.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT2H')))))
        self.t2.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT2L')))))

        self.t3.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT3H')))))
        self.t3.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT3L')))))

        self.t4.manual_H.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT4H')))))
        self.t4.manual_L.SetValue(str(float(self.datagen.next(R.com('Leer', 'AT4L')))))
#-----------------Clases importadas de Perfiles.py------------------------
class EditableListCtrl(wx.ListCtrl, listmix.TextEditMixin):

    def __init__(self, parent, ID=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        """Constructor"""
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.TextEditMixin.__init__(self)

class MyForm(wx.Frame):
    # ----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Perfiles de Temperatura")

        # Add a panel so it looks the correct on all platforms
        panel = wx.Panel(self, wx.ID_ANY)




    # ----------------------------------------------------------------------


#-------------------------------------------------------------------------
def main():

    #app = wx.PySimpleApp()
    app = wx.App(False)
    #--------------------------------------
    frame = PPLForm()
    frame.Show()
    #--------------------------------------
    app.MainLoop()

if __name__ == "__main__":
    main()


