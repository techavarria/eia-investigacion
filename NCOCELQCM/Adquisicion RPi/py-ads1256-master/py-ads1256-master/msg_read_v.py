import paho.mqtt.client as mqttClient
import time
import ads1256
import csv
from array import array
import numpy as np

gain = 1  # ADC's Gain parameter
sps = 25  # ADC's SPS parameter
AllChannelValuesVolts = [0, 0, 0, 0, 0, 0, 0, 0]
AllChannelValues = [0, 0, 0, 0, 0, 0, 0, 0]
ads1256.start(str(gain), str(sps))

frec = 0
tiempo_incial = 0
parar = 0

v_value = [0]
v_tiempo = [0]
v_amp = [0]
v_fase = [0]
var_fech = 0
fecha = ''

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        global Connected  # Use global variable
        Connected = True  # Signal connection
    else:
        print("Connection failed")

def on_message(client, obj, msg):
    global frec, tiempo_inicial, parar, var_fech, fecha
    if msg.topic == 'fecha':
        fecha = msg.payload
        var_fech = 1
    if var_fech == 1:
        if msg.topic == "inicio":
            frec = msg.payload
            tiempo_inicial = time.time()
            parar = 0

        if msg.topic == "fin":
            parar = 1
        print(msg.topic + " " + str(msg.payload))

def save_data():

    path = r'/home/pi/Documents/' + time.strftime("%d_%m_%Y-%H.%M.%S") + '.csv'
    with open(path, 'w') as fp:
        a = csv.writer(fp, delimiter=';')
        v_value.pop(0)
        for i in (v_value):
            v_tiempo.append(float(str(i).split(",")[0]))
            v_amp.append(float(str(i).split(",")[1]))
            v_fase.append(float(str(i).split(",")[2]))


        v_1 = array('f', np.array(v_tiempo))
        v_2 = array('f', np.array(v_amp))
        v_3 = array('f', np.array(v_fase))

        k = 0
        a.writerows([['Tiempo', 'Amplitud', 'Fase']])
        for step in v_1:
            a.writerows([[v_1[k], v_2[k], v_3[k]]])
            k = k + 1


Connected = False  # global variable for the state of the connection

broker_address = "m12.cloudmqtt.com"
port = 17785
user = "qsbfmyfp"
password = "zWnhGsM6YoH7"

client = mqttClient.Client("RPi")  # create new instance
client.username_pw_set(user, password=password)  # set username and password
client.on_connect = on_connect  # attach function to callback
client.connect(broker_address, port=port)  # connect to broker

client.on_message = on_message

client.loop_start()  # start the loop

client.subscribe("#", 0)

value = 0
amp = 0
fase = 0
tiempo = 0
v_i = 0
while Connected != True:  # Wait for connection
    time.sleep(0.1)
try:
    while True:
        if frec != 0:
            if v_i == 0:
                client.publish('RPi', 'Conectado y listo')
                v_i = 1
            AllChannelValues = ads1256.read_all_channels()
            for i in range(0, 8):
                AllChannelValuesVolts[i] = (((AllChannelValues[i] * 100) / 167.0) / int(gain)) / 1000000.0

            amp = AllChannelValuesVolts[1]
            fase = AllChannelValuesVolts[0]
            tiempo = time.time() - tiempo_inicial
            value = str(tiempo) + ',' + str(amp) + ',' + str(fase)

            client.publish("valor", value)
            v_value.append(value)
        if parar == 1:
            frec = 0
            save_data()
            parar = 0
            tiempo = 0
            amp = 0
            fase = 0
        time.sleep(.1)  # revisar si toca aumentar este valor

except KeyboardInterrupt:
    client.disconnect()
    client.loop_stop()
ads1256.stop()
