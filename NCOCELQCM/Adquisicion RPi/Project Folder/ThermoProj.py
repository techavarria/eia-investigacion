##Raspberry Pi Temperature Sensor/Thermal Time Constant Calculater
##author: Josh Holovka
##Sponsored by: Therm-O-Disc
##08/04/2016
##

from Tkinter import *

import Tkinter as TK
import sys
import matplotlib
import matplotlib.animation as animation
from matplotlib import style
style.use('ggplot')

matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2TkAgg
from matplotlib.figure import Figure

from datetime import datetime
import random
import RPi.GPIO as GPIO
import time
import os
from functools import partial

filename = "tempdata.tmp"
num_run=0
btn_funcid=0

root = Tk() #Makes the window

root.wm_title("Therm-O-Disc Project") #Makes the title that will appear in the top left
root.config(background = 'blue')
#root.wm_iconbitmap('/home/pi/favicon.ico')
active_stat = True
root.geometry('{}x{}'.format(800,425))

active_time = True
time_var =False
#update temp until another button is pressed
def scanning():
    
    if active_time:
        disTemp()
    
        
    root.after(100,scanning)
               
def disTemp():
    
    global active_time
    active_time=True
    global log_temp
    log_temp = False
    global temp
    anim = False
    Log.delete(0.0,'end')
    
   
    Log.insert(0.0, '%.1f C' %read())
  
#Method obtained at https://www.raspberrypi.org/forums/viewtopic.php?f=32&t=143991
#Original Author: Scotty
def closeNum(event):
    try:
        global num_run, btn_funcid
        if num_run == 1:
            boot.destroy()
            num_run = 0
            window.unbind('<Button-1>',runNum)
    except (TypeError):
            print "Numpad Unbind Exception Handled"       
        

##Method obtained at https://www.raspberrypi.org/forums/viewtopic.php?f=32&t=143991
##Original Author: ScottyAuthor: Scotty
def runNum(event):
    global num_run,btn_funcid
    if num_run==0:
        num_run=1
        numpad()
        btn_funcid=window.bind('<Button-1>',closeNum)
    
#creates numpad for Time Function
##Method obtained at https://www.raspberrypi.org/forums/viewtopic.php?f=32&t=143991
##Original Author: Scotty
def numpad():
    global num_run,boot
    boot = Tk()
    boot['bg']='blue'
    lf = LabelFrame(boot,text='keypad',bd=3)
    lf.pack(padx=15,pady=10)
    btn_list = [
        '7','8','9',
        '4','5','6',
        '1','2','3',
        '0','Del','Close']
    r =1
    c=0
    n=0
    btn = list(range(len(btn_list)))
    for label in btn_list:
        cmd = partial(clickNum,label)
        btn[n] =Button(lf,text=label,width=10,height=5,command=cmd)
        btn[n].grid(row=r,column=c)
        n+=1
        c+=1
        if c==3:
            c=0
            r+=1
#handles operation when buttons on numpad are pressed
#Method obtained at https://www.raspberrypi.org/forums/viewtopic.php?f=32&t=143991
#Author: Scotty
def clickNum(btn):
    global num_run
    text = '%s' % btn
    if not text =='Del' and not text=='Close':
        e.insert(END,text)
    if text == 'Del':
        e.delete(0,END)
    if text == 'Close':
        boot.destroy()
        num_run = 0
        e.unbind('<Button-1',btn_funcid)            
#Read Temp From File made by Thermproj.c
def read():
    global temp
    global log_temp
    
    filename = "tempdata.tmp"
	#block until writer finishes
   
    try:
    	 with open(filename,'r') as f:
           	data= f.read()
    		#print data
    		
    		temp = float (data)
    		LED(temp)    
                           
    except IOError as err:
            print "Exception Handled Read"
        
    return temp
#Displaying Current Temperature
def disTime():
    global time_var
    time_var=False
    global active_time
    active_time= False
    global log_temp
    log_temp = False
    global temp
    global tempHi
    global window
    global Log
    global LogT
    global eFram
    global e
    tempHi=0
    var=0
    Log.delete(0.0,'end')
    
    window = Toplevel(root)
    eFram = Frame(window, width=50, height=150)
    
    eFram.grid(row=2, column=2, padx=10, pady=2)
    lab = Label(eFram, text="Enter Final Temp C :")
    lab.grid(row=0, column=0, padx=10, pady=2)
    e = Entry(eFram,textvariable=v)
    e.delete(0,END)
    e.bind('<Button-1>',runNum)
    e.grid(row=0,column =1,padx=0,pady=2,sticky= 'W,E,N,S')
   
    ok = Button(eFram, text="OK",width=10,command=timer)
    ok.grid(row=1, column=0, padx=2, pady=2)
    ex = Button(eFram, text='Exit',width=10, command=window.destroy)
    ex.grid(row=1, column=1, padx=2,pady=2)
   
#Graphing the temperatures Overtime
def LogTemp():
    a.clear()
    global active_time
    active_time = False
    global log_temp
    log_temp = True
    global time_var
    time_var=False
  
#Thermal Constant Timer
def animate(f):
    
       global log_temp
       if log_temp:
         count=0
         ##Graph follows rpi's clock time in seconds
         ##Graph resets on every minute
         while count<1:
            count+=1
            Log.delete(0.0,'end')
            timeC= str(datetime.now().time())
            parts=[]
            parts=timeC.split(':')
            temps=read()
            second= parts[2].split('.')
            Log.insert(0.0, '%.1f C' %temps)
            yar.append(temps)
            xar.append(second[0])
            if(int(second[0])==59):
                del yar[:]
                del xar[:]
             
            root.update_idletasks()
            a.clear()
            a.set_ylabel('# Degrees C')
            a.set_xlabel('Time(Seconds)')
            a.set_ylim([-10,80])
            a.set_xlim([0,60])
            a.plot(xar,yar, linewidth = 3 )
            time.sleep(.001)
            
       return [a]
    
#starts and displays timer        
def timer():
    global tempHi
    global temp
    global roots
    global Log
    global time_var
    time_var=True
    log_temp=False
    counter =0;
    roots=Tk()
    roots.title('Thermal Time Constant')
    label = Label(roots,fg='blue')
    label.grid(row=2,column=0,padx=10,pady=2)
    labels = Label(roots,fg='black')
    labels.grid(row=1,column=0,padx=10,pady=2)
    button = Button(roots, text='Ok',width=30, command = close)
    button.grid(row=3,column =0,padx=10, pady=2)
    tempHi=v.get()
        #if final temp is greater than initial
    if(float(tempHi) > read()):
        #calculations for Thermal Time Constant
        currentTemp=float(read())
        tempDiff = float(tempHi)-currentTemp
        thermConst= float(tempDiff)*.632
        active_time=True
        tempChange = currentTemp+ thermConst
    
        labels.config(text=str(format(tempChange,'.1f'))+' C')
      
        time.sleep(.1)
        #displays time in seconds
        while(tempChange>=read()):
           counter +=1
           time.sleep(.1)
           Log.insert(0.0, '%.1f C' %temp)
           roots.update_idletasks()
           Log.delete(0.0,'end')
           sec = float(counter)/10
           label.config(text=str(sec)+' Seconds')
           if sec >= 90:
               break
          
          #if final temp is lower than intial
    elif(float(tempHi)<read()):
         #calculations for Thermal Time Constant
         currentTemp=float(read())
         tempDiff = currentTemp-float(tempHi)
         thermConst= float(tempDiff)*.632
         tempChange = currentTemp-thermConst
         
         labels.config(text=str(format(tempChange,'.1f'))+' C')
        
         sys.stdout.flush()
         while(tempChange<=read()):
           counter +=1
           time.sleep(.1)
           Log.insert(0.0, '%.1f C' %temp)
           roots.update_idletasks()
           Log.delete(0.0,'end')
           sec = float(counter)/10
           label.config(text=str(sec)+' Seconds')
           if sec >= 90:
               break
    
    active_time=False
#handles closing of multiple windows   
def close():
    try:
        window.destroy()
    except (Exception):
          print "Exception Handled 1"
    try:
        roots.destroy()
    except (Exception):
          print "Exception Handled 2"
    try:
        e.destroy()
    except (Exception):
          print "Exception Handled 3"
    try:
        boot.destroy()
    except (Exception):
          print "Exception Handled 4"
          
#Lights up LEDS based off current Temp
def LED(temp):
        
	GPIO.setmode(GPIO.BCM)
	
	GPIO.setup(16,GPIO.OUT)
	GPIO.setup(13,GPIO.OUT)
	GPIO.setup(12,GPIO.OUT)
	GPIO.setup(6,GPIO.OUT)
	GPIO.setup(5,GPIO.OUT)
	GPIO.setup(4,GPIO.OUT)

	if temp < 10:
		
		GPIO.output(16, GPIO.HIGH)
		GPIO.output(13, GPIO.HIGH)
		GPIO.output(12,GPIO.LOW)
		GPIO.output(6,GPIO.LOW)
		GPIO.output(5,GPIO.LOW)
		GPIO.output(4,GPIO.LOW)
	elif temp >10 and temp<12.7:
		
		GPIO.output(16, GPIO.LOW)
		GPIO.output(13, GPIO.HIGH)
		GPIO.output(12,GPIO.LOW)
		GPIO.output(6,GPIO.LOW)
		GPIO.output(5,GPIO.LOW)
		GPIO.output(4,GPIO.LOW)
	elif temp >12.7 and temp<16.67:
		
		GPIO.output(16, GPIO.LOW)
		GPIO.output(13, GPIO.HIGH)
		GPIO.output(12,GPIO.HIGH)
		GPIO.output(6,GPIO.LOW)
		GPIO.output(5,GPIO.LOW)
		GPIO.output(4,GPIO.LOW)
	elif temp>16.67 and temp<25:
		
		GPIO.output(16, GPIO.LOW)
		GPIO.output(13, GPIO.LOW)
		GPIO.output(12,GPIO.HIGH)
		GPIO.output(6,GPIO.HIGH)
		GPIO.output(5,GPIO.LOW)
		GPIO.output(4,GPIO.LOW)
	elif temp>25 and temp< 30.55:
		
		GPIO.output(16, GPIO.LOW)
		GPIO.output(13, GPIO.LOW)
		GPIO.output(12,GPIO.LOW)
		GPIO.output(6,GPIO.HIGH)
		GPIO.output(5,GPIO.HIGH)
		GPIO.output(4,GPIO.LOW)

	elif temp > 30.55:
		
		GPIO.output(16, GPIO.LOW)
		GPIO.output(13, GPIO.LOW)
		GPIO.output(12,GPIO.LOW)
		GPIO.output(6,GPIO.LOW)
		GPIO.output(5,GPIO.HIGH)
		GPIO.output(4,GPIO.HIGH)
	
	GPIO.cleanup
	return

log_temp=False
v = StringVar()

#Left Frame and its contents
leftFrame = Frame(root, width=350, height = 370)
leftFrame.grid(row=0, column=0, padx=2, pady=2)

Label(leftFrame, text="Instructions:").grid(row=0, column=0, padx=10, pady=2)

Instruct = Label(leftFrame, text="Created by Josh Holovka")
Instruct.grid(row=1, column=0, padx=10, pady=2)

try:
    imageEx = PhotoImage(file = 'Therm-O-Disc_Black_Logo.gif')
    Label(leftFrame, image=imageEx).grid(row=26, column=0, padx=10, pady=2)
except:
    print("Image not found")


#Right Frame and its contents
rightFrame = Frame(root, width=400, height =420)
rightFrame.grid(row=0, column=1, padx=2, pady=2)


#create graph
global yar
yar=[]
global xar
xar=[]
f = Figure(figsize=(7,5),dpi=65)
a = f.add_subplot(111)
a.set_ylabel('# Degrees C')
a.set_xlabel('Time(Seconds)')
a.set_ylim([-10,80])
a.set_xlim([0,60])
canvas= FigureCanvasTkAgg(f,leftFrame)
canvas.show()
canvas.get_tk_widget().grid(row=0, column=0, padx=5, pady=5)
canvas._tkcanvas.grid(row=0, column=0, padx=5, pady=5)

btnFrame = Frame(rightFrame, width=200, height = 200)
btnFrame.grid(row=10, column=0, padx=10, pady=10)

Log = Text(rightFrame, width = 10, height = 1, takefocus=0)
Log.grid(row=2, column=0, padx=10, pady=2)


Instructs = Label(rightFrame, text="Temp: Displays  temperature in Fahrenheit\nTime: Find Thermal Time Constant\nGraph:Graph's Temp data in realtime")
Instructs.grid(row=11, column=0, padx=4, pady=2)

TmpBtn = Button(btnFrame, text="Temp", command=disTemp)
TmpBtn.grid(row=4, column=0, padx=4, pady=2)

TimeBtn = Button(btnFrame, text="Therm Time", command=disTime)
TimeBtn.grid(row=4, column=1, padx=4, pady=2)

TempLog = Button(btnFrame, text="Graph", command=LogTemp)
TempLog.grid(row=4, column=2, padx=4, pady=2)

   
root.after(100,scanning)
ani = animation.FuncAnimation(f,animate,frames =30,interval = 20, blit = True)
root.mainloop() #start monitoring and updating the GUI

